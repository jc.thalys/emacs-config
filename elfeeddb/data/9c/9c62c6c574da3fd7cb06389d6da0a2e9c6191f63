<ul class="org-ul">
<li>Emacs Lisp:
<ul class="org-ul">
<li><a href="http://jr0cket.co.uk/2017/03/spacemacs-managing-broken-emacs-packages.html">Spacemacs &#8211; Managing Broken Emacs Packages</a></li>
<li><a href="http://www.wilfred.me.uk/blog/2017/03/19/pattern-matching-in-emacs-lisp/">Pattern Matching in Emacs Lisp</a></li>
</ul>
</li>
<li>Emacs development:
<ul class="org-ul">
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=9f771f6440a61433d34f14aee4046cf2fa6ba391">New configure option –with-mailutils</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=0f3d1b782353fd1fc0ab5f89d47d9e790f44e6b2">New option ediff-show-ancestor and new toggle ediff-toggle-show-ancestor</a></li>
<li><a href="http://lists.gnu.org/archive/html/emacs-devel/2017-03/msg00420.html">master has switched from Automake to GNU Make</a></li>
<li><a href="http://lists.gnu.org/archive/html/emacs-devel/2017-03/msg00338.html">Discussion of user-defined record types</a></li>
<li><a href="http://lists.gnu.org/archive/html/emacs-devel/2017-03/msg00356.html">Bug report statistics on OS and arch</a>, now with graphs</li>
</ul>
</li>
<li>Navigation:
<ul class="org-ul">
<li><a href="http://mbork.pl/2017-03-13_Ibuffer">Ibuffer</a></li>
<li><a href="https://www.masteringemacs.org/article/shell-comint-secrets-history-commands">Shell &amp; Comint Secrets: History commands</a> (<a href="https://www.reddit.com/r/emacs/comments/5z847v/shell_comint_secrets_history_commands/">Reddit</a>)</li>
<li><a href="https://oremacs.com/2017/03/18/dired-ediff/">Quickly ediff files from dired</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/5zxmcz/ediff_with_a_predicate/">ediff with a predicate</a> &#8211; narrowing helps</li>
<li><a href="https://github.com/tumashu/exwm-x">Package of the day: exwm-x, another window manager</a> (<a href="https://www.reddit.com/r/emacs/comments/5zb7yo/package_of_the_day_exwmx_another_window_manager/">Reddit</a>)</li>
<li><a href="http://irreal.org/blog/?p=6008">The Silver Searcher, Ivy-occur, and Swiper</a></li>
<li><a href="https://www.reddit.com/r/spacemacs/comments/5ztqc1/some_windows_style_defaults_in_a_spacemacs_layer/">Some &#8220;Windows style&#8221; defaults in a Spacemacs layer</a></li>
</ul>
</li>
<li>Appearance:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/5zrgh9/hlline_and_wrapprefix/">hl-line and wrap-prefix</a></li>
</ul>
</li>
<li>Org Mode:
<ul class="org-ul">
<li><a href="http://pragmaticemacs.com/emacs/export-org-mode-headlines-to-separate-files/">Export org-mode headlines to separate files</a></li>
<li><a href="http://samflint.com/capture-with-email.html">Capturing Data for Org Via Email</a> (<a href="https://www.reddit.com/r/emacs/comments/5zvk5q/capturing_data_for_org_via_email/">Reddit</a>)</li>
<li><a href="https://github.com/Kungsgeten/ivy-todo">ivy-todo – Manage org-mode TODO lists with ivy</a> (<a href="https://www.reddit.com/r/emacs/comments/5zqn50/ivytodo_manage_orgmode_todo_lists_with_ivy/">Reddit</a>)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/5zc0fc/org_for_collecting_bookmarks_with_content/">org for collecting bookmarks (with content)</a></li>
</ul>
</li>
<li>Coding:
<ul class="org-ul">
<li><a href="http://puntoblogspot.blogspot.com/2017/03/browsing-allegro-lisp-docs-inside-emacs.html">Browsing allegro lisp docs inside emacs</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/60913e/i_wrote_a_bit_of_code_for_a_dwim/">dwim python-send-to-shell function that behaves much like it might in ESS mode</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/6052md/emacs_for_htmlcssphpjavascript/">emacs for html+css+php+javascript</a></li>
<li><a href="https://github.com/hying-caritas/project-shells">Project Shells: Manage multiple shell (or terminal, eshell) buffers for each project</a> (<a href="https://www.reddit.com/r/emacs/comments/5zwc40/project_shells_manage_multiple_shell_or_terminal/">Reddit</a>)</li>
<li><a href="https://lists.gnu.org/archive/html/guile-devel/2017-03/msg00095.html">GNU Guile 2.2.0 released &#8211; &#8220;Guile&#8217;s Elisp implementation is now fully Emacs-compatible&#8221;</a> (<a href="https://www.reddit.com/r/emacs/comments/5zqnrm/gnu_guile_220_released_guiles_elisp/">Reddit</a>)</li>
<li><a href="https://github.com/gdrte/go-bimenu">A better imenu for go-mode in emacs</a> (<a href="https://www.reddit.com/r/emacs/comments/5zoavh/a_better_imenu_for_gomode_in_emacs/">Reddit</a>)</li>
</ul>
</li>
<li>Other:
<ul class="org-ul">
<li><a href="http://irreal.org/blog/?p=6010">Stein&#8217;s Emacs Productivity Series</a></li>
<li><a href="http://mbork.pl/2017-03-18_Deleting_all_instances_of_a_LaTeX_environment">Deleting all instances of a LaTeX environment</a></li>
<li><a href="https://notmuchmail.org/news/">New version of Notmuch (0.24) is out.</a> (<a href="https://www.reddit.com/r/emacs/comments/5zqqup/new_version_of_notmuch_024_is_out/">Reddit</a>)</li>
</ul>
</li>
<li>New packages:
<ul class="org-ul">
<li><a href="http://melpa.org/#/auto-read-only" target="_blank">auto-read-only</a>: Automatically make the buffer to read-only</li>
<li><a href="http://melpa.org/#/comint-intercept" target="_blank">comint-intercept</a>: Intercept input in comint-mode</li>
<li><a href="http://melpa.org/#/dired-hide-dotfiles" target="_blank">dired-hide-dotfiles</a>: Hide dotfiles in dired</li>
<li><a href="http://melpa.org/#/el2org" target="_blank">el2org</a>: Convert elisp file to org file</li>
<li><a href="http://melpa.org/#/hack-time-mode" target="_blank">hack-time-mode</a>: Forge time</li>
<li><a href="http://melpa.org/#/ivy-todo" target="_blank">ivy-todo</a>: Manage org-mode TODOs with ivy</li>
<li><a href="http://melpa.org/#/lastpass" target="_blank">lastpass</a>: LastPass command wrapper</li>
<li><a href="http://melpa.org/#/nimbus-theme" target="_blank">nimbus-theme</a>: An Awesome Dark Theme</li>
<li><a href="http://melpa.org/#/org-mru-clock" target="_blank">org-mru-clock</a>: load most recently used clocks into history</li>
<li><a href="http://melpa.org/#/persp-mode-projectile-bridge" target="_blank">persp-mode-projectile-bridge</a>: persp-mode + projectile integration.</li>
<li><a href="http://melpa.org/#/pippel" target="_blank">pippel</a>: Emacs frontend to python package manager pip</li>
<li><a href="http://melpa.org/#/pocket-mode" target="_blank">pocket-mode</a>: Manage your pocket</li>
</ul>
</li>
</ul>
<p>Links from <a href="http://reddit.com/r/emacs/new">reddit.com/r/emacs</a>, <a href="http://reddit.com/r/orgmode">/r/orgmode</a>, <a href="http://reddit.com/r/spacemacs">/r/spacemacs</a>, <a href="https://hn.algolia.com/?query=emacs&amp;sort=byDate&amp;prefix&amp;page=0&amp;dateRange=all&amp;type=story">Hacker News</a>, <a href="http://planet.emacsen.org">planet.emacsen.org</a>, <a href="https://www.youtube.com/results?search_query=emacs&amp;search_sort=video_date_uploaded">Youtube</a>, the changes to the <a href="http://git.savannah.gnu.org/cgit/emacs.git/log/etc/NEWS">Emacs NEWS file</a>, and <a href="http://lists.gnu.org/archive/html/emacs-devel/2017-03">emacs-devel</a>.</p>
<p><a href="http://sachachua.com/blog/category/emacs-news">Past Emacs News round-ups</a></p>
