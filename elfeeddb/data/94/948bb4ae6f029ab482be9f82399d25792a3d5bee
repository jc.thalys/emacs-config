<p>I've had something of an obsession with digital pinball for <a href="https://blog.codinghorror.com/pc-pinball-sims/">years</a> now. That recently culminated in me buying <a href="http://virtuapin.net/index.php?main_page=product_info&amp;products_id=92">a Virtuapin Mini</a>.</p>

<p><a href="http://virtuapin.net/index.php?main_page=product_info&amp;products_id=92"><img src="https://blog.codinghorror.com/content/images/2016/11/virtuapin-mini-screens.jpg" alt="" title=""></a></p>

<p>OK, yes, it's an extravagance. There's no question. But in my defense, it is a minor extravagance relative to a real pinball machine. </p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/virtuapin-mini-vs-real-pinball.jpg" alt=""></p>

<p>The mini is much smaller than a normal pinball machine, so it's easier to move around, takes up less space, and is less expensive. Plus <strong>you can emulate every pinball machine, ever!</strong> The <a href="http://virtuapin.net/index.php?main_page=product_info&amp;products_id=92">Virtuapin Mini</a> is a custom $3k build centered around three screens:</p>

<ul>
<li>27" main playfield (HDMI)</li>
<li>23" backglass (DVI)</li>
<li>8" digital matrix (USB LCD)</li>
</ul>

<p>Most of the magic is in those screens, and whether the pinball sim in question allows you to arrange the three screens in its advanced settings, usually by enabling a "cabinet" mode.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/virtuapin-three-monitors-size-and-position.png" alt=""></p>

<p>Let me give you an internal tour. Open the front coin door and detach the two internal nuts for the front bolts, which are finger tight. Then remove the metal lockdown bar and slide the tempered glass out.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/virtuapin-mini-lockdown-bar.jpg" alt=""></p>

<p>The most uniquely pinball item in the case is right at the front. This <a href="http://virtuapin.net/index.php?main_page=product_info&amp;cPath=8&amp;products_id=105">Digital Plunger Kit</a> connects the 8 buttons (2 on each side, 3 on the front, 1 on the bottom) and includes an <strong>analog tilt sensor</strong> and <strong>analog plunger sensor</strong>. All of which shows up as a standard game controller in Windows.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/virtuapin-mini-control-board-1.jpg" alt=""></p>

<p>On the left front side, the audio amplifier and left buttons.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/virtuapin-mini-left-front.jpg" alt=""></p>

<p>On the right front side, the digital plunger and right buttons.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/virtuapin-mini-right-front.jpg" alt=""></p>

<p>The 27" playfield monitor is mounted using a clever rod assembly to the standard VESA mount on the back, so we can easily rotate it up to work on the inside as needed.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/virtuapin-mini-monitor-raised.jpg" alt=""></p>

<p>To remove the playfield, disconnect the power cord and the HDMI connector. Then lift it up and out, and you now have complete access to the interior.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/virtuapin-mini-interior.jpg" alt=""></p>

<p>Notice the large down-firing subwoofer mounted in the middle of the body, as well as the ventilation holes. The PC "case" is just a back panel, and the power strip is <a href="https://www.amazon.com/dp/B000P1QJXQ/?tag=codihorr-20">the Smart Strip kind</a> where it auto-powers everything based on the PC being powered on or off. The actual power switch is on the bottom front right of the case.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/virtuapin-mini-backglass-speakers.jpg" alt=""></p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/virtuapin-mini-playfield.jpg" alt=""></p>

<p>Powering it up and getting all three screens configured in the pinball sim of your choice results in &hellip; <em>magic</em>.</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/7TpqHAU9oxk" frameborder="0" allowfullscreen></iframe>

<p>It is a <strong>thoroughly professional build</strong>, as you'd expect from a company that has been building these pinball rigs for the last decade. It uses real wood (not MDF), tempered glass, and authentic metal pinball parts throughout.</p>

<p>I was truly impressed by the build quality of this machine. Paul of Virtuapin said they're on roughly version four of the machine and it shows. It's over 100 pounds fully assembled and arrives on a shipping pallet. I can only imagine how heavy the full size version would be!</p>

<p>That said, I do have some tweaks I recommend:</p>

<ul>
<li><p><strong>Make <em>absolutely sure</em> you get <a href="https://blog.codinghorror.com/the-ips-lcd-revolution/">an IPS panel</a> as your 27" playfield monitor</strong>. As arrived, mine had a TN panel and while it was playable if you stood directly in front of the machine, playfield visibility was pretty dire outside that narrow range. I dropped in the <a href="https://www.amazon.com/dp/B00KYCSRSG/?tag=codihorr-20">BenQ GW2765HT</a> to replace the GL2760H that was in there, and I was golden. If you plan to order, I would definitely talk to Paul at VirtuaPin and specify that you want this IPS display even if it costs a little more. The 23" backglass monitor is also TN but the viewing angles are reasonable-ish in that orientation and the backglass is mostly for decoration anyway.</p></li>
<li><p>The improved display has a 1440p resolution compared to the 1080p originally shipped, so you might want to upgrade from the GeForce 750 Ti video card to the <a href="https://www.amazon.com/dp/B01MF7EQJZ/?tag=codihorr-20">just-released 1050 Ti</a>. This is not strictly required, as I found the 750 Ti an excellent performer even at the higher resolution, but I plan to play only fully 3D pinball sims and the 1050 Ti <a href="http://www.pcworld.com/article/3134528/components-graphics/nvidia-geforce-gtx-1050-and-gtx-1050-ti-review-the-new-budget-gaming-champions.html">gets excellent reviews</a> for $140, so I went for it.</p></li>
<li><p>Internally everything is exceptionally well laid out, the only very minor improvement I'd recommend is connecting the rear exhaust fan to the motherboard header so its fan speed can be dynamically controlled by the computer rather than being at full power all the time.</p></li>
<li><p>On the <a href="http://virtuapin.net/index.php?main_page=product_info&amp;products_id=92">Virtuapin website order form</a> the PC they provide sounds quite outdated, but don't sweat it: I picked the lowest options thinking I would have to replace it all, and they shipped me a Haswell based quad-core PC with 8GB RAM and a 256GB SSD, even though those options weren't even on the order form.</p></li>
</ul>

<p>I realize $3k (plus palletized shipping) is a lot of money, but I estimate it would cost you at <em>least</em> $1500 in parts to build this machine, plus a month of personal labor. Provided you get the IPS playfield monitor, this is a solidly constructed "real" pinball machine, and if you're into digital pinball like I am, it's an absolute <em>joy</em> to play and a good deal for what you actually get. As Ferris Bueller once said:</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/0wALArd2rvo" frameborder="0" allowfullscreen></iframe>

<p>If you'd like to experiment with this and don't have three grand burning a hole in your pocket, 90% of digital pinball simulation is <strong>a widescreen display in portrait mode</strong>. Rotate one of your monitors, add another monitor if you're feeling extra fancy, and give it a go.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/pinball-with-rotated-monitor.jpg" alt=""></p>

<p>As for software, most people talk about <a href="https://en.wikipedia.org/wiki/Visual_Pinball">Visual Pinball</a> for these machines, and it works. But the combination of janky hacked-together 2D bitmap technology used in the gameplay, and the fact that all those designs are ripoffs that pay nothing in licensing back to the original pinball manufacturers really bothers me.</p>

<p>I prefer <a href="http://store.steampowered.com/app/238260/">Pinball Arcade</a> in DirectX 11 mode, which is <a href="https://imgur.com/a/vPQvh">downright beautiful</a>, easily (and legally!) obtainable via Steam and offers a stable of 60+ incredible officially licensed classic pinball tables to choose from, all meticulously recreated in high resolution 3D with excellent physics.</p>

<p><a href="https://imgur.com/a/vPQvh"><img src="https://blog.codinghorror.com/content/images/2016/11/visual-pinball-dx9-vs-dx11.jpg" alt="" title=""></a></p>

<p>As for getting pinball simulations running on your three monitor setup, if you're lucky the game will have a <strong>cabinet mode</strong> you can turn on. Unfortunately, this can be weird due to &hellip; licensing issues. Apparently building a pinball sim on the computer requires entirely different licensing than placing it inside a full-blown pinball cabinet.</p>

<p><a href="http://store.steampowered.com/app/238260/"><strong>Pinball Arcade</strong></a> has a <a href="http://pinballarcadefans.com/showthread.php/10199-DOWNLOAD-Pinball-Arcade-Free-Camera-Mod">nifty camera hack</a> someone built that lets you position three cameras as needed to get the three displays. You will also need the excellent <a href="http://www.x360ce.com/">x360ce program</a> to dynamically map joystick events and buttons to a simulated Xbox 360 controller.</p>

<p><a href="http://store.steampowered.com/app/226980/"><strong>Pinball FX2</strong></a> added a cabinet mode about a year ago, but turning it on requires a special code and you have to send them a picture of your cabinet (!) to get that code. I did, and the cabinet mode works great; just enter your code, specify the coordinates of each screen in the settings and you are good to go. While these tables definitely have arcadey physics, I find them great fun and there are a ton to choose from. </p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/virtapin-mini-portal-2.JPG" alt=""></p>

<p><a href="http://store.steampowered.com/app/287900/"><strong>Pro Pinball Timeshock Ultra</strong></a> is unique because it's originally from 1997 and was one of the first "simulation" level pinball games. The current rebooted version is still pre-rendered graphics rather than 3D, but the client downloads the necessary gigabytes of pre-rendered content at your exact screen resolution and it looks amazing.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/pro-pinball-timeshock-ultra-4k-1.jpg" alt=""></p>

<p>Timeshock has explicit cabinet support in the settings and via command line tweaks. Also, in cabinet mode, when choosing table view, you want the bottom left one. Trust me on this! It supports maximum height for portrait cabinet mode.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/11/pro-pinball-timeshock-best-cabinet-view.jpg" alt=""></p>

<p>Position each window as necessary, then enable fullscreen for each one and it'll snap to the monitor you placed it on. It's "only" one table, but arguably the most classic of all pinball sims. I sincerely hope they continue to reboot the rest of the Pro Pinball series, including Big Race USA which is my favorite.</p>

<p>I've always loved pinball machines, even though they struggled to keep up with digital arcade games. In some ways I view my current project, <a href="https://discourse.org">Discourse</a>, as a similarly analog experience attempting to bridge the gap to the modern digital world:</p>

<blockquote>
  <p>The fantastic 60 minute documentary <a href="http://www.tilt-movie.com/">Tilt: The Battle to Save Pinball</a> has so many parallels with what we're trying to do for forum software.</p>
</blockquote>

<iframe width="560" height="315" src="https://www.youtube.com/embed/JIolBJwH9p0" frameborder="0" allowfullscreen></iframe>

<blockquote>
  <p>Pinball is threatened by Video Games, in the same way that Forums are threatened by Facebook and Twitter and Tumblr and Snapchat. They're considered old and archaic technology. They've stopped being sexy and interesting relative to what else is available.</p>
  
  <p>Pinball was forced to reinvent itself several times throughout the years, from mechanical, to solid state, to computerized. And the defining characteristic of each "era" of pinball is that the new tables, once you played them, made all the previous pinball games seem immediately obsolete because of all the new technology.</p>
  
  <p>The <a href="https://en.wikipedia.org/wiki/Pinball_2000">Pinball 2000</a> project was an attempt to invent the next generation of pinball machines:</p>
</blockquote>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Vt0OARfqeks" frameborder="0" allowfullscreen></iframe>

<blockquote>
  <blockquote>
    <p>It wasn't a new feature, a new hardware set, it was everything new. We have to get everything right. We thought that we had reinvented the wheel. And in many respects, we had.</p>
  </blockquote>
  
  <p>This is exactly what we want to do with <a href="https://discourse.org">Discourse</a> &ndash; build a forum experience so advanced that playing will make all previous forum software seem immediately obsolete. </p>
  
  <p>Discourse aims to save forums and make them relevant and useful to a whole new generation.</p>
</blockquote>

<p>So if I seem a little more nostalgic than most about pinball, perhaps a little <em>too</em> nostalgic at times, <a href="https://blog.codinghorror.com/the-only-truly-failed-project/">maybe that's why</a>. </p>

<table>  
<tr><td class="welovecodinghorror">  
[advertisement] Building out your tech team? <a href="http://careers.stackoverflow.com/products" rel="nofollow">Stack Overflow Careers</a> helps you hire from the largest community for programmers on the planet. We built our site with developers like you in mind.
</td></tr>  
</table>