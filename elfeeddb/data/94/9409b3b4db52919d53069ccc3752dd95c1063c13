<p>
It's been almost a month since <a href="https://stormpath.com/blog/stormpaths-new-path">Stormpath joined forces with Okta</a>. My first day at Okta was on February 27, and I was only briefly in the San Francisco headquarters. I had to fly out at noon on my second day, so I hunkered down in the Okta Pub and cranked out a presentation for a <a href="https://www.meetup.com/kc-spring/events/237347674/">talk with Micah Silverman</a> at the Kansas City Spring User Group.
</p>
<p style="text-align: center">
<a href="https://www.flickr.com/photos/mraible/32660168943/" title="The Okta Pub"><img src="https://c1.staticflickr.com/1/597/32660168943_760470f40f.jpg" width="500" alt="The Okta Pub"></a>
</p>
<p>
That's right, Okta has a <em>pub</em> in their SF HQ. When I first heard about this, I knew it'd be a good fit for me!
</p>

<p>Now properly fortified, I finished the presentation and headed for the airport, where I rejoiced in my clothing choices for the day.</p>

<div style="max-width: 500px; margin: 0 auto">
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">I love wearing this <a href="https://twitter.com/goStormpath">@goStormpath</a> t-shirt when going through security at the airport. ?? <a href="https://t.co/4oOGiaXEd9">pic.twitter.com/4oOGiaXEd9</a></p>&mdash; Matt Raible (@mraible) <a href="https://twitter.com/mraible/status/836650699962236928">February 28, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
</div>

<p>
The whirlwind of ramping up at Okta hasn't died down yet. Last week, I figured out how to authenticate with Okta's API using Spring Boot and SAML. I also got an OAuth 2.0 example working. Then I moved onto Angular and got an example working with OpenID Connect (OIDC), <a href="https://github.com/okta/okta-signin-widget">Okta's Sign-In Widget</a>, and the <a href="https://github.com/okta/okta-auth-js">Okta Auth SDK</a>. I was especially pumped when I got an Angular client working with OIDC and a Spring Boot + Spring Security backend. This week, I wrote up my findings as tutorials and recorded a couple screencasts to accompany them. These will likely show up as blogs posts on <a href="http://developer.okta.com/blog/">Okta Developer Blog</a> over the next few weeks.
</p>
<p>
While the first couple of weeks at Okta has been exciting, I'm more excited about the upcoming Devoxx conferences I'll be speaking at.
</p> 
<p>Next week, <a href="http://devoxx.us/">Devoxx US</a> will be happening for the first time! As a member of the program committee, I  promise you this is going to be a great show! We had an incredible number of high quality submissions and it shows in the <a href="http://cfp.devoxx.us/2017/talks">agenda</a>. I'm especially looking forward to <a href="https://twitter.com/janellekz">Janelle Klein's</a> <a href="http://cfp.devoxx.us/2017/talk/NAN-8167/What_is_%22Identity%22%3F">What is Identity?</a> keynote. I'll be doing talks on <a href="http://cfp.devoxx.us/2017/speaker/matt_raible">JHipster, Asciidoctor, and how NOT to restore a VW Bus</a>.
</p>
<p>Speaking of the bus, Hefe sure looks good, doesn't he? <img src="http://raibledesigns.com/images/smileys/wink.gif" class="smiley" alt=";-)" title=";-)" /></p>

<div style="max-width: 500px; margin: 0 auto">
<blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:34.76851851851852% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BRqXnjwBck-/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Matt Raible (@vwsforlife)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-03-15T14:24:26+00:00">Mar 15, 2017 at 7:24am PDT</time></p></div></blockquote> <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
</div>

<p>After returning from Devoxx US, Trish and I are taking Abbie and Jack on the spring break trip of a lifetime. I've never been to <a href="http://bigskyresort.com/">Big Sky</a>, so we're heading there for a week of skiing, frolicking, and playing in the snow. I might even go phoneless for the week to fully embrace the serenity that Montana provides.
</p>
<p>I'm off to <a href="http://devoxx.fr/">Devoxx France</a> the following week. I'm really looking forward to this conference because <a href="http://cfp.devoxx.fr/2017/speaker/matt_raible">my talks</a> are all about Angular. I'll be doing a hands-on lab on getting started with Angular, as well as developing a <abbr title="Progressive Web App">PWA</a> with Josh Long.
</p>
<p>To make things even better while I'm on the road, I'm getting some work done on both VWs. We're getting Stout the Syncro painted and having a stereo installed in Hefe. With any luck, Stout 5.0 and Hefe 3.0 will be released in April, just in time for the car show season.</p>
<p>So yeah, life is pretty darn good right now. Let me know if you'll be in Tahoe, San Jose, Big Sky, or Paris when I'm there. I'd love to chat about authentication, open source technologies, VWs, or good beer.
</p>