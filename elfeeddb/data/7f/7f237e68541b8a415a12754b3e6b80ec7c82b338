<div><p>While I was getting <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://www.hanselman.com/blog/RubyOnRailsOnAzureAppServiceWebSitesWithLinuxAndUbuntuOnWindows10.aspx">Ruby on Rails to work nicely under Ubuntu on Windows 10</a> I took the opportunity to set up my *nix bash environment, which was largely using defaults. Yes, I know I can use zsh or <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://www.hanselman.com/blog/InstallingFishShellOnUbuntuOnWindows10.aspx">fish</a> or other shells. Yes, I know I can use emacs and screen, but I am using Vim and tmux. Fight me. Anyway, once my post was done, I starting messing around with open source .NET Core on Linux (it runs on Windows, Mac, and Linux, but here I'm running on Linux on Windows. #Inception) <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://twitter.com/shanselman/status/852398546623971328">and tweeted a pic of my desktop</a>. </p> <p>By the way, I feel totally vindicated by all the interest in "text mode" given my 2004 blog post "<a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://www.hanselman.com/blog/OpportunityWindowsIsCompletelyMissingTheTextModeBoat.aspx">Windows is completely missing the TextMode boat</a>." ;)'</p> <p>Also, for those of you who are DEEPLY NOT INTERESTED in the command line, that's cool. You can stop reading now. Totally OK. I also use Visual Studio AND Visual Studio Code. Sometimes I click and mouse and sometimes I tap and type. There is room for us all.</p> <blockquote> <p><strong>WHAT IS ALL THIS LINUX ON WINDOWS STUFF?</strong> Here's a <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://msdn.microsoft.com/en-us/commandline/wsl/faq">FAQ on the Bash/Windows Subsystem for Linux/Ubuntu on Windows/Snowball in Hell</a> and some <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://msdn.microsoft.com/en-us/commandline/wsl/release_notes">detailed Release Notes</a>. Yes, it's real, and it's spectacular. Can't read that much text? Here's a <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://www.hanselman.com/blog/VIDEOHowToRunLinuxAndBashOnWindows10AnniversaryUpdate.aspx">video I did on Ubuntu on Windows 10</a>.</p></blockquote> <p>A number of people asked me how they could set up their WSL (Windows Subsystem for Linux) installs to be something like this, so here's what I did. Note that will I've been using *nix on and off for 20+ years, I am by no means an expert. I am, and have been, Permanently Intermediate in my skills. I do not dream in RegEx, and I am offended that others can bust out an awk script without googling.</p><figure><img title="C9RT5_bUwAALJ-H" style="border-left-width: 0px; border-right-width: 0px; background-image: none; border-bottom-width: 0px; padding-top: 0px; padding-left: 0px; display: inline; padding-right: 0px; border-top-width: 0px" border="0" alt="C9RT5_bUwAALJ-H" src="https://www.hanselman.com/blog/content/binary/Windows-Live-Writer/6d9c51f7ea4c_C30A/C9RT5_bUwAALJ-H_d967d687-7b03-4ea1-9fad-6d66e05d59e7.jpg" width="1001" height="627"></figure>  <p>So there's a few things going on in this screenshot.</p> <ul> <li>Running .NET Core on Linux (on Windows 10)  <li>Cool VIM theme with &gt;256 colors  <li><strike>Norton</strike> Midnight Commander in the corner (<a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://twitter.com/migueldeicaza/status/828288424473980929">thanks Miguel</a>)  <li>Desqview-esque tmux splitter (with mouse support)  <li>Some hotkey remapping, git prompt, completion  <li>Ubuntu Mono font  <li>Nice directory colors (DIRCOLORS/LS_COLORS)</li></ul> <p>Let's break them down one at a time. And, again, your mileage may vary, no warranty express or implied, any of this may destroy your world, you read this on a blog. Linux is infinitely configurable and the only constant is that my configuration rocks and yours sucks. Until I see something in yours that I can steal.</p> <h3>Running .NET Core on Linux (on Windows 10) </h3> <p>Since Linux on Windows 10 is (today) Ubuntu, you can install .NET Core within it just like any Linux. Here's <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://www.microsoft.com/net/core#linuxubuntu">the Ubuntu instructions for .NET Core's SDK</a>. You may have Ubuntu 14.04 or 16.04 (you can upgrade your Linux on Windows if you like). Make sure you know what you're running by doing a:</p><pre>~ $ lsb_release -a
<br>No LSB modules are available.
<br>Distributor ID: Ubuntu
<br>Description:    Ubuntu 16.04.2 LTS
<br>Release:        16.04
<br>Codename:       xenial
<br>~ $</pre>
<p>If you're not on 16.04 you can easily remove and reinstall the whole subsystem with these commands at cmd.exe (note the /full is serious and torches the Linux filesystem):</p><pre>&gt; lxrun /uninstall /full
<br>&gt; lxrun /install </pre>
<p>Or if you want you can run this within bash (will take longer but maintain settings). </p>
<blockquote>
<p><strong>NOTE</strong> that you'll need <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://www.microsoft.com/en-us/windows/features">Windows 10 Creators Edition</a> build 16163 or greater to run Ubuntu 16.04. Type "winver" to check your build.</p></blockquote><pre>sudo do-release-upgrade</pre>
<p>Know what Ubuntu your Windows 10 has <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://www.microsoft.com/net/core#linuxubuntu">when you install .NET Core within it</a>. The other thing to remember is that now you have two .NET Cores, one Windows and one Ubuntu, on the same (kinda) machine. Since the file systems are separated it's not a big deal. I do my development work within Ubuntu on /mnt/d/github (which is a Windows drive). It's OK for the Linux subsystem to edit files in Linux or Windows, but <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://blogs.msdn.microsoft.com/commandline/2016/11/17/do-not-change-linux-files-using-windows-apps-and-tools/">don't "reach into" the Linux file system from Windows</a>.</p>
<h3>Cool Vim theme with &gt;256 colors </h3>
<p>That Vim theme is <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://github.com/morhetz/gruvbox">gruvbox</a> and I installed it like this. Thanks to Rich Turner for turning me on to this theme.</p><pre>$ cd ~/
<br>$ mkdir .vim
<br>$ cd .vim
<br>$ mkdir colors
<br>$ cd colors
<br>$ curl -O https://raw.githubusercontent.com/morhetz/gruvbox/master/colors/gruvbox.vim
<br>$ cd ~/
<br>$ vim .vimrc</pre>
<p>Paste the following (hit ‘i’ for insert and then right click/paste)</p><pre>set number
<br>syntax enable
<br>set background=dark
<br>colorscheme gruvbox
<br>set mouse=a
<br>
<br>if &amp;term =~ '256color'
<br>  " disable Background Color Erase (BCE) so that color schemes
<br>  " render properly when inside 256-color tmux and GNU screen.
<br>  " see also http://snk.tuxfamily.org/log/vim-256color-bce.html
<br>  set t_ut=
<br>endif</pre>
<p>Then save and exit with Esc, <strong>:wq </strong>(write and quit). There's a ton of themes out there, so try some for yourself!</p>
<h3><strike>Norton</strike> Midnight Commander in the corner (<a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://twitter.com/migueldeicaza/status/828288424473980929">thanks Miguel</a>) </h3>
<p>Midnight Commander is a wonderful Norton Commander clone that Miguel de Icaza started, that's licensed as part of GNU. I installed it via apt, as I would any Ubuntu software.</p><pre>$ sudo apt-get install mc</pre>
<p>There's mouse support within the Windows conhost (console host) that bash runs within, so you'll even get mouse support within Midnight Commander!</p>
<p><a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://midnight-commander.org/"><img title="Midnight Commander" style="border-left-width: 0px; border-right-width: 0px; background-image: none; border-bottom-width: 0px; padding-top: 0px; padding-left: 0px; display: inline; padding-right: 0px; border-top-width: 0px" border="0" alt="Midnight Commander" src="https://www.hanselman.com/blog/content/binary/Windows-Live-Writer/6d9c51f7ea4c_C30A/image_5e1729e4-a2bc-4002-9737-e04dff4cd608.png" width="642" height="425"></a></p>
<p>Great stuff.</p>
<h3>Desqview-esque tmux splitter (with mouse support) </h3>
<p>Tmux is a terminal multiplexer. It's a text-mode windowing environment within which you can run multiple programs. Even better, you can "detach" from a running session and reattached from elsewhere. Because of this, folks love using tmux on servers where they can ssh in, set up an environment, detach, and reattach from elsewhere. </p>
<blockquote>
<p><strong>NOTE:</strong> The Windows Subsystem for Linux shuts down all background processes when the last console exits. So you can detach and attach tmux sessions happily, but just make sure you don't close every console on your machine.</p></blockquote>
<p>Here's a nice animated gif of me moving the splitter on tmux on Windows. YES I KNOW YOU CAN USE THE KEYBOARD BUT THIS GIF IS COOL.</p>
<p><img src="https://www.hanselman.com/blog/content/binary/tmuxonwindows.gif"></p>
<h3>Some hotkey remapping, git prompt, completion </h3>
<p>I am still learning tmux but here's my .tmux.conf. I've made a few common changes to make the hotkey creation of windows easier.</p><pre>#remap prefix from 'C-b' to 'C-a'
<br>unbind C-b
<br>set-option -g prefix C-a
<br>bind-key C-a send-prefix
<br>
<br># split panes using | and -
<br>bind | split-window -h
<br>bind _ split-window -v
<br>unbind '"'
<br>unbind %
<br>bind k confirm kill-window
<br>bind K confirm kill-server
<br>bind &lt; resize-pane -L 1
<br>bind &gt; resize-pane -R 1
<br>bind - resize-pane -D 1
<br>bind + resize-pane -U 1
<br>bind r source-file ~/.tmux.conf
<br>
<br># switch panes using Alt-arrow without prefix
<br>bind -n M-Left select-pane -L
<br>bind -n M-Right select-pane -R
<br>bind -n M-Up select-pane -U
<br>bind -n M-Down select-pane -D
<br>
<br># Enable mouse control (clickable windows, panes, resizable panes)
<br>set -g mouse on
<br>set -g default-terminal "screen-256color"</pre>
<p>I'm using the default Ubuntu .bashrc that includes a check for dircolors (more on this below) but I added this for git-completion.sh and a git prompt, as well as these two alias. I like being able to type "desktop" to jump to my Windows Desktop. And the -x on Midnight Commander helps the mouse support.</p><pre>alias desktop="cd /mnt/c/Users/scott/Desktop"
<br>alias mc="mc -x"

<br>export CLICOLOR=1
<br>source ~/.git-completion.sh
<br>PS1='\[\033[37m\]\W\[\033[0m\]$(__git_ps1 " (\[\033[35m\]%s\[\033[0m\])") \$ '
<br>GIT_PS1_SHOWDIRTYSTATE=1
<br>GIT_PS1_SHOWSTASHSTATE=1
<br>GIT_PS1_SHOWUNTRACKEDFILES=1
<br>GIT_PS1_SHOWUPSTREAM="auto"</pre>
<p>Git Completion can be installed with:</p><pre>sudo apt-get install git bash-completion</pre>
<h3>Ubuntu Mono font </h3>
<p>I really like the Ubuntu Mono font, and I like the way it looks when running Ubuntu under Windows. You can <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~font.ubuntu.com/">download the Ubuntu Font Family free</a>. Right click the downloaded TTF files and right click and "Install," or drag them into C:\windows\fonts. Then click the upper left corner of any Bash console window and change your font to Ubuntu Mono.</p>
<p><img title="Ubuntu Mono" style="border-left-width: 0px; border-right-width: 0px; background-image: none; border-bottom-width: 0px; padding-top: 0px; padding-left: 0px; display: inline; padding-right: 0px; border-top-width: 0px" border="0" alt="Ubuntu Mono" src="https://www.hanselman.com/blog/content/binary/Windows-Live-Writer/6d9c51f7ea4c_C30A/image_e6c3e3a5-d76c-4a28-b0d1-e3cea4533f91.png" width="642" height="354"></p>
<h3>Nice directory colors (DIRCOLORS/LS_COLORS)'</h3>
<p>If you have a black command prompt background, then default colors for directories will be dark blue on black, which sucks. Fortunately you can get .dircolors files from all over the wep, or set the <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://github.com/trapd00r/LS_COLORS">LS_COLORS</a> (make sure to search for LS_COLORS for Linux, not the other, different LSCOLORS on Mac) environment variable.</p>
<p>I ended up with <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~https://github.com/seebi/dircolors-solarized">"dircolors-solarized" from here</a>, downloaded it with wget or curl and put it in ~. Then confirm this is in your .bashrc (it likely is already)</p><pre># enable color support of ls and also add handy aliases
<br>if [ -x /usr/bin/dircolors ]; then
<br>    test -r ~/.dircolors &amp;&amp; eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
<br>    alias ls='ls --color=auto'
<br>    alias dir='dir --color=auto'
<br>    #alias vdir='vdir --color=auto'
<br>
<br>    alias grep='grep --color=auto'
<br>    alias fgrep='fgrep --color=auto'
<br>    alias egrep='egrep --color=auto'
<br>fi</pre>
<p>Download whatever .dircolors file makes you happy (make sure the filename ends up as ".dircolors," so you may need to cp yoursourcefile ~/.dircolors and then restart your console.</p>
<p>Make a big difference for me, and as I mention, it's totally, gloriously, maddeningly configurable.</p>
<p><img title="Nice dircolors" style="border-left-width: 0px; border-right-width: 0px; background-image: none; border-bottom-width: 0px; padding-top: 0px; padding-left: 0px; display: inline; padding-right: 0px; border-top-width: 0px" border="0" alt="Nice dircolors" src="https://www.hanselman.com/blog/content/binary/Windows-Live-Writer/6d9c51f7ea4c_C30A/image_009c6b53-3d5c-4458-a8e7-7cc5f78e39e0.png" width="642" height="353"></p>
<p>Leave YOUR <strong>Linux on Windows </strong>tips in the comments!</p>
<hr>
<p><strong>Sponsor: </strong>Did you know <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~hnsl.mn/2nVVBhs">VSTS can integrate closely with Octopus Deploy</a>? Watch Damian Brady and Brian A. Randell as they show you how to automate deployments from VSTS to Octopus Deploy, and demo the new <strong>VSTS Octopus Deploy dashboard</strong> widget. <a href="http://feeds.hanselman.com/~/t/0/0/scotthanselman/~hnsl.mn/2nVVBhs">Watch now</a>
<br/><hr/>© 2017 Scott Hanselman. All rights reserved. 
<br/></div><Img align="left" border="0" height="1" width="1" alt="" style="border:0;float:left;margin:0;padding:0;width:1px!important;height:1px!important;" hspace="0" src="http://feeds.hanselman.com/~/i/291693404/0/scotthanselman">
<div style="clear:both;padding-top:0.2em;"><a title="Like on Facebook" href="http://feeds.hanselman.com/_/28/291693404/scotthanselman"><img height="20" src="http://assets.feedblitz.com/i/fblike20.png" style="border:0;margin:0;padding:0;"></a>&#160;<a title="Share on Google+" href="http://feeds.hanselman.com/_/30/291693404/scotthanselman"><img height="20" src="http://assets.feedblitz.com/i/googleplus20.png" style="border:0;margin:0;padding:0;"></a>&#160;<a title="Tweet This" href="http://feeds.hanselman.com/_/24/291693404/scotthanselman"><img height="20" src="http://assets.feedblitz.com/i/twitter20.png" style="border:0;margin:0;padding:0;"></a>&#160;<a title="Subscribe by email" href="http://feeds.hanselman.com/_/19/291693404/scotthanselman"><img height="20" src="http://assets.feedblitz.com/i/email20.png" style="border:0;margin:0;padding:0;"></a>&#160;<a title="Subscribe by RSS" href="http://feeds.hanselman.com/_/20/291693404/scotthanselman"><img height="20" src="http://assets.feedblitz.com/i/rss20.png" style="border:0;margin:0;padding:0;"></a>&#160;</div>
