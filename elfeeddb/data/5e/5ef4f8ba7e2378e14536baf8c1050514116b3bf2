<p>Every geek goes through a phase where they discover emulation. It's practically a <a href="https://blog.codinghorror.com/rediscovering-arcade-nostalgia/">rite of passage</a>.</p>

<blockquote>
  <p>I think I spent most of my childhood &ndash; and a large part of my life as a young adult &ndash; <b>desperately wishing I was in a video game arcade.</b> When I finally obtained my driver's license, my first thought wasn't about the girls I would take on dates, or the road trips I'd take with my friends. Sadly, no. I was thrilled that I could drive myself to the arcade any time I wanted. </p>
</blockquote>

<p>My two arcade emulator builds in 2005 satisfied my itch thoroughly. I recently took my son Henry to the <a href="http://www.caextreme.org/">California Extreme expo</a>, which features almost every significant pinball and arcade game ever made, live and in person and real. He enjoyed it so much that I found myself again yearning to share that part of our history with my kids &ndash; in a suitably emulated, arcade form factor.</p>

<p>Down, down the rabbit hole I went again:</p>

<p><a href="http://www.hybridarcades.com/"><img src="https://blog.codinghorror.com/content/images/2016/08/bartop-ebay.jpg" alt="" title=""></a></p>

<p><a href="https://www.arcademinis.com/sunshop/index.php?l=product_detail&amp;p=60"><img src="https://blog.codinghorror.com/content/images/2016/07/bartop-vertical-jamma.jpg" alt="" title=""></a></p>

<p><a href="https://shop.pimoroni.com/collections/picade-and-arcade/products/picade"><img src="https://blog.codinghorror.com/content/images/2016/07/picade.jpg" alt="" title=""></a></p>

<p><a href="http://forum.arcadecontrols.com/index.php?topic=122366.0"><img src="https://blog.codinghorror.com/content/images/2016/07/bartop-bubble-bobble.jpg" alt="" title=""></a></p>

<p><img src="https://blog.codinghorror.com/content/images/2016/07/bartop-cocktail.jpg" alt=""></p>

<p>I discovered that emulation builds are so much <em>cheaper and easier</em> now than they were when I last attempted this a decade ago. Here's why:</p>

<ol>
<li><p><strong>The ascendance of <a href="https://www.raspberrypi.org/">Raspberry Pi</a> has single-handedly revolutionized the emulation scene.</strong> The Pi is now on <a href="https://www.raspberrypi.org/products/raspberry-pi-3-model-b/">version 3</a>, which adds critical WiFi and Bluetooth functionality on top of additional speed. It's fast enough to emulate N64 and PSX and Dreamcast reasonably, all for a whopping $35. Just download the <a href="https://retropie.org.uk/">RetroPie bootable OS</a> on a $10 32GB SD card, slot it into your Pi, and &hellip; well, basically you're done. The distribution comes with some free games on it. Add additional ROMs and game images to taste.</p></li>
<li><p><strong>Chinese all-in-one JAMMA cards</strong> are available everywhere for about $90. <a href="https://www.youtube.com/watch?v=ctN60cYTRdI">Pandora's Box is one "brand"</a>. These things are are an entire 60-in-1 to 600-in-1 arcade on a board, with an ARM CPU and built-in ROMs and everything &hellip; probably completely illegal and unlicensed, of course. You could buy some old broken down husk of an arcade game cabinet, anything at all as long as it's a <a href="https://en.wikipedia.org/wiki/Japan_Amusement_Machine_and_Marketing_Association#Connector_standards">JAMMA compatible arcade game</a> &ndash; a standard introduced in 1985 &ndash; with working monitor and controls. Plug this replacement JAMMA box in, and bam: you now have your own virtual arcade. Or you could build or buy a new JAMMA compatible cabinet; there are hundreds out there to choose from.</p></li>
<li><p><strong>Cheap, quality IPS arcade size LCDs</strong>. The CRTs I used in 2005 may have been truer to old arcade games, but they were a giant pain to work with. They're enormous, heavy, and require a lot of power. Viewing angle and speed of refresh are rather critical for arcade machines, and both are largely solved problems for LCDs at this point, which are light, easy to work with, and sip power for $100 or less.</p></li>
</ol>

<p>Add all that up &ndash; it's not like the price of MDF or arcade buttons and joysticks has changed substantially in the last decade &ndash; and what we have today is a console and arcade emulation wonderland! If you'd like to go down this rabbit hole with me, bear in mind that I've just started, but I do have some specific recommendations.</p>

<p><strong>Get a Raspberry Pi starter kit.</strong> I recommend <a href="http://www.amazon.com/dp/B01D92SSX6/?tag=codihorr-20">this particular starter kit</a>, which includes the essentials: a clear case, heatsinks &ndash; you definitely want small heatsinks on your 3, as it dissipate <a href="http://www.pidramble.com/wiki/benchmarks/power-consumption">almost 4 watts</a> under full load &ndash; and a suitable power adapter. That's $50.</p>

<p><a href="http://www.amazon.com/dp/B01D92SSX6/?tag=codihorr-20"><img src="https://blog.codinghorror.com/content/images/2016/07/raspberry-pi-port-diagram.jpg" alt="" title=""></a></p>

<p><strong>Get a quality SD card.</strong> The primary "drive" on your Pi will be the SD card, so make it a quality one. Based on <a href="http://www.pidramble.com/wiki/benchmarks/microsd-cards">these excellent benchmarks</a>, I recommend the <a href="http://www.amazon.com/dp/B013CP5HCK/?tag=codihorr-20">Sandisk Extreme 32GB</a> or <a href="http://www.amazon.com/dp/B00WR4IJBE/?tag=codihorr-20">Samsung Evo+ 32GB</a> models for best price to peformance ratio. That'll be $15, tops.</p>

<p><strong>Download and install the bootable RetroPie image on your SD card.</strong> It's amazing how far this project has come since 2013, it is now about as close to plug and play as it gets for free, open source software. The install is, <a href="https://github.com/retropie/retropie-setup/wiki/First-Installation">dare I say &hellip; "easy"?</a></p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/xvYX_7iRRI0" frameborder="0" allowfullscreen></iframe>

<p><strong>Decide how much you want to build.</strong> At this point you have a fully functioning emulation brain for well under $100 which is capable of playing literally <em>every significant console and arcade game created prior to 1997</em>. Your 1985 self is probably drunk with power. It is kinda awesome. Stop doing the Safety Dance for a moment and ask yourself these questions:</p>

<ul>
<li><p>What <strong>controls</strong> do you plan to plug in via the USB ports? This will depend heavily on which games you want to play. Beyond the absolute basics of joystick and two buttons, there are Nintendo 64 games (think analog stick(s) required), driving games, spinner and trackball games, multiplayer games, yoke control games (think Star Wars), virtual gun games, and so on.</p></li>
<li><p>What <strong>display</strong> to you plan to plug in via the HDMI port? You could go with a tiny screen and build a handheld emulator, the Pi is certainly small enough. Or you could have no display at all, and jack in via HDMI to any nearby display for whatever gaming jamboree might befall you and your friends. I will say that, for whatever size you build, <em>more display is better</em>. Absolutely go as big as you can in the allowed form factor, though the Pi won't effectively use much more than a 1080p display maximum.</p></li>
<li><p>How much <strong>space</strong> do you want to dedicate to the box? Will it be portable? You could go anywhere from ultra-minimalist &ndash; a control box you can plug into any HDMI screen with a wireless controller &ndash; to a giant 40" widescreen stand up arcade machine with room for four players.</p></li>
<li><p>What's your <strong>budget</strong>? We've only spent under $100 at this point, and great screens and new controllers aren't a whole lot more, but sometimes you want to build from spare parts you have lying around, if you can.</p></li>
<li><p>Do you have the <strong>time</strong> and inclination to build this from parts? Or do you prefer to buy it pre-built?</p></li>
</ul>

<p>These are all your calls to make. You can get some ideas from the pictures I posted at the top of this blog post, or search the web for "Raspberry Pi Arcade" for <a href="http://www.slothygeek.com/6-raspberry-pi-arcade-projects-step-by-step-tutorials/">lots of other ideas</a>.</p>

<p>As a reasonable all-purpose starting point, I recommend <a href="http://www.retrobuiltgames.com/diy-kits-shop/porta-pi-arcade-wood-kit-10-hd/">the Build-Your-Own-Arcade kits</a> from Retro Built Games. From $330 for full kit, to $90 for just the wood case.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/07/retrobuiltgames-diy-pi-arcade.jpg" alt=""></p>

<p>You could also buy <a href="http://www.amazon.com/dp/B00WAY9848/?tag=codihorr-20">the arcade controls alone</a> for $75, and build out (or buy) a case to put them in.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/07/arcade-controls-for-pi.jpg" alt=""></p>

<p><img src="https://blog.codinghorror.com/content/images/2016/07/arcade-controls-mdf-case.jpg" alt=""></p>

<p>My "mainstream" recommendation is <strong>a bartop arcade</strong>. It uses a common LCD panel size in the typical horizontal orientation, it's reasonably space efficient and somewhat portable, while still being comfortably large enough for a nice big screen with large speakers gameplay experience, and it supports two players if that's what you want. That'll be about $100 to $300 depending on options.</p>

<p><img src="https://blog.codinghorror.com/content/images/2016/07/bartop-arcade-kit.jpg" alt=""></p>

<p>I remember spending well over $1,500 to build <a href="https://blog.codinghorror.com/rediscovering-arcade-nostalgia/">my old arcade cabinets</a>. I'm excited that it's no longer necessary to invest that much time, effort or money to successfully revisit our arcade past.</p>

<p>Thanks largely to the <a href="http://www.amazon.com/dp/B01D92SSX6/?tag=codihorr-20">Raspberry Pi 3</a> and the <a href="https://retropie.org.uk/">RetroPie project</a>, this is now a simple Maker project you can (and should!) take on in a weekend with a friend or family. For a budget of $100 to $300 &ndash; maybe $500 if you want to get <em>extra</em> fancy &ndash; you can have a pretty great classic arcade and classic console emulation experience. That's way better than I was doing in 2005, even adjusting for inflation.</p>

<table>  
<tr><td class="welovecodinghorror">[advertisement] At Stack Overflow, we put developers first. We already help you find answers to your tough coding questions; now let us help you <a href="http://careers.stackoverflow.com" rel="nofollow">find your next job</a>.</td></tr>  
</table>