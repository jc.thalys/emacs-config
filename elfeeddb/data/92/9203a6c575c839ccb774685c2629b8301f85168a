<p>
    Our Syncro Westy was returned to us in early February, after being in the shop since just before Christmas.
    It was in the shop for body work caused by an accident that was my fault. Luckily, no one was hurt and the
    damage was minor. The morning after we got it back, my awesome friend Ryan Moore and I packed it up and
    <a href="http://raibledesigns.com/rd/entry/harry_gates_hut_trip_in">headed on a hut trip</a> near
    Aspen, Colorado.
</p>
<p style="text-align: center">
    <a href="https://farm8.staticflickr.com/7417/15859692313_fc80f62317_c.jpg"
       title="The Syncro is back! by Matt Raible, on Flickr" rel="lightbox[winterskiingadventures]"
       data-href="https://www.flickr.com/photos/mraible/15859692313"><img
        src="https://farm8.staticflickr.com/7417/15859692313_fc80f62317_m.jpg" width="240"
        alt="The Syncro is back!" style="border: 1px solid black;"></a>
    <a href="https://farm8.staticflickr.com/7299/16293745437_8969e240d5_c.jpg"
       title="Two Packs by Matt Raible, on Flickr" rel="lightbox[winterskiingadventures]"
       data-href="https://www.flickr.com/photos/mraible/16293745437"><img
        src="https://farm8.staticflickr.com/7299/16293745437_8969e240d5_m.jpg" width="240" alt="Two Packs"
        style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p>
    <a href="https://farm9.staticflickr.com/8729/16180350303_c112218136_c.jpg"
       title="Packed and ready for Crested Butte. by Matt Raible, on Flickr" rel="lightbox[winterskiingadventures]"
       data-href="https://www.flickr.com/photos/mraible/16180350303"><img
        src="https://farm9.staticflickr.com/8729/16180350303_c112218136_t.jpg" width="100"
        alt="Packed and ready for Crested Butte." class="picture" style="border: 1px solid black;"></a>
    The next weekend was one of the most popular skiing holidays: Presidents' Day Weekend. We packed up the kids and drove
    our Ski Bus to Crested Butte. I took my guitar and Trish took her banjo (we both started taking weekly lessons at
    the beginning of the year). Our drive was smooth and our weekend was fabulous.
</p>
<p>
    Saturday was Valentine's Day and I surprised Trish with a photo shoot of our family. I'd secretly hired
    <a href="http://www.alisonwhitephotography.com/">Alison White</a> to
    take our pictures and we met with her to talk about what we wanted on Saturday morning. After a fun consultation,
    we ventured to the mountain, picking up <a href="http://www.jamesward.com/">James Ward</a> along the way.
    We skied a few runs together, stopped at the Ice Bar for a car bomb, then whisked off to our photo shoot.
</p>
<p style="text-align: center">
    <a href="https://farm9.staticflickr.com/8613/16799153151_12e8b3d9ea_c.jpg"
       title="Jack, James, Abbie and myself skiing in heaven! by Matt Raible, on Flickr"
       rel="lightbox[winterskiingadventures]" data-href="https://www.flickr.com/photos/mraible/16799153151"><img
        src="https://farm9.staticflickr.com/8613/16799153151_12e8b3d9ea_m.jpg" width="240"
        alt="Jack, James, Abbie and myself skiing in heaven!" style="border: 1px solid black;"></a>
    <a href="https://farm9.staticflickr.com/8738/16612686298_1ea3dfc384_c.jpg"
       title="Yay! Car bombs at Ice bar by Matt Raible, on Flickr" rel="lightbox[winterskiingadventures]"
       data-href="https://www.flickr.com/photos/mraible/16612686298"><img
        src="https://farm9.staticflickr.com/8738/16612686298_1ea3dfc384_m.jpg" width="240"
        alt="Yay! Car bombs at Ice bar" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p>
    We are extremely pleased with the results. Thanks Alison!
</p>
<p style="text-align: center">
    <a href="https://farm8.staticflickr.com/7617/16592698987_eed43017d3_c.jpg" title="Abbie by Matt Raible, on Flickr"
       rel="lightbox[winterskiingadventures]" data-href="https://www.flickr.com/photos/mraible/16592698987"><img
        src="https://farm8.staticflickr.com/7617/16592698987_eed43017d3_m.jpg" width="192" alt="Abbie"
        style="border: 1px solid black;"></a>
    <a href="https://farm9.staticflickr.com/8717/16592699317_c52e5ac4ca_c.jpg" title="Jack by Matt Raible, on Flickr"
       rel="lightbox[winterskiingadventures]" data-href="https://www.flickr.com/photos/mraible/16592699317"><img
        src="https://farm9.staticflickr.com/8717/16592699317_c52e5ac4ca_m.jpg" width="192" alt="Jack"
        style="border: 1px solid black; margin-left: 15px;"></a>
</p>

<p style="text-align: center">
    <a href="https://farm8.staticflickr.com/7647/16177662854_ae002ace92_c.jpg"
       title="Trish, You're Amazing! by Matt Raible, on Flickr" rel="lightbox[winterskiingadventures]"
       data-href="https://www.flickr.com/photos/mraible/16177662854"><img
        src="https://farm8.staticflickr.com/7647/16177662854_ae002ace92.jpg" width="500"
        alt="Trish, You're Amazing!" style="border: 1px solid black;"></a>
</p>
<p style="text-align: center">
    <a href="https://farm9.staticflickr.com/8611/16798890162_5dca71b78e_c.jpg"
       title="Our Fabulous Family by Matt Raible, on Flickr" rel="lightbox[winterskiingadventures]"
       data-href="https://www.flickr.com/photos/mraible/16798890162"><img
        src="https://farm9.staticflickr.com/8611/16798890162_5dca71b78e.jpg" width="500"
        alt="Our Fabulous Family" style="border: 1px solid black;"></a>

</p>
<p>
    The next weekend, Trish and I packed up and headed to our Ski Shack in Winter Park. We stayed up late that Friday
    night, preparing chili and other fixins for a fun tailgate on Saturday. We didn't get much sleep, but we were
    in "C Lot" by 7:30 and on the lifts shortly after they opened. It was a powder day, and the runs were great.
    We met up with friends at our van around noon and enjoyed some tasty chili and cold beverages.
</p>
<p style="text-align: center">
    <a href="https://farm9.staticflickr.com/8628/16800248455_de547b4489_c.jpg"
       title="Ski In, Ski Out at Challenger Lift by Matt Raible, on Flickr" rel="lightbox[winterskiingadventures]"
       data-href="https://www.flickr.com/photos/mraible/16800248455"><img
        src="https://farm9.staticflickr.com/8628/16800248455_de547b4489_m.jpg" width="240"
        alt="Ski In, Ski Out at Challenger Lift" style="border: 1px solid black;"></a>
    <a href="https://farm9.staticflickr.com/8628/16180349473_b99397f8f9_c.jpg"
       title="Tailgating at Mary Jane by Matt Raible, on Flickr" rel="lightbox[winterskiingadventures]"
       data-href="https://www.flickr.com/photos/mraible/16180349473"><img
        src="https://farm9.staticflickr.com/8628/16180349473_b99397f8f9_m.jpg" width="240"
        alt="Tailgating at Mary Jane" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p>
    The last weekend in February, we met Trish's brother (also named Matt) in Vail. He was on a ski trip with a number
    of friends and Trish drove up to ski with him on Saturday morning. The kids and I slept in a bit and arrived
    at Copper just in time for free parking near the lifts. We skied all afternoon, then met up with Trish and Matt
    for some cocktails, then dinner. That night, we did some winter camping in the van.
</p>
<p style="text-align: center">
    <a href="https://farm9.staticflickr.com/8666/16612895760_f998e5bb92_c.jpg"
       title="Our Ski Bus is loving all the snow in Denver! by Matt Raible, on Flickr"
       rel="lightbox[winterskiingadventures]" data-href="https://www.flickr.com/photos/mraible/16612895760"><img
        src="https://farm9.staticflickr.com/8666/16612895760_f998e5bb92_m.jpg" width="240"
        alt="Our Ski Bus is loving all the snow in Denver!" style="border: 1px solid black;"></a>
    <a href="https://farm8.staticflickr.com/7590/16799160842_0d170fbc3d_c.jpg"
       title="Nestled in for a night of winter camping in our Westy at Vail. Trish is snoring, kids are watching a movie upstairs, and it's dumping outside. Dad is enjoying a beer, proud of the fact he talked them all into it. by Matt Raible, on Flickr"
       rel="lightbox[winterskiingadventures]" data-href="https://www.flickr.com/photos/mraible/16799160842"><img
        src="https://farm8.staticflickr.com/7590/16799160842_0d170fbc3d_m.jpg" width="240"
        alt="Nestled in for a night of winter camping in our Westy at Vail. Trish is snoring, kids are watching a movie upstairs, and it's dumping outside. Dad is enjoying a beer, proud of the fact he talked them all into it."
        style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p>
    <a href="https://farm8.staticflickr.com/7634/16799019561_a9a44ae3d0_c.jpg"
       title="Best. Parking. Ever. by Matt Raible, on Flickr" rel="lightbox[winterskiingadventures]"
       data-href="https://www.flickr.com/photos/mraible/16799019561"><img
        src="https://farm8.staticflickr.com/7634/16799019561_a9a44ae3d0_t.jpg" width="100"
        alt="Best. Parking. Ever." class="picture" style="border: 1px solid black;"></a>
    The final weekend in my 5-weekends-in-a-row of skiing was a wonderful trip to Telluride. The kids had Thursday and
    Friday off of school last week, so we loaded up and drove to Telluride on Thursday. It took us most of the day
    to get there (7.5 hours), but the weather was beautiful and the roads were dry. Our van was too tall for our
    hotel's garage, so we had excellent parking spots all weekend.
</p>
<p>This was the breakout weekend for Jack. Abbie's been skiing awesome all year and Jack's been pretty timid (lots of
    turns, going slow).
    There was something about our first day at Telluride that inspired him, because he loved their double blacks. We
    skied
    lots of steep runs, did laps on the terrain parks and flew down the mountain on blues. Telluride had a Spring Break
    atmosphere and our ski-in/ski-out accommodations didn't hurt. It was a glorious weekend. Trish and I were both
    very proud of the kids and how fast and fearless they've become on skis.
</p>
<p style="text-align: center">
    <a href="https://farm8.staticflickr.com/7653/16612682158_11321cb770_c.jpg"
       title="From the top! by Matt Raible, on Flickr" rel="lightbox[winterskiingadventures]"
       data-href="https://www.flickr.com/photos/mraible/16612682158"><img
        src="https://farm8.staticflickr.com/7653/16612682158_11321cb770_m.jpg" width="240"
        alt="From the top!" style="border: 1px solid black;"></a>
    <a href="https://farm8.staticflickr.com/7594/16612889070_9ff03d7b31_c.jpg"
       title="Kids first double black. by Matt Raible, on Flickr" rel="lightbox[winterskiingadventures]"
       data-href="https://www.flickr.com/photos/mraible/16612889070"><img
        src="https://farm8.staticflickr.com/7594/16612889070_9ff03d7b31_m.jpg" width="240"
        alt="Kids first double black." style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p style="text-align: center">
    <a href="https://farm9.staticflickr.com/8613/16592965537_cf8e77b8ea_c.jpg"
       title="Telluride back bowl by Matt Raible, on Flickr" rel="lightbox[winterskiingadventures]"
       data-href="https://www.flickr.com/photos/mraible/16592965537"><img
        src="https://farm9.staticflickr.com/8613/16592965537_cf8e77b8ea.jpg" width="500"
        alt="Telluride back bowl" style="border: 1px solid black;"></a>
</p>
<div style="text-align: right; width: 500px; margin: -15px auto 0">
<a href="https://www.flickr.com/photos/mraible/sets/72157648990124764" style="color: #999;font-size: .9em;">Full Album &rarr;</a>
</div>
<p>I'm extremely happy with our Syncro's performance and its new H6 (Subaru Outback) engine. It's been averaging 20mpg,
    most likely because I drive 55mph in the slow lanes up the passes. I figure we drove it just over 2000 miles on our 5-ski-weekends-in-5-weeks adventure. We thoroughly enjoyed skiing at Crested Butte, tailgating with powder at Mary Jane and shredding the double blacks at Telluride. I know we'll be back to all of them, some sooner than later.</p>
<p>In fact, I'll be meeting a few
    friends at Mary Jane this morning to attempt 30,000 vertical feet in one day. It'll be my 36th day of skiing this
    year. Happy Friday! Life is Good. <img src="http://raibledesigns.com/images/smileys/smile.gif" class="smiley" alt=":)" title=":)" />
</p>
