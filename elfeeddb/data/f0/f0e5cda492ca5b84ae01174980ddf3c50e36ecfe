<div xmlns='http://www.w3.org/1999/xhtml'><p>Since some time the Banana Pi is supported in the Linux kernel and <a href='http://git.denx.de/?p=u-boot.git;a=commit;h=3340eab26d89176dd0bf543e6d2590665c577423'>U-Boot</a>.
<a href='http://archlinuxarm.org/'>Arch Linux ARM</a> does not support the board yet, but it is possible to get it working with an upstream U-Boot.</p>

<h2 id='sd-card-image'>SD card Image</h2>

<p>First we to create an SD card image, since the BananaPi is also ARMv7 we can use the <a href='http://archlinuxarm.org/platforms/armv7/allwinner/cubieboard-2'>Cubieboard 2 installation instructions</a>
Follow the steps until step 6, “Install the U-Boot bootloader”. We will have to compile our own u-boot for the board.</p>

<h2 id='u-boot'>U-Boot</h2>

<p>The next step is creating a u-boot image.
Make sure you have the following packages installed on Arch Linux.</p>

<ul>
  <li>git</li>
  <li>arm-none-eabi-gcc # ARM cross compiler</li>
</ul>

<p>Now follow the following steps.</p>

<figure class='highlight'><pre><code class='language-bash'>git clone git://git.denx.de/u-boot.git
<span class='nb'>cd </span>u-boot
make -j4 <span class='nv'>ARCH</span><span class='o'>=</span>arm <span class='nv'>CROSS_COMPILE</span><span class='o'>=</span>arm-none-eabi- Bananapi_defconfig 
make -j4 <span class='nv'>ARCH</span><span class='o'>=</span>arm <span class='nv'>CROSS_COMPILE</span><span class='o'>=</span>arm-none-eabi-</code></pre></figure>

<p>If everything went fine you should have an U-Boot image: <strong>u-boot-sunxi-with-spl.bin</strong>. Now dd the image to your sdcard, where /dev/sdX is your sdcard.</p>

<figure class='highlight'><pre><code class='language-bash'>sudo dd <span class='k'>if</span><span class='o'>=</span>u-boot-sunxi-with-spl.bin <span class='nv'>of</span><span class='o'>=</span>/dev/sdX <span class='nv'>bs</span><span class='o'>=</span>1024 <span class='nv'>seek</span><span class='o'>=</span>8</code></pre></figure>

<p>Flush buffers</p>

<figure class='highlight'><pre><code class='language-bash'> sync</code></pre></figure>

<p>Now mount the sdcard again.</p>

<figure class='highlight'><pre><code class='language-bash'>mount /dev/sdX1 mnt
wget http://pkgbuild.com/~jelle/bananapi/boot.scr -O mnt/boot/boot.scr
umount /dev/sdX1
sync</code></pre></figure>

<h2 id='booting--serial'>Booting &amp; Serial</h2>

<p>Instructions on getting serial working are on the <a href='http://linux-sunxi.org/LeMaker_Banana_Pi#Locating_the_UART'>sunxi wiki</a>.</p>

  <p><a href='http://vdwaa.nl/archlinux/arm/bananapi/banana-pi-archlinux-arm/'>Arch Linux ARM on the BananaPi</a> was originally published by Jelle van der Waa at <a href='http://vdwaa.nl'>Jelly&apos;s Blog</a> on December 22, 2015.</p></div>
    