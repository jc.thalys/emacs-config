<p>
The concept of a <a href="https://github.com/Malabarba/lazy-map-clojure#the-lazy-map">lazy-map</a> might sounds odd at first. How do you know if a map
contains an entry without resolving the whole map? But it’s not the entries that
are lazy, it’s the values they hold. See this example from the Readme:
</p>

<figure class="highlight"><pre><code class="language-clojure" data-lang="clojure"><span class="n">user&gt;</span><span class="w"> </span><span class="p">(</span><span class="k">def</span><span class="w"> </span><span class="n">my-map</span><span class="w">
        </span><span class="p">(</span><span class="nf">lazy-map</span><span class="w"> </span><span class="p">{</span><span class="no">:cause</span><span class="w"> </span><span class="p">(</span><span class="nf">do</span><span class="w"> </span><span class="p">(</span><span class="nb">println</span><span class="w"> </span><span class="s">"Getting Cause"</span><span class="p">)</span><span class="w">
                              </span><span class="no">:major-failure</span><span class="p">)</span><span class="w">
                   </span><span class="no">:name</span><span class="w"> </span><span class="p">(</span><span class="nf">do</span><span class="w"> </span><span class="p">(</span><span class="nb">println</span><span class="w"> </span><span class="s">"Getting Name"</span><span class="p">)</span><span class="w">
                             </span><span class="s">"Some Name"</span><span class="p">)}))</span><span class="w">
</span><span class="o">#</span><span class="ss">'user/my-map</span><span class="w">

</span><span class="n">user&gt;</span><span class="w"> </span><span class="p">(</span><span class="no">:name</span><span class="w"> </span><span class="n">my-map</span><span class="p">)</span><span class="w">
</span><span class="n">Getting</span><span class="w"> </span><span class="n">Name</span><span class="w">
</span><span class="s">"Some Name"</span><span class="w">

</span><span class="n">user&gt;</span><span class="w"> </span><span class="p">(</span><span class="no">:name</span><span class="w"> </span><span class="n">my-map</span><span class="p">)</span><span class="w">
</span><span class="s">"Some Name"</span><span class="w">

</span><span class="n">user&gt;</span><span class="w"> </span><span class="p">(</span><span class="no">:cause</span><span class="w"> </span><span class="n">my-map</span><span class="p">)</span><span class="w">
</span><span class="n">Getting</span><span class="w"> </span><span class="n">Cause</span><span class="w">
</span><span class="no">:major-failure</span></code></pre></figure>

<p>
<a href="https://github.com/Malabarba/lazy-map-clojure#the-lazy-map">Lazy-map</a> is on Clojars, so you can just add it as a dep and play around:
</p>


<div class="figure">
<p><a href="https://clojars.org/malabarba/lazy-map"><img src="https://clojars.org/malabarba/lazy-map/latest-version.svg"></a>
</p>
</div>

<div id="outline-container-orgheadline1" class="outline-2">
<h2 id="orgheadline1">How would this ever be useful?</h2>
<div class="outline-text-2" id="text-orgheadline1">
<p>
One of my side projects a few months ago (when I was playing around with
Clojure-on-Android) was an Android client for a desktop Java application.
Because of this, a lot of the code was about interfacing with Java objects
defined by this package.  These objects were from many different classes, but
the one thing they had in common is that they were full of <code>get</code> methods.
</p>

<p>
I wanted so much to be able to use these objects as maps that I wrote <a href="https://github.com/Malabarba/lazy-map-clojure#the-to-lazy-map-protocol">a protocol</a>
for converting general objects to Clojure maps. Here’s an example of how it
worked on a string.
</p>
<figure class="highlight"><pre><code class="language-clojure" data-lang="clojure"><span class="n">user&gt;</span><span class="w"> </span><span class="p">(</span><span class="nf">to-map</span><span class="w"> </span><span class="s">"My own String!"</span><span class="p">)</span><span class="w">
</span><span class="p">{</span><span class="no">:to-char-array</span><span class="w"> </span><span class="o">#</span><span class="n">object</span><span class="p">[</span><span class="s">"[C"</span><span class="w"> </span><span class="mi">0</span><span class="n">xdf4ddb3</span><span class="w"> </span><span class="s">"[C@df4ddb3"</span><span class="p">]</span><span class="n">,</span><span class="w">
 </span><span class="no">:empty?</span><span class="w"> </span><span class="n">false,</span><span class="w">
 </span><span class="no">:to-string</span><span class="w"> </span><span class="s">"My own String!"</span><span class="n">,</span><span class="w">
 </span><span class="no">:chars</span><span class="w"> </span><span class="o">#</span><span class="n">object</span><span class="p">[</span><span class="n">java.util.stream.IntPipeline$Head</span><span class="w"> </span><span class="mi">0</span><span class="n">xad35343</span><span class="w"> </span><span class="s">"java.util.stream.IntPipeline$Head@ad35343"</span><span class="p">]</span><span class="n">,</span><span class="w">
 </span><span class="no">:class</span><span class="w"> </span><span class="n">java.lang.String,</span><span class="w">
 </span><span class="no">:length</span><span class="w"> </span><span class="mi">14</span><span class="n">,</span><span class="w">
 </span><span class="no">:trim</span><span class="w"> </span><span class="s">"My own String!"</span><span class="n">,</span><span class="w">
 </span><span class="no">:bytes</span><span class="w"> </span><span class="o">#</span><span class="n">object</span><span class="p">[</span><span class="s">"[B"</span><span class="w"> </span><span class="mi">0</span><span class="n">x75ef7d8f</span><span class="w"> </span><span class="s">"[B@75ef7d8f"</span><span class="p">]</span><span class="n">,</span><span class="w">
 </span><span class="no">:hash-code</span><span class="w"> </span><span class="mi">1673659170</span><span class="n">,</span><span class="w">
 </span><span class="no">:object</span><span class="w"> </span><span class="s">"My own String!"</span><span class="n">,</span><span class="w">
 </span><span class="no">:to-upper-case</span><span class="w"> </span><span class="s">"MY OWN STRING!"</span><span class="p">}</span></code></pre></figure>
<p>
For comparison, here’s how <code>bean</code> works (a similar function from
<code>clojure.core</code>).
</p>
<figure class="highlight"><pre><code class="language-clojure" data-lang="clojure"><span class="n">user&gt;</span><span class="w"> </span><span class="p">(</span><span class="nb">bean</span><span class="w"> </span><span class="s">"My own String!"</span><span class="p">)</span><span class="w">
</span><span class="p">{</span><span class="no">:bytes</span><span class="w"> </span><span class="o">#</span><span class="n">object</span><span class="p">[</span><span class="s">"[B"</span><span class="w"> </span><span class="mi">0</span><span class="n">x1ad60072</span><span class="w"> </span><span class="s">"[B@1ad60072"</span><span class="p">]</span><span class="n">,</span><span class="w">
 </span><span class="no">:class</span><span class="w"> </span><span class="n">java.lang.String,</span><span class="w">
 </span><span class="no">:empty</span><span class="w"> </span><span class="n">false</span><span class="p">}</span></code></pre></figure>

<p>
The protocol is actually quite smart. It uses a number of heuristics to only
convert methods that look like they’re side-effect free. Of course, it’s not
foolproof (this <i>is</i> Java we’re talking about), but the macro used to extend the
protocol lets you specify methods to exclude.
</p>

<p>
The only problem was the performance cost. Some of these methods were very
expensive to run, and eagerly calling all methods of all objects just so I could
later access some of these was obviously a bad deal. The solution, clearly, was
to only call these methods when the map entries were actually accessed. And so
<code>lazy-map</code> was born.
</p>
</div>
</div>

   <p><a href="http://endlessparentheses.com/new-clojure-lib-lazy-map.html?source=rss#disqus_thread">Comment on this.</a></p>