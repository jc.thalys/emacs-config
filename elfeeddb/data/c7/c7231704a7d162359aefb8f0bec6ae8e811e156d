<p><a href="http://sachachua.com/blog/wp-content/uploads/2017/04/2017-04-03a-March-2017-monthly-review.png"><img class="alignnone size-medium wp-image-28963" src="http://sachachua.com/blog/wp-content/uploads/2017/04/2017-04-03a-March-2017-monthly-review-640x384.png" alt="" width="640" height="384" srcset="http://sachachua.com/blog/wp-content/uploads/2017/04/2017-04-03a-March-2017-monthly-review-640x384.png 640w, http://sachachua.com/blog/wp-content/uploads/2017/04/2017-04-03a-March-2017-monthly-review-280x168.png 280w, http://sachachua.com/blog/wp-content/uploads/2017/04/2017-04-03a-March-2017-monthly-review-768x461.png 768w, http://sachachua.com/blog/wp-content/uploads/2017/04/2017-04-03a-March-2017-monthly-review.png 1500w" sizes="(max-width: 640px) 100vw, 640px" /></a></p>
<p>W- has been back at work for a little over a month, and we&#8217;ve settled into comfortable routines. I focus on A- during the day, and we reconnect with W- when he gets home. After a bit of playtime, we settle down for her afternoon nap. He makes dinner or pulls it out of the fridge, we eat together, and I clean up while he plays with A-. Then we have some more play time before we give A- her evening snack and do our bedtime routines. W- sleeps early so that he can get to work before the morning rush, and I stay up late so that I can have some discretionary time for my own things.</p>
<p>We found out that A- has enamel hypoplasia, so brushing her teeth is a regular part of our routines. I took her to get a new conformer for her eye, and we scheduled the follow-up exams for her liver and heart in August. I didn&#8217;t hear back about her blood test results, so I guess her iron levels are fine. I ordered a textbook on ocular prosthetics, too, and I&#8217;m excited to read all the medical details. We saw someone from Surrey Place who reassured me that A- will probably be totally all right with monocular vision. Our Healthy Babies Healthy Children nurse went through the 12-month developmental checklists, and our home visitor gave us tips for language development.</p>
<p>In addition to A-&#8216;s health-related appointments, we&#8217;ve been filling our days with various parenting workshops and early years drop-ins. I picked up a few tips on school accommodations and visual routines from the Let&#8217;s Get Started series, and I&#8217;ve also been enjoying the parent advocacy workshop. We&#8217;ve been regularly going to the Junction Family Resource Centre and the Parenting and Family Literacy Centre, and we made it out to the JFRC&#8217;s sensory play day too. I took A- to the High Park zoo, and we&#8217;ve started a new habit of going to the Royal Ontario Museum. (Sometimes with other people!) I find spending time with her to be surprisingly enjoyable, and I&#8217;m looking forward to helping her learn.</p>
<p>A- has been learning a lot about language. She added more variety to her babbling: nai, ha, pa, da, and ga. She now recognizes words for a few body parts (head, knees, toes, belly button), mittens, socks, and &#8220;all done.&#8221;</p>
<p>She&#8217;s also been working on her motor skills. She can crawl up and down stairs with supervision, although she usually needs a reminder to go down legs first instead of taking the direct approach. She likes practising taking off and putting on jackets, socks, and mittens with help. She&#8217;s interested in chopsticks, spoons, forks, whisks, and measuring cups. She eats crackers and bits of bagels. She pushes toys along while crawling. She can usually connect 2&#215;2 pieces of Duplo if they&#8217;re facing the right way. She enjoys sprinkling rock salt and herbs on things we&#8217;re cooking.</p>
<p>It&#8217;s a lot of fun spending time with A-, and I&#8217;ve been thinking about how to share that experience with my parents. After lots of planning, we decided it&#8217;s best to hold off on flying to the Philippines until W- can come along. In the meantime, we&#8217;ve been doing video chats. She&#8217;s still getting used to their faces and voices, but it&#8217;s a good first step.</p>
<p>Speaking of family, I took some time to make a GEDCOM export of A-&#8216;s family tree, taking advantage of the data entered by my mom and by W-. Might be interesting for her later, so it&#8217;s good to have a personal copy instead of relying on a web service to continue. I also set up Owncloud and borgbackup to improve my backup routines, and I finally set up a VM that makes it easy to confirm if my blog backups work. (They do. Whew!)</p>
<p>Lots of baking this month: a few iterations of roast potatoes, blondies, muffins, and cabbage rolls. We got the roast potatoes down pat, but there seems to be some variability in the quality of russet potatoes we get, so we&#8217;re looking into that too.</p>
<p>The new routines have been working out pretty well for discretionary time. I&#8217;ve been focusing on small improvements and personal tasks instead of increasing my consulting workload. The backups I mentioned above had been on my list a long time! I also did preliminary tax prep, although we&#8217;re still waiting for J- to do her numbers so that she can transfer tuition tax credits to W-. I sorted out my Ledger, learned the new features in ledger-mode, and added net worth and stock allocation reports. In the process, I discovered an unpaid invoice and got payment, yay! In terms of consulting, I deployed some code to add priorities and categorization, and I helped with an add-on that I&#8217;ve turned over to the other team. I even had time to watch a couple of movies.</p>
<p>If the rest of the year is much like this month, I think it&#8217;ll be pretty good! :)</p>
<p><b>Blog posts</b></p>
<ul class="org-ul">
<li>Emacs News
<ul class="org-ul">
<li><a href="http://sachachua.com/blog/2017/03/2017-02-27-emacs-news/">2017-02-27 Emacs News</a></li>
<li><a href="http://sachachua.com/blog/2017/03/2017-03-06-emacs-news/">2017-03-06 Emacs news</a></li>
<li><a href="http://sachachua.com/blog/2017/03/2017-03-13-emacs-news/">2017-03-13 Emacs news</a></li>
<li><a href="http://sachachua.com/blog/2017/03/2017-03-20-emacs-news/">2017-03-20 Emacs news</a></li>
<li><a href="http://sachachua.com/blog/2017/03/2017-03-27-emacs-news/">2017-03-27 Emacs news</a></li>
</ul>
</li>
<li>Journal
<ul class="org-ul">
<li><a href="http://sachachua.com/blog/2017/03/monthly-review-february-2017/">Monthly review: February 2017</a></li>
<li><a href="http://sachachua.com/blog/2017/03/weekly-review-week-ending-february-24-2017/">Weekly review: Week ending February 24, 2017</a></li>
<li><a href="http://sachachua.com/blog/2017/03/weekly-review-week-ending-march-3-2017/">Weekly review: Week ending March 3, 2017</a></li>
<li><a href="http://sachachua.com/blog/2017/03/weekly-review-week-ending-march-10-2017/">Weekly review: Week ending March 10, 2017</a></li>
<li><a href="http://sachachua.com/blog/2017/03/weekly-review-week-ending-march-17-2017/">Weekly review: Week ending March 17, 2017</a></li>
<li><a href="http://sachachua.com/blog/2017/03/weekly-review-week-ending-march-24-2017/">Weekly review: Week ending March 24, 2017</a></li>
</ul>
</li>
<li><a href="http://sachachua.com/blog/2017/03/what-did-i-learn-from-this-experiment-with-semi-retirement/">What did I learn from this experiment with semi-retirement?</a></li>
<li><a href="http://sachachua.com/blog/2017/03/notes-from-the-lets-get-started-parenting-series/">Notes from the Let&#8217;s Get Started parenting series</a></li>
</ul>
<p><b>Sketches</b></p>
<ul class="org-ul">
<li>Journal:
<ul class="org-ul">
<li><a href="https://www.flickr.com/photos/sachac/32376344684/">2017-03-01a Week ending 2017-02-24 #journal #weekly</a></li>
<li><a href="https://www.flickr.com/photos/sachac/32915888550/">2017-03-05b February 2017 #monthly #review</a></li>
<li><a href="https://www.flickr.com/photos/sachac/33102935612/">2017-03-05a Week ending 2017-03-03 #journal #weekly</a></li>
<li><a href="https://www.flickr.com/photos/sachac/32607988064/">2017-03-13a Week ending 2017-03-10 #journal #weekly</a></li>
<li><a href="https://www.flickr.com/photos/sachac/32811537034/">2017-03-23a Week ending 2017-03-17 #journal #weekly</a></li>
<li><a href="https://www.flickr.com/photos/sachac/33767227565/">2017-03-26a Week ending 2017-03-24 #journal #weekly</a></li>
<li><a href="https://www.flickr.com/photos/sachac/33671220882/">2017-04-01a Week ending 2017-03-31 #journal #weekly</a></li>
<li>Daily: <a href="https://www.flickr.com/photos/sachac/32875321730/">01</a> <a href="https://www.flickr.com/photos/sachac/32443743893/">02</a> <a href="https://www.flickr.com/photos/sachac/33129854281/">03</a> <a href="https://www.flickr.com/photos/sachac/33102473792/">04</a> <a href="https://www.flickr.com/photos/sachac/32548472884/">05</a> <a href="https://www.flickr.com/photos/sachac/32548473024/">06</a> <a href="https://www.flickr.com/photos/sachac/33008431970/">07</a> <a href="https://www.flickr.com/photos/sachac/32548473044/">08</a> <a href="https://www.flickr.com/photos/sachac/32548472714/">09</a> <a href="https://www.flickr.com/photos/sachac/33008432300/">10</a> <a href="https://www.flickr.com/photos/sachac/33391421455/">11</a> <a href="https://www.flickr.com/photos/sachac/32607988084/">12</a> <a href="https://www.flickr.com/photos/sachac/33428046256/">13</a> <a href="https://www.flickr.com/photos/sachac/33468804475/">14</a> <a href="https://www.flickr.com/photos/sachac/32625292444/">15</a> <a href="https://www.flickr.com/photos/sachac/33388207182/">16</a> <a href="https://www.flickr.com/photos/sachac/32730612183/">17</a> <a href="https://www.flickr.com/photos/sachac/33504218506/">18</a> <a href="https://www.flickr.com/photos/sachac/32730611893/">19</a> <a href="https://www.flickr.com/photos/sachac/33600544765/">20</a> <a href="https://www.flickr.com/photos/sachac/33217401060/">21</a> <a href="https://www.flickr.com/photos/sachac/33471661531/">22</a> <a href="https://www.flickr.com/photos/sachac/33497657582/">23</a> <a href="https://www.flickr.com/photos/sachac/33654191605/">24</a> <a href="https://www.flickr.com/photos/sachac/33525008491/">25</a> <a href="https://www.flickr.com/photos/sachac/33767227425/">26</a> <a href="https://www.flickr.com/photos/sachac/33610533372/">27</a> <a href="https://www.flickr.com/photos/sachac/33382607760/">28</a> <a href="https://www.flickr.com/photos/sachac/33767227435/">29</a> <a href="https://www.flickr.com/photos/sachac/33382607650/">30</a> <a href="https://www.flickr.com/photos/sachac/32954146533/">31</a></li>
</ul>
</li>
<li><a href="https://www.flickr.com/photos/sachac/33216919156/">2017-03-03a Reflecting on new routines as a stay-at-home parent #routines #parenting</a></li>
<li><a href="https://www.flickr.com/photos/sachac/33216919776/">2017-03-03b Magic wand thinking &#8211; sitter #parenting #delegation</a></li>
<li><a href="https://www.flickr.com/photos/sachac/33102473822/">2017-03-03c Increasing focused time opportunities #parenting #time</a></li>
<li><a href="https://www.flickr.com/photos/sachac/33350917906/">2017-03-09a Adapting to A-&#8216;s vision &#8211; 1 year old #microphthalmia #monocular #vision</a></li>
<li><a href="https://www.flickr.com/photos/sachac/33263652801/">2017-03-09b Adapting to A-&#8216;s vision &#8211; 1-2 years #microphthalmia #vision #monocular #planning</a></li>
<li><a href="https://www.flickr.com/photos/sachac/33613512736/">2017-03-17a Possible trip timeline &#8211; layover in ICN #travel #planning</a></li>
</ul>
<p><b>Time</b></p>
<table border="2" frame="hsides" rules="groups" cellspacing="0" cellpadding="6">
<colgroup>
<col class="org-left">
<col class="org-right">
<col class="org-right">
<col class="org-right">
<col class="org-right">
<col class="org-right"> </colgroup>
<thead>
<tr>
<th class="org-left" scope="col">Category</th>
<th class="org-right" scope="col">Feb</th>
<th class="org-right" scope="col">March</th>
<th class="org-right" scope="col">Diff</th>
<th class="org-right" scope="col">h/wk</th>
<th class="org-right" scope="col">Diff h/wk</th>
</tr>
</thead>
<tbody>
<tr>
<td class="org-left">Unpaid work &#8211; Childcare</td>
<td class="org-right">41.0</td>
<td class="org-right">44.3</td>
<td class="org-right">3.3</td>
<td class="org-right">82.2</td>
<td class="org-right">5.5</td>
</tr>
<tr>
<td class="org-left">Unpaid work</td>
<td class="org-right">48.3</td>
<td class="org-right">50.6</td>
<td class="org-right">2.3</td>
<td class="org-right">94.0</td>
<td class="org-right">3.9</td>
</tr>
<tr>
<td class="org-left">Discretionary &#8211; Productive</td>
<td class="org-right">2.2</td>
<td class="org-right">3.2</td>
<td class="org-right">1.1</td>
<td class="org-right">6.0</td>
<td class="org-right">1.8</td>
</tr>
<tr>
<td class="org-left">Discretionary &#8211; Social</td>
<td class="org-right">0.3</td>
<td class="org-right">0.6</td>
<td class="org-right">0.4</td>
<td class="org-right">1.2</td>
<td class="org-right">0.6</td>
</tr>
<tr>
<td class="org-left">Business &#8211; Build</td>
<td class="org-right">0.1</td>
<td class="org-right">0.1</td>
<td class="org-right">0.1</td>
<td class="org-right">0.3</td>
<td class="org-right">0.1</td>
</tr>
<tr>
<td class="org-left">Business &#8211; Connect</td>
<td class="org-right">0.0</td>
<td class="org-right">0.0</td>
<td class="org-right">-0.0</td>
<td class="org-right">0.0</td>
<td class="org-right">-0.0</td>
</tr>
<tr>
<td class="org-left">Business &#8211; Earn</td>
<td class="org-right">0.9</td>
<td class="org-right">0.8</td>
<td class="org-right">-0.0</td>
<td class="org-right">1.6</td>
<td class="org-right">-0.1</td>
</tr>
<tr>
<td class="org-left">Discretionary &#8211; Play</td>
<td class="org-right">1.0</td>
<td class="org-right">0.8</td>
<td class="org-right">-0.2</td>
<td class="org-right">1.6</td>
<td class="org-right">-0.3</td>
</tr>
<tr>
<td class="org-left">Discretionary &#8211; Family</td>
<td class="org-right">1.5</td>
<td class="org-right">0.5</td>
<td class="org-right">-1.0</td>
<td class="org-right">1.0</td>
<td class="org-right">-1.6</td>
</tr>
<tr>
<td class="org-left">Personal</td>
<td class="org-right">9.9</td>
<td class="org-right">8.7</td>
<td class="org-right">-1.2</td>
<td class="org-right">16.2</td>
<td class="org-right">-2.0</td>
</tr>
<tr>
<td class="org-left">Sleep</td>
<td class="org-right">35.9</td>
<td class="org-right">34.3</td>
<td class="org-right">-1.6</td>
<td class="org-right">63.7</td>
<td class="org-right">-2.7</td>
</tr>
</tbody>
</table>
<p><a href="http://sachachua.com/blog/wp-content/uploads/2017/04/output-1.png"><img class="alignnone size-medium wp-image-28964" src="http://sachachua.com/blog/wp-content/uploads/2017/04/output-1-640x576.png" alt="" width="640" height="576" srcset="http://sachachua.com/blog/wp-content/uploads/2017/04/output-1-640x576.png 640w, http://sachachua.com/blog/wp-content/uploads/2017/04/output-1-222x200.png 222w, http://sachachua.com/blog/wp-content/uploads/2017/04/output-1-768x691.png 768w" sizes="(max-width: 640px) 100vw, 640px" /></a></p>
