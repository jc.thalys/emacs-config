
<p>Data encapsulation is a central tenet in object-oriented style. This says that the
  fields of an object should not be exposed publicly, instead all access from outside the
  object should be via accessor methods (getters and setters). There are languages that
  allow publicly accessible fields, but we usually caution programmers not to do this.
  <i>Self-encapsulation</i> goes a step further, indicating that all <i>internal</i>
  access to a data field should also go through accessor methods as well. Only the
  accessor methods should touch the data value itself. If the data field isn't exposed to
  the outside, this will mean adding additional private accessors.</p>

<p>Here's an example of a reasonably encapsulated java class</p>

<p class="code-label">class Charge...
</p>

<pre>  private int units;
  private double rate;

  public Charge(int units, double rate) {
    this.units = units;
    this.rate = rate;
  }
  public int getUnits() { return units; }
  public Money getAmount() { return Money.usd(units * rate); }
</pre>

<p>Both fields are immutable. The units field is exposed to clients of the class via a
  getter, but the rate field is only used internally, so doesn't need a getter.</p>

<p>Here is a version using self-encapsulation.</p>

<p class="code-label">class ChargeSE...
</p>

<pre>  private int units;
  private double rate;

  public ChargeSE(int units, double rate) {
    this.units = units;
    this.rate = rate;
  }
  public int getUnits()    { return units; }
<span class="highlight">  private double getRate() { return rate; }</span>
  public Money getAmount() { return Money.usd(<span class="highlight">getUnits()</span> * <span class="highlight">getRate()</span>); }
</pre>

<p>Self encapsulaton means that <code>getAmount</code> needs to access both fields
  through getters. This also means I have to add a getter for <code>rate</code>, which I
  should make private.</p>

<p>Encapsulating mutable data is generally a good idea. Update functions can contain
  code to execute validations and consequential logic. By restricting access through
  functions, we can support the <a href="https://martinfowler.com/bliki/UniformAccessPrinciple.html">UniformAccessPrinciple</a>, allowing us to hide
  which data is computed and which is stored. These accessors allow us to modify the data
  structures while retaining the same public interface. Different languages differ in
  details of what is "outside" for an object by various kinds of
  <a href="https://martinfowler.com/bliki/AccessModifier.html">AccessModifier</a>, but most environments support data encapsulation to some
  degree.</p>

<p>I've come across a few organizations that mandated self-encapsulation, and whether to
  use it or not was a regular topic of debate since the 90's. Its advocates said that
  encapsulation was such a benefit, that you wanted to incorporate it to internal access
  too. Critics argued that it was unnecessary ceremony leading to unnecessary code that
  obscured what was going on.</p>

<p>My view on this is that most of the time there's little value in self-encapsulation.
  The value of encapsulation is proportional to the scope of the data access. Classes are
  usually small (at least mine are) so direct access isn't going to be an issue within
  that scope. Most accessors are simple assignments for the setter and retrieval for the
  getter, so there's little value in using them internally.</p>

<p>But there are common circumstances where self-encapsulation is worth the effort. If there is
  logic in the setter, then it's wise to consider it for any internal updates too. Another
  circumstance is when the class is part of an inheritance structure, in which case the
  accessors provide valuable hook points for subclasses to override behavior.</p>

<p>So my usual first move is to use direct access to fields, but refactor using <a href="http://www.refactoring.com/catalog/selfEncapsulateField.html">Self Encapsulate
  Field</a> should circumstances demand it. Often the forces that lead me to
  consider self-encapsulation I can resolve by <a href="//refactoring.com/catalog/extractClass.html">extracting a new class</a>.</p>

<div class="furtherReading">
<h2>Further Reading</h2>

<p>Kent Beck discusses these trade-offs under the names Direct Access and
    Indirect Access in both <a href="https://www.amazon.com/gp/product/0321413091?ie=UTF8&amp;tag=martinfowlerc-20&amp;linkCode=as2&amp;camp=1789&amp;creative=9325&amp;creativeASIN=0321413091">Implementation Patterns</a><img src="https://www.assoc-amazon.com/e/ir?t=martinfowlerc-20&amp;l=as2&amp;o=1&amp;a=0321601912" width="1" height="1" border="0" alt="" style="width: 1px !important; height: 1px !important; border:none !important; margin:0px !important;"> and
    <a href="https://www.amazon.com/gp/product/013476904X?ie=UTF8&amp;tag=martinfowlerc-20&amp;linkCode=as2&amp;camp=1789&amp;creative=9325&amp;creativeASIN=013476904X">Smalltalk Best Practice Patterns</a><img src="https://www.assoc-amazon.com/e/ir?t=martinfowlerc-20&amp;l=as2&amp;o=1&amp;a=0321601912" width="1" height="1" border="0" alt="" style="width: 1px !important; height: 1px !important; border:none !important; margin:0px !important;"></p>
</div>

<div class="acknowledgements">
<h2>Acknowledgements</h2>

    
    Ian Cartwright, Matteo Vaccari, and Philip Duldig

    commented on drafts of this post
    
  </div>

<div class="translations"><b>Translations: </b><a href="https://www.itran.cc/2017/03/17/zi-feng-zhuang-selfencapsulation-martin-fowlerbo-ke-2/">Chinese</a></div>

<div class="shares">
<div class="icons"><span class="label">Share:</span><a href="https://twitter.com/intent/tweet?url=https://martinfowler.com/bliki/SelfEncapsulation.html&amp;text=Bliki:%20SelfEncapsulation%20%E2%9E%99%20" title="Share on Twitter"><img src="/t_mini-a.png"></a><a href="https://facebook.com/sharer.php?u=https://martinfowler.com/bliki/SelfEncapsulation.html" title="Share on Facebook"><img src="/fb-icon-20.png"></a><a href="https://plus.google.com/share?url=https://martinfowler.com/bliki/SelfEncapsulation.html" title="Share on Google Plus"><img src="/gplus-16.png"></a></div>

<div class="comment">if you found this article useful, please share it. I appreciate the feedback and encouragement</div>
</div>

<div class="clear"></div>
