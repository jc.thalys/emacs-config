<p>Yesterday, I received a message from a friend, asking about how to become a programmer. It's not the first time I've been asked this. In fact, this summer I've been asked by several friends how to get into the field. It seems that as people grow older, they see the lifestyle of working remotely and enjoying their job as an attractive thing to do. In yesterday's case, this friend is a mom that now has her days free because all her kids are in school. Here's what she wrote:
</p>
<p class="quote">
Now that my girls are both in school full day, I've been thinking about taking some programming classes. It's something I started to do while I was working at [ABC Company], but obviously didn't pursue once I quit to have kids. I'm thinking of getting my MIS in web development or specializing in designing apps if that's even a thing? Anyway, what languages would you recommend I concentrate on? JavaScript, Python? Lastly, is there a particular school you would recommend? I can't afford DU on my stay-at-home-mom salary, or even Regis which is where I started when I was getting tuition reimbursement. I was hoping I could do most of my education online while the kids are in school? Any advice or words of wisdom would be greatly appreciated! </p>
<p>Since this is a common question I see, I figured I'd publish my answers here, and get some advice from y'all too. Here's my response:</p>
<div class="quote">
<p style="margin-top: 0">
Python would definitely be good, as would JavaScript. JavaScript can be done on the client and server these days, so you could do that and be able to do front-end and backend development. 
</p>
<p>
For programming specifically, I've heard these guys have a good JavaScript course: <a href="https://www.codecademy.com/">https://www.codecademy.com</a>. Here's how to get started with Python in eight weeks: <a href="http://lifehacker.com/how-i-taught-myself-to-code-in-eight-weeks-511615189">http://lifehacker.com/how-i-taught-myself-to-code-in-eight-weeks-511615189</a>. And one of my favorites: <a href="http://programming-motherfucker.com/become.html">http://programming-motherfucker.com/become.html</a>.
</p>
<p>
I've taken a Scala course from Coursera, it was hard and intense, but I learned a lot. They have lots of courses and give you certifications you can put on your LinkedIn profile: <a href="https://www.coursera.org/">https://www.coursera.org</a>.
</p>
<p>
I've also recommended <a href="https://teamtreehouse.com">https://teamtreehouse.com</a> to folks and <a href="https://www.khanacademy.org/">https://www.khanacademy.org</a> has always been good, even for kids.</p>
<p style="margin-bottom: 0">
Ultimately, the best way to learn to code is by doing. It's definitely good to study, learn and practice, but it'll probably won't sink in and become real knowledge until you're getting paid to do it. With the plethora of high-priced programmers out there, you can likely find a junior position, show a willingness to learn and come up to speed quickly. If you can couple that with a remote position, I think you'll really enjoy yourself.</p>
</div>
<p>Her response was interesting, as she thought she might need a <abbr title="Computer Science">CS</abbr> degree to even get a programming job.</p>
<p class="quote">
Coincidentally I looked over many of these coding sites yesterday but wasn't sure if I needed an accredited diploma. It sounds like it's more important that I just get some experience.</p>
<p>From my experience, a college degree matters, but not a CS degree. I told her people skills make programmers stand out and she's a witty person that certainly has those. What's your advice as a programmer? What would you tell people to do if they want to break into the field?</p>
<p>More importantly, if you're on the hiring side, what would it take for you to hire a 40-something person with no programming background? If they've been studying for six months and have really good people skills, would you hire them for a junior position?</p>