<ul class="org-ul">
<li>Emacs development:
<ul class="org-ul">
<li><a href="https://lists.gnu.org/archive/html/emacs-devel/2017-04/msg00619.html">Emacs 25.2 released</a> (<a href="https://www.reddit.com/r/emacs/comments/66q9wx/emacs_252_released/">Reddit</a>, <a href="https://news.ycombinator.com/item?id=14173332">HN</a>, <a href="https://news.ycombinator.com/item?id=14169797">HN also</a>)</li>
<li><a href="http://lists.gnu.org/archive/html/emacs-devel/2017-04/msg00520.html">Continued work on libnettle/libhogweed WIP</a></li>
</ul>
</li>
<li>Emacs Lisp:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/673wek/emacs_bankruptcy_and_structure/">.emacs bankruptcy and structure</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/66gh2q/elisp_for_scripting/">elisp for scripting</a></li>
</ul>
</li>
<li>Org Mode:
<ul class="org-ul">
<li><a href="https://github.com/alphapapa/helm-org-rifle#changelog">helm-org-rifle 1.4.0 released</a></li>
<li><a href="http://github.com/alphapapa/org-sticky-header">org-sticky-header: Show off-screen Org heading in header line</a></li>
<li><a href="https://github.com/scallywag/org-board">org board</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/66w75c/monospace_font_for_calendar_in_orgmode/">Monospace font for Calendar in org-mode, proportional font in main buffer</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/66v3zs/agenda_view_with_random_sorting_of_the_entries/">Agenda view with random sorting of the entries</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/66b82k/some_questions_about_a_songwriting_workflow_in/">Some questions about a songwriting workflow in emacs.</a></li>
<li><a href="http://www.accidentalrebel.com/blog/2017/04/19/converting-org-journal-entry-to-org-page-post/">Converting org-journal entry to org-page post</a></li>
</ul>
</li>
<li>Coding:
<ul class="org-ul">
<li><a href="https://magit.vc/manual/magit/MacOS-Performance.html">Patch Emacs to speedup Magit on macOS</a> (<a href="https://www.reddit.com/r/emacs/comments/66o45w/patch_emacs_to_speedup_magit_on_macos/">Reddit</a>)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/66sj2z/n00b_question_how_do_i_get_a_shebang_copied_into/">Inserting shebangs in files</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/66qvir/can_i_have_line_numbers_only_show_up_in_the/">Can I have line numbers only show up in the active (focused) window?</a></li>
<li><a href="https://www.youtube.com/watch?v=T1WBsI3gdDE">Productive Emacs: Paredit</a> (19:03, from 2016) &#8211; hat-tip to <a href="http://irreal.org/blog/?p=6158">Irreal</a> for the link</li>
</ul>
</li>
<li>Other:
<ul class="org-ul">
<li><a href="https://spacemacs.org">Try Spacemacs directly in the browser!</a> (<a href="https://www.reddit.com/r/emacs/comments/66locu/try_spacemacs_directly_in_the_browser/">Reddit</a>)</li>
<li><a href="http://emacspeak.blogspot.com/2017/04/mail-on-emacspeak-audio-desktop.html">Mail On The emacspeak Audio Desktop</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/66y6xg/how_to_rebind_the_meta_key_for_emacs_only/">How to rebind the meta key for emacs only?</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/66ubv8/tmuxlike_behavior_in_emacs/">tmux-like behavior in Emacs</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/66ln5d/copying_large_files_in_dired/">Copying large files in dired</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/66kdfb/state_of_spell_checking_in_emacs_for_non_english/">State of spell checking in emacs for non English writers</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/66fs1e/i_want_to_try_using_a_foot_pedal_for_pressing_the/">Foot pedal recommendations</a></li>
</ul>
</li>
<li>New packages:
<ul class="org-ul">
<li><a href="http://melpa.org/#/binclock" target="_blank" rel="noopener noreferrer">binclock</a>: Display the current time using a binary clock.</li>
<li><a href="http://melpa.org/#/exsqlaim-mode" target="_blank" rel="noopener noreferrer">exsqlaim-mode</a>: Use variables inside sql queries</li>
<li><a href="http://melpa.org/#/flatui-dark-theme" target="_blank" rel="noopener noreferrer">flatui-dark-theme</a>: Dark color theme with colors from <a href="https://flatuicolors.com/">https://flatuicolors.com/</a></li>
<li><a href="http://melpa.org/#/flow-minor-mode" target="_blank" rel="noopener noreferrer">flow-minor-mode</a>: Flow type mode based on web-mode.</li>
<li><a href="http://melpa.org/#/frame-mode" target="_blank" rel="noopener noreferrer">frame-mode</a>: Use frames instead of windows</li>
<li><a href="http://melpa.org/#/lsp-go" target="_blank" rel="noopener noreferrer">lsp-go</a>: Go support for lsp-mode</li>
<li><a href="http://melpa.org/#/lsp-haskell" target="_blank" rel="noopener noreferrer">lsp-haskell</a>: Haskell support for lsp-mode</li>
<li><a href="http://melpa.org/#/lsp-java" target="_blank" rel="noopener noreferrer">lsp-java</a>: Java support for lsp-mode</li>
<li><a href="http://melpa.org/#/lsp-python" target="_blank" rel="noopener noreferrer">lsp-python</a>: Python support for lsp-mode</li>
<li><a href="http://melpa.org/#/lsp-rust" target="_blank" rel="noopener noreferrer">lsp-rust</a>: Rust support for lsp-mode&#8221;</li>
</ul>
</li>
</ul>
<p>Links from <a href="http://reddit.com/r/emacs/new">reddit.com/r/emacs</a>, <a href="http://reddit.com/r/orgmode">/r/orgmode</a>, <a href="http://reddit.com/r/spacemacs">/r/spacemacs</a>, <a href="https://hn.algolia.com/?query=emacs&amp;sort=byDate&amp;prefix&amp;page=0&amp;dateRange=all&amp;type=story">Hacker News</a>, <a href="http://planet.emacsen.org">planet.emacsen.org</a>, <a href="https://www.youtube.com/results?search_query=emacs&amp;search_sort=video_date_uploaded">Youtube</a>, the changes to the <a href="http://git.savannah.gnu.org/cgit/emacs.git/log/etc/NEWS">Emacs NEWS file</a>, and <a href="http://lists.gnu.org/archive/html/emacs-devel/2017-04">emacs-devel</a>.</p>
<p><a href="http://sachachua.com/blog/category/emacs-news">Past Emacs News round-ups</a></p>
