<p><img src="http://static.appfuse.org/images/appfuse-icon.gif" class="picture" style="border: 0; margin-top: -25px">
In mid-February, I decided to stop working on <a href="https://github.com/appfuse/appfuse">AppFuse</a>. My reason was simple: I was no longer getting any value from my contributions to the project. I sent <a href="http://appfuse.547863.n4.nabble.com/New-version-of-AppFuse-td4657900.html">a message</a> to the developers mailing list the next day:</p>
<blockquote class="quote" style="margin-left: 20px">
<p style="margin-top: 0">Hello everyone, </p>
<p>
Last night, I started working on AppFuse 4.0, with the following features from the roadmap: </p>
<ul>
       <li>Remove XML wherever possible</li>
       <li>Java 8</li>
       <li>Spring Boot</li>
       <li>Spring Data</li>
       <li>JSR 303 (might require removing or developing client-side support)</li>
</ul>
<p>
As I started removing XML and integrating Spring Boot and Spring Data, it quickly became apparent that it’d be a lot of work to make all of these changes. My guess is it’d take over 100 hours of my time to do everything. This is time I’d be taking away from my family and personal time. 
</p>
<p>
At the end of last year, I wanted to make AppFuse 4.0 happen because I thought it’d help me stay up-to-date with Java technologies and learn some things along the way. As I dug into the codebase last night, I realized it’d be more of a headache than a learning experience. It seems there would be little reward for all the work. 
</p>
<p>
Because there’s little-to-no activity on the mailing list these days, it seems like it’s the right time to shutdown the project and dedicate my free time to other open source endeavors. As you might know, I’m a big fan of JHipster (<a href="http://jhipster.github.io/">http://jhipster.github.io/</a>). It combines AngularJS and Spring Boot and has all the features that AppFuse has - but with a more modern technology stack. 
</p>
<p>
If we had everything hosted on GitHub, I think it’d make sense to add a line to the README that says “This project is no longer maintained”. However, since there’s a lot hosted on appfuse.org (with Confluence), it might not be that easy. Maybe it’s possible to export everything from Confluence to static HTML pages and host them somewhere with the same URLs so there’s not a bunch of 404s from shutting down the project. 
</p>
<p>
Thank you for your contributions over the years. AppFuse was pretty cool back in the day, but now there’s better solutions. 
</p>
<p>
Cheers, 
</p>
<p style="margin-bottom: 0">
Matt</p>
</blockquote>
<p>
The good news is I've worked out a deal with <a href="http://contegix.com">Contegix</a> to keep appfuse.org up and running for the next year. The <a href="http://demo.appfuse.org">demos</a>, <a href="http://appfuse.org">documentation</a> and <a href="http://issues.appfuse.org">bug tracker</a> will be available until April 30, 2017. Bamboo and FishEye will be discontinued in the next week since they're too memory intensive for a smaller server. I'd love to figure out a way to export all the documentation from Confluence to Asciidoctor so everything can be on GitHub for years to come. However, there's something to be said for just letting a project fade away rather than holding onto nostalgic artifacts.</p>
<p>On a related note, <a href="https://community.oracle.com/community/java/javanet-forge-sunset">Java.net will be closing in a year from today</a>. AppFuse started <a href="https://sourceforge.net/projects/struts/files/appfuse/">on SourceForge</a>, but moved to <a href="http://appfuse.java.net">appfuse.java.net</a> shortly after. Today, the only thing left on java.net are AppFuse's mailing lists. I suppose it makes sense that both projects will cease to exist around the same time. </p>
<p>AppFuse's <a href="https://github.com/appfuse">source code will remain on GitHub</a>. I have no plans to delete it.</p>
<p>Thanks to everyone that used and contributed to AppFuse over the years. It was a pretty wild and crazy ride from 2003-2007! <img src="http://raibledesigns.com/images/smileys/smile.gif" class="smiley" alt=":)" title=":)" /></p>