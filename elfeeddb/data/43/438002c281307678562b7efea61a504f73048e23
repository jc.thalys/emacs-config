<p><b>A note from the editors:</b> We’re pleased to share an excerpt from Chapter 3 of Dan Brown's new book, <cite>Practical Design Discovery</cite>, available now from <a href="https://abookapart.com/products/practical-design-discovery">A Book Apart</a>.</p><p>One of the hardest design problems I ever worked on was for a company that helps IT groups manage risk. Their product focused on open-source components—inexpensive and widely supported by an enormous community, but often vulnerable to security flaws. </p>

<p>What made this design problem hard was the complexity of the product’s underlying structure, a triangle of interrelated abstract concepts. To work through the problem, we created a series of sketches that helped us understand it. </p>

<p>The outcome ended up being a relatively simple prototype, a model of the overall structure of the application. Though we were chartered to create a detailed design, our client later admitted that they knew we wouldn’t get there, but that they highly valued our efforts to solve the underlying structure. Those efforts set the direction for everything else on the product.</p>

<h2>Direction-setting assertions</h2>
<p>Much like when we frame problems, we can make assertions that set direction and describe decisions about the design. These decisions will be pretty high-level, meaning they’ll deal with a holistic view of the site or product. Decisions about details come later, though you’ll see that some assertions get pretty specific as a way of clarifying and testing the direction.</p>

<p>There are three kinds of assertions you can make about design direction:</p><ul>
<li><b>Principles</b> define what the design should or shouldn’t do. These statements are grounded in research, and may be referred to as <em>implications</em> when you can tie them to research.</li>
<li><b>Concepts</b> establish an overall approach for the product, expressed as a central theme or idea.</li>
<li><b>Models</b> describe the product in an abstract way, showing the underlying architecture, structure, flow, or approach. They offer a sense of how the product will work (without actual functionality).</li>
</ul>

<p>If you try to make tactical decisions too early, you may set a precedent without understanding how it influences what comes next—it’s difficult to trace low-level decisions back to a specific objective or problem statement. Why is the button blue? There’s no project objective in the world that can justify such a decision. </p>

<p>Instead, you’ll make a <em>few</em> low-level decisions alongside your assertions, using samples to illustrate, clarify, and demonstrate the application of the high-level decisions. For example, you might arrive at the design principle that the site’s tone should be friendly without being too casual or informal. You would demonstrate that through sample screen designs and content, showing messaging that says “Thanks!” instead of the too-formal “Thank you very much” or too-casual “You rock!”</p>

<p>Exploring the big decisions through examples might encourage you to reconsider them, or to find places in the product experience that need variation. Perhaps the color palette is insufficient for everything you need, or the authoritative voice isn’t appropriate for certain pages.</p>

<p>By venturing a solution, you’re not just asking, “Will this work?” You’re also asking, “Do I have enough knowledge to know whether this will work?” That is, steps toward solving the problem may trigger additional insights, or questions, about the problem. Great discovery entails providing just enough shape and definition so the team can get aligned behind them as direction for the product.</p>

<h2>Principles and implications</h2>
<p>Principles are rules that help designers evaluate their decisions about the design. They provide guidance in the form of absolute statements about what the design should or should not do. That said, no set of principles can be exhaustive. They read, sometimes, as commandments: rules that may be applicable to many different kinds of design decisions, and therefore open to interpretation.</p>

<p>There’s no industry standard on how to write design principles, so you won’t be violating some ordinance if you use pictograms or write a dialogue. But principles are usually just one sentence, often written in the imperative:</p><figure class="quote">
	<blockquote>
		<p>Do more with less (Microsoft Design Principles)</p><p>Design for the customer and instill confidence (Intuit)</p><p>Use data to make and improve decisions (Principles for 21st Century Government, Code for America)</p>
</blockquote>
</figure>

<p>I like these, but they don’t feel specific to the product or company. Principles are most powerful when they’re directly relevant. These use more elaborate phrases that closely relate to the product:</p><figure class="quote">
	<blockquote>
		<p>More than boxes on a screen (Google Calendar)</p><p>Transitional interfaces are easier to learn and more pleasant to use (MapBox)</p><p>Time matters, so build for people on the go (Windows User Experience Design Principles)</p>
</blockquote>
</figure>

<p>Sometimes, you’ll find principles rendered as one- or two-word noun phrases, as if to complete the expression, “The Principle of ______.”:</p><figure class="quote">
	<blockquote>
		<p>More Contrast (10 Principles of Codeacademy.com)</p><p>Consistency (First Principles of Interaction Design, Bruce Tognazzini)</p>
</blockquote>
</figure>

<p>Principles are sometimes followed by deeper descriptions and examples. My favorite variation of this comes from the <a href="http://bkaprt.com/pdd/03-01/">Windows User Experience Design Principles</a>. These principles include questions for designers to ask themselves about design decisions:</p>

<figure class="quote">
	<blockquote>
<ol style="margin-left: 1.5rem;">
<li>Personalization, not customization
<ul>
<li>Does the feature allow users to express an element of themselves?</li>
<li>Have you made the distinction between personalization and customization?</li>
<li>Does the personalization have to be a new feature, or can it make use of existing features and information (such as the user’s location, background picture, or tile)?</li>
</ul></li></ol></blockquote>
</figure>

<p>Regardless of the approach you take in framing the principles, use consistent language and structures, if only to make them easier to remember and use. If you lead with a verb, always lead with a verb. If you write a pithy phrase or a complete sentence to express the principle, always do that. If you write single-word principles, well, there’s a special place in purgatory for you.</p>

<p>In my practice, I phrase principles as direct consequences of what we learned in research. I call them <em>implications</em>, and I prefer them because they fit into the narrative: “We learned that users often lose their place in the system. The implication is that the UI should prioritize clarifying context.” </p>

<p>Implications answer the question, “So what?” You’ve generated a lot of data, and now need to explain why it all matters. I typically document this in a spreadsheet that identifies project questions, answers I&#8217;ve uncovered, and the resulting implications (<b>Fig. 1</b>).</p>

<figure>
<img src="https://alistapart.com/d/practical-design-discovery/findings-and-implications-edit.jpg" alt="Table of three columns and five rows, with a list of questions in column one, answers in column two, and implications in column 3">
<figcaption>Fig. 1: Gathering activities generate answers to questions concerning context or requirements.</figcaption>		
</figure>

<p>Ultimately, principles and implications do the same thing, so I won’t belabor the distinction between them. In both cases, they make an assertion that, yes, guides the designer, but also provides a test: designers can compare an idea to the principle and determine how closely it adheres to the guide. </p>

<p>There’s no standard for design principles, though there are lots of suggestions out there (the Resources section includes a few of the best). Here are my suggestions for crafting design principles.</p>

<h3>Be specific</h3>
<p>Principles should be as specific to the product as possible. “Easy to use” isn’t a meaningful principle, because it could apply to anything.</p>

<p>For the project with the risk-management company I described at the beginning of this chapter, we used a number of principles. In early versions of their product, users complained that it was easy to lose their place, so they couldn’t keep track of what they were working on. This led us to the principle:</p><figure class="quote">
	<blockquote>Always display the user’s context within the system, so they know where they are and what they’re working on.</blockquote>
</figure>

<p>Context became something we talked about a lot. It forced us to think carefully before moving information to a different screen, or triggering a dialog box for taking action. Because of this principle, we often asked ourselves, “Can the user tell where they are? Is loss of context here okay?”</p>

<h3>Question your choices</h3>
<p>Good principles go beyond specificity: they issue a direct challenge to designers. They force you to take a second look at your work: does the principle invalidate any of your decisions? Done right, principles should make you squirm a little.</p>

<p>In the risk-management product, the complexity of its requirements inevitably produced dense, esoteric designs. Elaborate displays attempted to capture every nuance, pack in every detail. At the same time, our client had heard their users didn’t like the dense displays. We had to walk a fine line, and so we relied on this principle:</p><figure class="quote">
	<blockquote>Show just enough information to support essential decisions—no more, no less.</blockquote>
</figure>

<p>The principle’s borderline self-contradiction provoked us to reconsider what stayed on each screen as users worked through the process. <i>Did we take out too much? Is everything on this screen absolutely necessary?</i> On one hand, we wanted users to feel confident about where they were, but on the other, we didn’t want the page overwhelmed by navigation devices irrelevant to the current task.</p>

<p>We also constantly asked ourselves, “What is ‘just enough information?’” and “What are the ‘essential decisions?’” Every iteration of the design tested the meaning of these key phrases.</p>

<h3>Inspire your team</h3>
<p>Specific and provocative principles may seem like whip-cracking: <i>Do this, and do it this way</i>. But a good principle also inspires you, pointing you to even loftier goals. It opens up possibilities by encouraging you to explore—and providing rationale for where you end up. </p>

<p>In Luke Wroblewski’s summary of a 2009 talk by Stephan Hoefnagels of Microsoft, he writes, “Goals are the mountain peaks you are trying to get to. [Design] principles are the path we use to get to the top of the mountain.”</p>

<p>One of the driving principles for my client’s product rested on the insight that the product was focused on bad news: every display was about what was going wrong in the IT department that day, how bad it was, and what wasn’t getting done. Like most interactive products, though, this one was meant to be a pleasure to use. In short, we needed to balance the gloom and doom with the satisfaction that comes from understanding the nature and extent of the bad news. We relied on this principle:</p><figure class="quote">
	<blockquote>Build confidence by clearly stating risks and making the data actionable.</blockquote>
</figure>

<p>We knew the goal was to help customers manage risk. This principle acted as the path to the top of the mountain by inspiring us to focus not just on reporting the bad news, but also on ensuring customers could do something about it.</p>

<h3>Link principles to research</h3>
<p>Principles grounded in research make for stronger statements. The death knell of any principle is arbitrariness: if a principle comes from the subjective preference of the Chief Something Officer or because it reflects the (dysfunctional) way the organization has always worked, designers will ignore it. Your principle can be otherwise perfect, but if its source is suspect, the team won’t take it seriously.</p>

<p>The team’s participation in all discovery activities is crucial here, too. Since they helped with the research, they can also help with writing the principles. By participating in crafting principles, your team will internalize them. Seeing the principles later will trigger memories of user observations, which they can integrate into their work more readily.</p>

<p>The Windows User Experience Design Principles came directly from research. In reading some of these principles, you can almost hear supporting quotes from users:</p><ul>
<li>Reduce concepts to increase confidence</li>
<li>Small things matter, good and bad</li>
<li>Be great at “look” and “do”</li>
<li>Solve distractions, not discoverability</li>
<li>UX before knobs and questions</li>
<li>Personalization, not customization</li>
<li>Value the lifecycle of the experience</li>
<li>Time matters, so build for people on the go</li>
</ul>

<p>You might argue that these lack specificity. When you take into account the scope of the project, however—an entire operating system—they’re sufficiently provocative and inspirational. “Solve distractions, not discoverability” is a bold statement, offering clear opportunities to refine the design without dictating a particular solution. It opens up conversations, and steers them, too.</p>

<h2>Concepts and big ideas</h2>
<p>One of my favorite scenes in <i>Mad Men</i>, the television show about advertising agencies in the 1960s, is the pitch to Kodak at the end of the first season. Kodak is introducing a new product, a circular tray that makes it easy to store and show photographic slides. They call it “The Wheel,” admitting, “We know wheels aren’t seen as exciting technology.”</p>

<p>Creative director Don Draper, the show’s main character, explains that this product isn’t about the technology: it’s about tapping into our memories and emotions. The agency then pulls the veil off their concept for the campaign: the carousel.</p>

<p>By establishing a central concept, a team (whether in advertising or web design) has a singular source of inspiration, a template for considering ideas. And while principles can serve as guideposts, only a concept can establish a vision. With both of them in your toolkit, your team has a potentially interesting tension to draw from.</p>

<p>Using a carousel to describe a slide projector creates a metaphor brimming with meaning and possibility. It shows two ways we can express a big idea:</p><ul>
<li><b>How the product makes you feel:</b> carousels evoke the joy of reliving happy memories.</li>
<li><b>How the product works:</b> the spinning carousel mimics storing and displaying photographic slides from a wheel.</li>
</ul>

<p>Either approach can help us express the big idea behind our digital products and websites. (Though I’ve never worked on a project that gave us a central concept as elegant as the carousel, which employs both approaches!)</p>

<h3>How the product makes you feel</h3>
<p>The purpose and function of interactive products offer ripe opportunities for metaphors, but metaphor isn’t the only way to express a central concept. For one web application project, my team expressed the essence with the phrase, “Power with flexibility.” Doesn’t quite roll off the tongue like the word carousel, but it evoked the desired feeling: that the app should make users feel like they can do anything.</p>

<p>We elaborated with descriptions of how people would experience unconstrained power with the product:</p><figure class="quote">
	<blockquote>
		<p>Provide users up-to-date status so they feel in control</p><p>Lower barriers to entry</p>
<p>Allow different styles of creating new content</p>
</blockquote>
</figure>

<p>We also described what “Power with flexibility” meant from the user’s perspective:</p><ul>
<li><b>Knowledge:</b> having the right data to shed light on immediate needs</li>
<li><b>Responsiveness:</b> being able to provide answers to stakeholders immediately</li>
<li><b>Accomplishment:</b> getting up to speed on a crucial tool right away</li>
<li><b>Control:</b> being able to fine-tune their content to suit different needs in different situations</li>
<li><b>Comfort:</b> seeing the application as an extension of one’s own thought process</li>
</ul>

<p>Since this essence was a succinct idea, a little elaboration helped it to resonate with both the client and the project team.</p>

<h3>How the product works</h3>
<p>Complex interactive products benefit from a central idea that describes how they work. This usually means employing a big idea to convey the underlying structure. </p>

<p><em>Shopping cart</em>, for example, is a popular metaphor used on ecommerce sites. You could use it even if you weren’t working on an ecommerce site. The idea of “adding stuff to the cart” is a familiar metaphor that conveys a site’s underlying structure. We even relied on this metaphor on our career-guidance site: students would “add careers to their cart” after taking an assessment.</p>

<p>There are a few other tried-and-true frameworks for describing the structure of a website. For web applications, there are two common ones beyond the shopping cart:</p><ul>
<li><b>Hub-and-spoke:</b> This is perhaps the most common pattern for structuring a website or digital product. The hub-and-spoke metaphor implies that the web application has a central screen, from which users may trigger all other functions.</li>
<li><b>List-detail:</b> Another typical approach consists of a list of items from which users can select for more detail—like your email inbox.</li>
</ul>

<p>Do you have to use one of these structures? Of course not. But if your site lends itself to one of these approaches, you have your <em>big idea</em> that the rest of the functionality revolves around. (That wasn&#8217;t a carousel reference. I promise.)</p>

<p>For sites that focus on delivering content (rather than transactional functionality), the tried-and-true frameworks deal more with how the content is organized:</p><ul>
<li><b>Topics:</b> what the content is about, or the subject matter</li>
<li><b>Actions:</b> what tasks the content supports (like researching products versus troubleshooting products)</li>
</ul>

<p>These aren’t the only structures for categorizing content, but they are my go-to starting points.</p>

<p>None of these is a fully fledged design in and of itself. They are well-understood frameworks that serve as the backbone to a much larger design. They are big ideas that describe how the product works.</p>

<p>You don’t have to rely on an abstraction or metaphor (like the carousel) to convey the big idea, but instead draw from the emerging library of understood frameworks. That they are becoming part of web design lingo is a testament to their power and flexibility.</p>

<h2>There&#8217;s more where that came from!</h2>
<p>Check out the rest of <cite>Practical Design Discovery</cite> at <a href="https://abookapart.com/products/practical-design-discovery">A Book Apart</a>.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/dNW13PRBM4Y" height="1" width="1" alt=""/>