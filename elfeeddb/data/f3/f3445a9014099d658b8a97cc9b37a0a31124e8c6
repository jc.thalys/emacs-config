<ul class="org-ul">
<li>Emacs Lisp:
<ul class="org-ul">
<li><a href="https://github.com/caisah/emacs.dz">emacs.dz &#8211; &#8220;A list of nice emacs config files&#8221;</a> (<a href="https://www.reddit.com/r/emacs/comments/68mcpe/emacsdz_a_list_of_nice_emacs_config_files/">Reddit</a>)</li>
<li><a href="http://mbork.pl/2017-05-01_show-some-last-messages">show-some-last-messages</a></li>
<li><a href="https://github.com/vermiculus/apiwrap.el">API-Wrap.el: macros for building Elisp clients for REST APIs</a> (<a href="https://www.reddit.com/r/emacs/comments/69q0b6/apiwrapel_macros_for_building_elisp_clients_for/">Reddit</a>)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/69oqzw/how_do_i_make_a_keymap_with_higher_precedence/">How do I make a keymap with higher precedence than minor mode maps?</a> transient maps</li>
<li><a href="https://www.reddit.com/r/emacs/comments/69c3bg/function_to_get_next_keypress/">Function to get next keypress</a></li>
</ul>
</li>
<li>Emacs development:
<ul class="org-ul">
<li><a href="https://lists.gnu.org/archive/html/emacs-devel/2017-04/msg00798.html">RMS supports Language Server Protocol integration into Emacs core</a> (<a href="https://www.reddit.com/r/emacs/comments/696pv1/rms_supports_language_server_protocol_integration/">Reddit</a>)</li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=c311b8b15e91dd07e2d23d8d21ebb53d0b5f2204">write-region character counts now off by default</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=bc4d6185528b7e0933cd0486879ea3c2fbc7cf5a">GCC warnings now include checking list object types</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=88f96e69cfcd265f2ef0db3e134ac9e29e64ec3e">New function seq-set-equal-p</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=f0708fc5e424a2b5b814c59be0ec7234a739a500">CSS colours now fontified</a></li>
<li><a href="http://lists.gnu.org/archive/html/emacs-devel/2017-05/msg00222.html">Ongoing work on address-sanitizer-enabled emacs</a></li>
<li><a href="http://lists.gnu.org/archive/html/emacs-devel/2017-05/msg00084.html">Pausing work on overlays as an AA-tree</a></li>
</ul>
</li>
<li>Appearance:
<ul class="org-ul">
<li><a href="http://vfoley.xyz/syntax-highlighting/">Less intrusive color theme</a> (<a href="https://www.reddit.com/r/emacs/comments/69qz5u/less_intrusive_color_theme/">Reddit</a>)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/6905nl/pdf_tools_dark_theme/">Pdf tools dark theme</a></li>
</ul>
</li>
<li>Navigation:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/69qe9l/findfileinproject_v530/">find-file-in-project v5.3.0</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/68z254/switchwindow_v150_is_out/">switch-window v1.5.0 is out.</a></li>
<li><a href="https://fuco1.github.io/2017-05-06-Enhanced-beginning--and-end-of-buffer-in-special-mode-buffers-(dired-etc.).html">Enhanced beginning- and end-of-buffer in special mode buffers (dired etc.)</a> (hat-tip to <a href="http://irreal.org/blog/?p=6194">Irreal</a> for the link)</li>
</ul>
</li>
<li>Org Mode:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/6995gh/exporting_orgagenda_to_mobile/">Exporting org-agenda to mobile</a></li>
<li><a href="https://emacs.stackexchange.com/questions/32617/how-to-jump-directly-to-an-org-headline">how to jump directly to an org-headline?</a> (<a href="https://www.reddit.com/r/emacs/comments/69mc6l/how_to_jump_directly_to_an_orgheadline/">Reddit</a>)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/69jkqe/orgmode_notifications/">Org-mode notifications?</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/69cazb/auto_loading_a_custom_el_script_when_opening_one/">auto loading a custom el script when opening one specific org file</a></li>
</ul>
</li>
<li>Coding:
<ul class="org-ul">
<li><a href="https://gitlab.com/emacs-stuff/git-commit-insert-issue/tree/master">Get and reference an issue in a commit message: now Bitbucket support (also Gitlab and Github)</a> (<a href="https://www.reddit.com/r/emacs/comments/696aed/get_and_reference_an_issue_in_a_commit_message/">Reddit</a>)</li>
<li><a href="https://www.youtube.com/watch?v=O6g5C4jUCUc">Emacs and Clojure, a Lispy Love Affair (41:16)</a> (<a href="https://www.reddit.com/r/emacs/comments/68zxbo/emacs_and_clojure_a_lispy_love_affair_video/">Reddit</a>)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/68zacv/using_tidemode_to_typecheck_javascript/">Using tide-mode to type-check JavaScript</a></li>
<li><a href="https://www.youtube.com/watch?v=aYA4AAjLfT0">Getting Started with CIDER</a> (56:02, from 2016)</li>
<li><a href="https://github.com/alex-hhh/emacs-sql-indent">Smart indentation for SQL files</a> (<a href="https://www.reddit.com/r/emacs/comments/68xul4/smart_indentation_for_sql_files/">Reddit</a>)</li>
<li><a href="https://www.youtube.com/watch?v=PRlFJSf3xqU">Lightning talk: Giraffe, Visualizing Data With Emacs and ClojureScript &#8211; Jack Rusher</a> (6:27)</li>
</ul>
</li>
<li>Other:
<ul class="org-ul">
<li><a href="http://irreal.org/blog/?p=6192">Email With Notmuch and Astroid</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/68xtx1/reliable_way_of_setting_up_gpg_password_input_in/">Reliable way of setting up gpg (password input) in emacs?</a></li>
<li><a href="https://writequit.org/denver-emacs/">Denver Emacs Group</a> (<a href="https://www.reddit.com/r/emacs/comments/68mi97/denver_emacs_group/">Reddit</a>)</li>
<li><a href="https://github.com/josteink/emacs-oob-reboot">Help us improve the default Emacs out-of-the-box experience</a> (<a href="https://www.reddit.com/r/emacs/comments/69dvkm/help_us_improve_the_default_emacs_outofthebox/">Reddit</a>)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/69ebki/hows_climacs_going/">How&#8217;s Climacs going?</a></li>
</ul>
</li>
<li>New packages:
<ul class="org-ul">
<li><a href="http://melpa.org/#/dakrone-light-theme" target="_blank" rel="noopener noreferrer">dakrone-light-theme</a>: dakrone&#8217;s custom light theme</li>
<li><a href="http://melpa.org/#/evil-goggles" target="_blank" rel="noopener noreferrer">evil-goggles</a>: Add a visual hint to evil operations</li>
<li><a href="http://melpa.org/#/helm-xref" target="_blank" rel="noopener noreferrer">helm-xref</a>: Helm interface for xref results</li>
<li><a href="http://melpa.org/#/ini-mode" target="_blank" rel="noopener noreferrer">ini-mode</a>: Major mode for Windows-style ini files.</li>
<li><a href="http://melpa.org/#/mastodon" target="_blank" rel="noopener noreferrer">mastodon</a>: Client for Mastodon</li>
<li><a href="http://melpa.org/#/point-pos" target="_blank" rel="noopener noreferrer">point-pos</a>: Save and restore point positions</li>
<li><a href="http://melpa.org/#/turing-machine" target="_blank" rel="noopener noreferrer">turing-machine</a>: Single-tape Turing machine simulator</li>
<li><a href="https://elpa.gnu.org/packages/hook-helpers.html" target="_blank" rel="noopener noreferrer">hook-helpers</a>: Anonymous, modifiable hook functions</li>
</ul>
</li>
</ul>
<p>Links from <a href="http://reddit.com/r/emacs/new">reddit.com/r/emacs</a>, <a href="http://reddit.com/r/orgmode">/r/orgmode</a>, <a href="http://reddit.com/r/spacemacs">/r/spacemacs</a>, <a href="https://hn.algolia.com/?query=emacs&amp;sort=byDate&amp;prefix&amp;page=0&amp;dateRange=all&amp;type=story">Hacker News</a>, <a href="http://planet.emacsen.org">planet.emacsen.org</a>, <a href="https://www.youtube.com/results?search_query=emacs&amp;search_sort=video_date_uploaded">Youtube</a>, the changes to the <a href="http://git.savannah.gnu.org/cgit/emacs.git/log/etc/NEWS">Emacs NEWS file</a>, and <a href="http://lists.gnu.org/archive/html/emacs-devel/2017-05">emacs-devel</a>.</p>
<p><a href="http://sachachua.com/blog/category/emacs-news">Past Emacs News round-ups</a></p>
<div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/sachac?a=Y_vMfar1WQE:wJXlOoEdKmM:a8iZE8QBh80"><img src="http://feeds.feedburner.com/~ff/sachac?i=Y_vMfar1WQE:wJXlOoEdKmM:a8iZE8QBh80" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/sachac?a=Y_vMfar1WQE:wJXlOoEdKmM:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/sachac?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/sachac/~4/Y_vMfar1WQE" height="1" width="1" alt=""/>