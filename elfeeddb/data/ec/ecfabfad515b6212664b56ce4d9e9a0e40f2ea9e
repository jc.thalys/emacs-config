
<p>While browsing Reddit the other day, I stumbled upon <a href='https://www.reddit.com/r/youtubehaiku'>/r/youtubehaiku</a>. As with any seemingly popular subreddit that I discover, I decided to check out the top posts. The next day I showed the subreddit to my coworker and jokingly suggested that he write a script to scrape the links from Reddit and create a YouTube playlist from them. We spent the next couple of hours doing just that. Here's how I wrote mine.</p><h3><a name="the&#95;setup"></a>The Setup</h3><p>I used the <code>app</code> template as the base of the project by running the following command in the terminal:</p><pre><code>lein new app youtubehaikus
</code></pre><p>I knew I'd need dependencies for making http requests, encoding/decoding JSON, and URL parsing so I added the relevant libraries to my <code>project.clj</code></p><pre><code class="clojure">:dependencies &#91;&#91;org.clojure/clojure &quot;1.8.0&quot;&#93;
               &#91;clj-http &quot;2.2.0&quot;&#93;
               &#91;cheshire &quot;5.6.1&quot;&#93;
               &#91;com.cemerick/url &quot;0.1.1&quot;&#93;&#93;
</code></pre><p>Next, I created a new playlist on YouTube and <a href='https://developers.google.com/youtube/v3/guides/auth/server-side-web-apps'>created an OAuth 2.0 access token</a>. These tokens expire after 60 minutes.</p><h3><a name="getting&#95;the&#95;reddit&#95;post&#95;data"></a>Getting the Reddit Post Data</h3><p><a href='https://www.reddit.com/dev/api#GET_top'>This</a> is the Reddit API endpoint that we want to use to get the post data. To get the first 100 top posts of all time, we need to specify the query params <code>t=all</code> and <code>limit=100</code> in our request. If you specify the <code>after=FULLNAME</code> param then you can retrieve data about posts that come after a certain post. I be using this optional parameter in order to scrape more than just the top 100 posts of all time. </p><p>We'll start by defining our endpoint as follows:</p><pre><code class="clojure">&#40;def reddit-url &quot;https://www.reddit.com/r/youtubehaiku/top.json?t=all&amp;limit=100&quot;&#41;
</code></pre><p>The body of the response looks something like this:</p><pre><code class="clojure">{:kind &quot;Listing&quot;,
 :data {:modhash &quot;&quot;,
        :children &#91;...&#93;}}
</code></pre><p>The post data is stored under the <code>:children</code> key so we'll want to retrieve that:</p><pre><code class="clojure">&#40;-&gt; reddit-url
    &#40;http/get {:headers {&quot;User-Agent&quot; &quot;thing by /u/me&quot;
                         &quot;Accept&quot;     &quot;application/json&quot;}}&#41;
    :body
    &#40;json/parse-string true&#41;
    &#40;get-in &#91;:data :children&#93;&#41;&#41;
</code></pre><p>Next, we want to extract the YouTube video ids from the links in the posts. Each object in the <code>:children</code> vector is structured like so:</p><pre><code class="clojure">{:kind &quot;t3&quot;,
  :data { ...
         :url &quot;...&quot;
         :name &quot;t3&#95;...&quot;}}
</code></pre><p>and we'll obtain the video ids as follows:</p><pre><code class="clojure">&#40;defn get-path &#91;{:keys &#91;host path query&#93;}&#93;
  &#40;if &#40;= &quot;youtu.be&quot; host&#41;
    &#40;subs path 1&#41;
    &#40;or &#40;get query &quot;v&quot;&#41;
        &#40;get query &quot;amp;v&quot;&#41;&#41;&#41;&#41;

&#40;map #&#40;-&gt; % &#40;get-in &#91;:data :url&#93;&#41; url get-path&#41; post-data&#41;
</code></pre><h3><a name="creating&#95;the&#95;playlist"></a>Creating the Playlist</h3><p>The last step is to iterate through the video ids and add them to our playlist using the <a href='https://developers.google.com/youtube/v3/docs/playlistItems/insert'>YouTube API</a>. </p><p>However, I wanted to scrape the top 500 videos so I did that by using <code>loop/recur</code> and the <code>after</code> query param in the Reddit endpoint. The <code>FULLNAME</code> that you want to use as the <code>after</code> param is the value associated with the <code>:name</code> key in the last entry of the post data. </p><p>Putting it all together, this was my entire namespace:</p><pre><code class="clojure">&#40;ns youtubehaikus.core
  &#40;:gen-class&#41;
  &#40;:require &#91;clj-http.client :as http&#93;
            &#91;cheshire.core :as json&#93;
            &#91;cemerick.url :refer &#91;url&#93;&#93;&#41;&#41;

&#40;def reddit-url &quot;https://www.reddit.com/r/youtubehaiku/top.json?t=all&amp;limit=100&quot;&#41;

&#40;defn get-path &#91;{:keys &#91;host path query&#93;}&#93;
  &#40;if &#40;= &quot;youtu.be&quot; host&#41;
    &#40;subs path 1&#41;
    &#40;or &#40;get query &quot;v&quot;&#41;
        &#40;get query &quot;amp;v&quot;&#41;&#41;&#41;&#41;

&#40;defn -main &#91;&amp; args&#93;
  &#40;loop &#91;reddit-url reddit-url
         pages      5&#93;
    &#40;let &#91;post-data    &#40;-&gt; reddit-url
                           &#40;http/get {:headers {&quot;User-Agent&quot; &quot;thing by /u/me&quot;
                                                &quot;Accept&quot;     &quot;application/json&quot;}}&#41;
                           :body
                           &#40;json/parse-string true&#41;
                           &#40;get-in &#91;:data :children&#93;&#41;&#41;
          last-post-id &#40;get-in &#40;last post-data&#41; &#91;:data :name&#93;&#41;
          haiku-ids    &#40;map #&#40;-&gt; % &#40;get-in &#91;:data :url&#93;&#41; url get-path&#41; post-data&#41;&#93;
      &#40;doseq &#91;id haiku-ids&#93;
        &#40;try &#40;http/post &quot;https://www.googleapis.com/youtube/v3/playlistItems&quot;
                        {:query-params {:access&#95;token &quot;ACCESS&#95;TOKEN&quot;
                                        :part         &quot;snippet&quot;}
                         :content-type :json
                         :body         &#40;-&gt; {:snippet {:playlistId &quot;PLAYLIST&#95;ID&quot;
                                                      :resourceId {:kind    &quot;youtube#video&quot;
                                                                   :videoId id}}}
                                           json/generate-string&#41;}&#41;
             &#40;catch Exception &#95;&#41;&#41;&#41;
      &#40;when-not &#40;= 1 pages&#41;
        &#40;recur &#40;str reddit-url &quot;&amp;after=&quot; last-post-id&#41;
               &#40;dec pages&#41;&#41;&#41;&#41;&#41;&#41;
</code></pre><p>You can run this from the root directory of the project on the command line by doing <code>lein run</code> or from your repl.</p><p>I wrapped the <code>post</code> in a <code>try/catch</code> block that eats the exception because I was getting some bad responses when attempting to add some of the videos to my playlist - I think it was because some of the videos no longer exist. The link to my playlist is <a href='https://www.youtube.com/watch?v=7DRL6ocEK-M&list=PLJLxMUV2EaMRtYRm5xMsnxquhFzQguB3T'>here</a>.</p>
