<p>
One of the things I like most in CIDER is how evaluation results are displayed
by inline overlays. And yet, for some reason, it’s taken me almost a year to
transfer that to Elisp.
</p>


<div class="figure">
<p><a href="/images/elisp-inline-eval-result.png"><img src="/images/elisp-inline-eval-result.png" alt="elisp-inline-eval-result.png" /></a>
</p>
</div>

<p>
It’s the tiniest of changes &#x2014; you’re just taking something that would be
displayed in the minibuffer and moving it up a dozen or so lines &#x2014; but the
difference is palpable. By displaying the result inline, you display it where
the user is looking, instead of forcing them to shift focus. The quick-feedback
loop we all love in our lisps becomes even faster (something I would’ve thought
impossible one year ago).
</p>

<p>
Assuming you already have <a href="https://github.com/clojure-emacs/cider#installation">CIDER</a> installed, porting this feature to Elisp is
almost trivial. We just define a small wrapper around the function that creates
the overlay, and then advise the relevant elisp commands to call it.
</p>
<figure class="highlight"><pre><code class="language-cl" data-lang="cl"><span class="p">(</span><span class="nv">autoload</span> <span class="ss">'cider--make-result-overlay</span> <span class="s">"cider-overlays"</span><span class="p">)</span>

<span class="p">(</span><span class="nb">defun</span> <span class="nv">endless/eval-overlay</span> <span class="p">(</span><span class="nv">value</span> <span class="nv">point</span><span class="p">)</span>
  <span class="p">(</span><span class="nv">cider--make-result-overlay</span> <span class="p">(</span><span class="nb">format</span> <span class="s">"%S"</span> <span class="nv">value</span><span class="p">)</span>
    <span class="ss">:where</span> <span class="nv">point</span>
    <span class="ss">:duration</span> <span class="ss">'command</span><span class="p">)</span>
  <span class="c1">;; Preserve the return value.</span>
  <span class="nv">value</span><span class="p">)</span>

<span class="p">(</span><span class="nv">advice-add</span> <span class="ss">'eval-region</span> <span class="ss">:around</span>
            <span class="p">(</span><span class="k">lambda</span> <span class="p">(</span><span class="nv">f</span> <span class="nv">beg</span> <span class="nv">end</span> <span class="k">&amp;rest</span> <span class="nv">r</span><span class="p">)</span>
              <span class="p">(</span><span class="nv">endless/eval-overlay</span>
               <span class="p">(</span><span class="nb">apply</span> <span class="nv">f</span> <span class="nv">beg</span> <span class="nv">end</span> <span class="nv">r</span><span class="p">)</span>
               <span class="nv">end</span><span class="p">)))</span>

<span class="p">(</span><span class="nv">advice-add</span> <span class="ss">'eval-last-sexp</span> <span class="ss">:filter-return</span>
            <span class="p">(</span><span class="k">lambda</span> <span class="p">(</span><span class="nv">r</span><span class="p">)</span>
              <span class="p">(</span><span class="nv">endless/eval-overlay</span> <span class="nv">r</span> <span class="p">(</span><span class="nv">point</span><span class="p">))))</span>

<span class="p">(</span><span class="nv">advice-add</span> <span class="ss">'eval-defun</span> <span class="ss">:filter-return</span>
            <span class="p">(</span><span class="k">lambda</span> <span class="p">(</span><span class="nv">r</span><span class="p">)</span>
              <span class="p">(</span><span class="nv">endless/eval-overlay</span>
               <span class="nv">r</span>
               <span class="p">(</span><span class="nv">save-excursion</span>
                 <span class="p">(</span><span class="nv">end-of-defun</span><span class="p">)</span>
                 <span class="p">(</span><span class="nv">point</span><span class="p">)))))</span></code></pre></figure>
<p>
If I like this enough, I might implement it more properly and propose its
addition to Emacs core. For now the advices are more than enough.
</p>

<p>
If you don’t want to install CIDER, you can just <a href="https://github.com/clojure-emacs/cider/blob/master/cider-overlays.el#L121">copy that function</a> to your
configs (you’ll also have to copy the functions above it in the same file, and
the <code>when-let</code> definition from <code>cider-compat.el</code>).
</p>

   <p><a href="http://endlessparentheses.com/eval-result-overlays-in-emacs-lisp.html?source=rss#disqus_thread">Comment on this.</a></p>