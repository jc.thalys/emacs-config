<p>
    It's been several years since my <a href="http://raibledesigns.com/rd/entry/peter_estin_hut_trip_in">last hut
    trip</a>. When my friend <a href="https://twitter.com/bradswanson">Brad Swanson</a> invited me this year, I jumped
    at the opportunity. Trish skipped this trip and my good friend Ryan joined in her place. It was Ryan's first hut
    trip. As a snowboarder, he opted to snowshoe with his snowboard on his back.</p>
<p>Our journey to <a href="http://www.huts.org/The_Huts/gates.php">Harry Gates Hut</a> began early last Friday morning.
    Our Syncro had just returned from the body shop the night before and was ready to head for the hills. We arrived in
    Basalt, Colorado (in the <a href="http://en.wikipedia.org/wiki/Roaring_Fork_Valley">Roaring Fork Valley</a>) around
    11am and were on the trail at 12:30pm.</p>

<p style="text-align: center">
    <a href="https://farm8.staticflickr.com/7417/15859692313_fc80f62317_c.jpg" title="The Syncro is back! by Matt Raible, on Flickr" rel="lightbox[harrygateshuttrip]" data-href="https://www.flickr.com/photos/mraible/15859692313"><img src="https://farm8.staticflickr.com/7417/15859692313_fc80f62317_n.jpg" width="320" alt="The Syncro is back!" style="border: 1px solid black;"></a>
</p>

<p>
    From experience, I knew it was going to be a long slog uphill. I rented telemark skis, with NTN boots/bindings, from
    <a href="http://confluencekayaks.com/">Confluence Kayaks</a>. We both quickly realized we'd packed too many
    supplies, as our packs were quite heavy. Nevertheless, we trudged on, one foot in front of the other.
</p>
<p>
    <a href="https://farm8.staticflickr.com/7372/16292192770_136023b438_c.jpg" title="Sunset on the hike in by Matt Raible, on Flickr" rel="lightbox[harrygateshuttrip]" data-href="https://www.flickr.com/photos/mraible/16292192770"><img src="https://farm8.staticflickr.com/7372/16292192770_136023b438_t.jpg" width="100" alt="Sunset on the hike in" class="picture" style="border: 1px solid black;"></a>
    At 6.6 miles and 1900' elevation gain, Brad estimated it'd take us 4-6 hours for the hike in. We thought we were
    close to finishing the uphill when the sun set around 6pm.
    Because it was dark, and we only had one headlamp, we were unable to ski the last mile of downhill near the end.
    We arrived at the hut at 8:00pm, after 7.5 hours of hiking and sweating profusely. We were extremely happy to be
    finished.
</p>
<p>The next day, we woke up, had a delicious pancake breakfast, then hiked to the top of Burnt Mountain. It was a two
    mile jaunt, straight uphill.
</p>
<p style="text-align: center">
    <a href="https://farm8.staticflickr.com/7395/15859518043_93c74fa324_c.jpg" title="Heading up Burnt Mountain by Matt Raible, on Flickr" rel="lightbox[harrygateshuttrip]" data-href="https://www.flickr.com/photos/mraible/15859518043"><img src="https://farm8.staticflickr.com/7395/15859518043_93c74fa324_m.jpg" width="240" alt="Heading up Burnt Mountain" style="border: 1px solid black;"></a>
    <a href="https://farm8.staticflickr.com/7393/16293730027_4dd6840b44_c.jpg" title="Ryan was a trooper on his snowboard by Matt Raible, on Flickr" rel="lightbox[harrygateshuttrip]" data-href="https://www.flickr.com/photos/mraible/16293730027"><img src="https://farm8.staticflickr.com/7393/16293730027_4dd6840b44_m.jpg" width="240" alt="Ryan was a trooper on his snowboard" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p>
    About halfway up, I noticed the other guy's heals looked funny. They had some sort of post
    that
    prevented their heal from coming all the way down, making it a lot easier for them to climb. I thought "WTF?", then
    looked at my own bindings and realized I had the same contraption. I shouted "Hey guys, this is a helluva lot easier
    - why didn't you tell me about the heal posts?!" They laughed and marveled that this was my third hut trip and no
    one had ever mentioned climbing posts. I guess that's what happens when you're always at the back of the pack. <img src="http://raibledesigns.com/images/smileys/wink.gif" class="smiley" alt=";)" title=";)" />
</p>
<p>The views from the top of Burnt Mountain were spectacular.</p>
<p style="text-align: center">
    <a href="https://farm8.staticflickr.com/7343/16479631305_a2239b32de_c.jpg" title="Mark at the top of Burnt Mountain by Matt Raible, on Flickr" rel="lightbox[harrygateshuttrip]" data-href="https://www.flickr.com/photos/mraible/16479631305"><img src="https://farm8.staticflickr.com/7343/16479631305_a2239b32de_m.jpg" width="240" alt="Mark at the top of Burnt Mountain" style="border: 1px solid black;"></a>
    <a href="https://farm8.staticflickr.com/7457/16453627646_25d63e5910_c.jpg" title="Joe and Brad on Burnt Mountain by Matt Raible, on Flickr" rel="lightbox[harrygateshuttrip]" data-href="https://www.flickr.com/photos/mraible/16453627646"><img src="https://farm8.staticflickr.com/7457/16453627646_25d63e5910_m.jpg" width="240" alt="Joe and Brad on Burnt Mountain" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p>The run down wasn't great - we got about six turns in before we ran into flat trees. From there,
    it was lots of traversing and navigating between trees back to the hut. Our Saturday ski excursion took around 4.5
    hours. We hung out at the hut, played cribbage, enjoyed the scenery and went to bed early that night.</p>
<p style="text-align: center">
    <a href="https://farm8.staticflickr.com/7424/16479638175_4937c594f9_c.jpg" title="10th Mountain Division by Matt Raible, on Flickr" rel="lightbox[harrygateshuttrip]" data-href="https://www.flickr.com/photos/mraible/16479638175"><img src="https://farm8.staticflickr.com/7424/16479638175_4937c594f9_m.jpg" width="240" alt="10th Mountain Division" style="border: 1px solid black;"></a>
    <a href="https://farm8.staticflickr.com/7364/16293739607_ae4bc2fe9c_c.jpg" title="The view from our porch by Matt Raible, on Flickr" rel="lightbox[harrygateshuttrip]" data-href="https://www.flickr.com/photos/mraible/16293739607"><img src="https://farm8.staticflickr.com/7364/16293739607_ae4bc2fe9c_m.jpg" width="240" alt="The view from our porch" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p style="text-align: center">
    <a href="https://farm9.staticflickr.com/8620/16292206910_cd7331541a_c.jpg" title="Heat stove and water supply by Matt Raible, on Flickr" rel="lightbox[harrygateshuttrip]" data-href="https://www.flickr.com/photos/mraible/16292206910"><img src="https://farm9.staticflickr.com/8620/16292206910_cd7331541a_m.jpg" width="240" alt="Heat stove and water supply" style="border: 1px solid black;"></a>
    <a href="https://farm8.staticflickr.com/7359/16453636836_2dbecfb3a5_c.jpg" title="Just like Mom's! by Matt Raible, on Flickr" rel="lightbox[harrygateshuttrip]" data-href="https://www.flickr.com/photos/mraible/16453636836"><img src="https://farm8.staticflickr.com/7359/16453636836_2dbecfb3a5_m.jpg" width="240" alt="Just like Mom's!" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p style="text-align: center">
    <a href="https://farm9.staticflickr.com/8683/16478677212_3dc2b0806d_c.jpg" title="Group Photo! by Matt Raible, on Flickr" rel="lightbox[harrygateshuttrip]" data-href="https://www.flickr.com/photos/mraible/16478677212"><img src="https://farm9.staticflickr.com/8683/16478677212_3dc2b0806d.jpg" width="500" alt="Group Photo!" style="border: 1px solid black;"></a>
</p>
<p>Sunday, we hiked out. It took us around two hours to reach the high point in the trail, then 30 minutes to complete
    the 5-mile downhill stretch.</p>
<p>Looking back, Ryan and I estimated we did about 14 hours of hiking at 10,000 feet over the weekend. While my pack was
    heavy, it was much easier for me to skin up the mountain than it was for Ryan on snowshoes. Especially when he kept
    post-holing on the hike up Burnt Mountain. Nevertheless, we survived and created some great memories from the
    experience.
</p>
<p>
    Thanks to Brad and everyone else for showing us that packing light can make a real difference.
    To lighten the load on my next hut trip, I plan on bringing nothing but a sleeping bag, some almonds and a couple bananas.
</p>

<p>For more pictures from this adventure, see <a href="https://www.flickr.com/photos/mraible/sets/72157650307961778/">my Harry Gates Hut Trip photos on Flickr</a>.

</p>