<p>Over the past few years, I&#8217;ve built many versions of the Employee Directory application as a way to explore new languages and frameworks. In this post I’ll share a version of the Employee Directory sample app built with <a href="https://facebook.github.io/react-native/">React Native</a>. </p>
<p>React Native is a framework for building native mobile applications using JavaScript. It uses the same programming model as <a href="https://facebook.github.io/react/">React</a>, with one key difference: instead of rendering DOM elements in a browser window, a React Native application renders native components in your mobile platform UI system (i.e. UIKit on iOS). In other words:</p>
<ul>
<li>In <strong>React</strong>, you render your components using React components like <strong>&lt;div></strong> and <strong>&lt;ul></strong> which are wrappers around DOM elements.</li>
<li>In <strong>React Native</strong>, you render your components using React Native components like <strong>&lt;View></strong> and <strong>&lt;ListView></strong> which are wrappers around native components.</li>
</ul>
<h2>The Sample Application</h2>
<p>The Employee Directory application allows you to search for employees by name, view the details of an employee, call, text, or email an employee, and navigate up and down the org chart. </p>
<p>Watch the Video:</p>
<p><iframe width="640" height="360" src="https://www.youtube.com/embed/2Pbgi2d6hEw" frameborder="0" allowfullscreen></iframe></p>
<h2>Highlights:</h2>
<p>Employee Directory uses the following React Native features:</p>
<ul>
<li>Page navigation using <a href="https://facebook.github.io/react-native/docs/navigator.html">Navigator</a> and <a href="https://facebook.github.io/react-native/docs/navigator.html#navigation-bar">NavigationBar</a></li>
<li>List management using <a href="https://facebook.github.io/react-native/docs/listview.html">ListView</a></li>
<li>Styles using <a href="https://facebook.github.io/react-native/docs/stylesheet.html">StyleSheet</a></li>
<li>Other components such as <a href="https://facebook.github.io/react-native/docs/view.html">&lt;View></a>, <a href="https://facebook.github.io/react-native/docs/image.html">&lt;Image></a>, <a href="https://facebook.github.io/react-native/docs/textinput.html">&lt;TextInput></a>, <a href="https://facebook.github.io/react-native/docs/touchableopacity.html">&lt;TouchableOpacity></a>, <a href="https://facebook.github.io/react-native/docs/touchablehighlight.html">&lt;TouchableHighlight></a>, etc.</li>
<li>The <a href="https://facebook.github.io/react-native/docs/network.html">Fetch API</a> to load resources from a remote URL</li>
</ul>
<h2>Installing the Application</h2>
<ol>
<li>Follow the React Native <a href="https://facebook.github.io/react-native/docs/getting-started.html">getting started instructions</a> to install React Native. Make sure you select your Mobile OS and Development OS, for the right instructions. The instructions below assume ios.</li>
<li>Create a new React Native app named <strong>EmployeeDirectory</strong>:
<pre class="brush: bash; title: ; notranslate">
react-native init EmployeeDirectory
cd EmployeeDirectory
react-native run-ios
</pre>
</li>
<li>Clone the <a href="https://github.com/ccoenraets/employee-directory-react-native">sample app repository</a>:
<pre class="brush: bash; title: ; notranslate">
git clone https://github.com/ccoenraets/employee-directory-react-native
</pre>
</li>
<li>Copy the <strong>app</strong> folder from <strong>employee-directory-react-native</strong> into your <strong>EmployeeDirectory</strong> project folder</li>
<li>Open <strong>index.ios.js</strong>. Delete the existing content and replace it with:
<pre class="brush: jscript; title: ; notranslate">
import {AppRegistry} from 'react-native';

import EmployeeDirectoryApp from './app/EmployeeDirectoryApp';

AppRegistry.registerComponent('EmployeeDirectory', () =&gt; EmployeeDirectoryApp);
</pre>
</li>
<li>Hit Cmd + R to refresh the app in the emulator</li>
</ol>
<h2>Mock and REST Services</h2>
<p>The app comes with two interchangeable implementations of the data service:</p>
<ul>
<li>The Mock service implementation (employee-service-mock.js) uses in-memory data and is used for prototyping or testing purpose.</li>
<li>The REST service implementation (employee-service-rest.js) uses fetch to access Node.js-based REST services <a href="http://employee-directory-services.herokuapp.com/employees">hosted on Heroku</a>. You can find the source code for the Nodes.js services in <a href="https://github.com/ccoenraets/employee-directory-services">this</a> GitHub repository.</li>
</ul>
<p>The application uses the mock service by default. To use the REST service instead, open <strong>EmployeeList.js</strong> and <strong>EmployeeDetails.js</strong>, and replace:</p>
<pre class="brush: jscript; title: ; notranslate">
import * as employeeService from './services/employee-service-mock';
</pre>
<p>with:</p>
<pre class="brush: jscript; title: ; notranslate">
import * as employeeService from './services/employee-service-rest';
</pre>
<h2>Source Code</h2>
<p>The source code for the React Native application is available in <a href="https://github.com/ccoenraets/employee-directory-react-native">this</a> GitHub repository.<br />
The source code for the REST services is available in <a href="https://github.com/ccoenraets/employee-directory-services">this</a> GitHub repository.</p>
<h2>Additional Resources</h2>
<p>If you are new to React or ECMAScript 6, check out my React and ECMAScript 6 tutorials:</p>
<ul>
<li><a href="http://ccoenraets.github.io/es6-tutorial/">ECMAScript 6 tutorial</a></li>
<li><a href="http://ccoenraets.github.io/es6-tutorial-data/">ECMAScript 6 Data Access tutorial</a></li>
<li><a href="http://ccoenraets.github.io/es6-tutorial-react/">ECMAScript 6 React tutorial</a></li>
</ul>
<p>And/or check out my React sample apps:</p>
<ul>
<li><a href="https://github.com/ccoenraets/lightning-react-app">Real Estate Application built with React</a></li>
<li><a href="https://github.com/ccoenraets/belgian-beer-explorer">Belgian Beer Explorer built with React</a>  (hosted live <a href="http://www.belgianbeerexplorer.com/">here</a>)</li>
<li><a href="http://coenraets.org/blog/2016/08/building-a-trivia-app-with-react/">Trivia application built with React</a> (hosted live <a href="https://ccoenraets.github.io/react-trivia/">here</a>)</li>
</ul>
