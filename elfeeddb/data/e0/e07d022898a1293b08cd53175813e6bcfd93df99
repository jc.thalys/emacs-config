<p>This article is the second in a series about learning <a href="https://angularjs.org/">AngularJS</a>. It describes
    how to test a simple AngularJS application. In a previous article, <a
            href="//raibledesigns.com/rd/entry/getting_started_with_angularjs">Getting Started with AngularJS</a>, I
    showed how to develop a simple search and edit feature.</p>
<h3>What you'll learn</h3>
<p>You'll learn to use <a href="http://jasmine.github.io/">Jasmine</a> for unit testing controllers and <a
        href="http://angular.github.io/protractor/#/">Protractor</a> for integration testing. Angular's documentation
    has a
    good <a href="https://docs.angularjs.org/guide/unit-testing">developer's guide to unit testing</a> if you'd like
    more information on testing and why it's important. </p>
<p>The best reason for writing tests is to automate your testing. Without tests, you'll likely be testing manually. This
    manual testing will take longer and longer as your codebase grows. </p>
<h3>What you'll need</h3>
<ul>
    <li>About 15-30 minutes</li>
    <li>A favorite text editor or IDE. We recommend <a href="https://www.jetbrains.com/idea/">IntelliJ IDEA</a>.
    </li>
    <li><a href="http://git-scm.com/">Git</a> installed.</li>
    <li><a href="http://nodejs.org/">Node.js</a> and NPM installed.</li>
</ul>
<h3>Get the tutorial project</h3>
<p>Clone the angular-tutorial repository using <a href="http://git-scm.com/">git</a> and
    install the dependencies. </p>

<pre>
git clone https://github.com/mraible/angular-tutorial.git
cd angular-tutorial
npm install</pre>
    <p>If you haven't completed the <a href="//raibledesigns.com/rd/entry/getting_started_with_angularjs">Getting
        Started with AngularJS</a> tutorial, you should peruse it so you
        understand how this application works. You can also simply start the app with &quot;npm start&quot; and view it
        in your browser at <a href="http://localhost:8000/app/">http://localhost:8000/app/</a>.</p>

<h3>Test the SearchController</h3>
<ol>
    <li>
        <p>Create <code>app/search/search_test.js</code> and populate it with the basic test infrastructure. This code
            sets up a mock <code>SearchService</code> that has the first function we want to test:
            <code>query(term)</code>. It uses <code>$provide</code> to override the default <code>SearchService</code>.
            <a href="http://stackoverflow.com/questions/20828179/angular-unit-test-controllers-mocking-service-inside-controller">Angular
                unit-test controllers - mocking service inside controller</a> was a useful question on Stack Overflow
            for figuring out how to mock services in controllers.</p>
<pre class="brush: js">'use strict';

describe('myApp.search module', function() {
    var mockSearchService;

    beforeEach(module('myApp.search', function($provide) {
        mockSearchService = {query: function(term) {}};
        $provide.value("SearchService", mockSearchService);
    }));
});</pre>
    </li>
    <li>
        <p>Modify <code>karma.conf.js</code> (in the root directory) to add the search implementation and test.</p>
<pre class="brush: js">files : [
  ...
  'app/components/**/*.js',
  'app/search/search.js',
  'app/search/search_test.js',
  'app/view*/**/*.js'
],</pre>
    </li>
    <li>
        <p>Add the first test to <code>search_test.js</code>. This test verifies that setting a search term and
            executing the <code>search()</code> function will call the service and return results. You can see the results returned
            from the service are mocked with
            <code>deferred.resolve()</code>. The <code>deferred.resolve()</code> call is <a
                    href="http://entwicklertagebuch.com/blog/2013/10/how-to-handle-angularjs-promises-in-jasmine-unit-tests/">how
                to handle promises in unit tests</a>.</p>
<pre class="brush: js">describe('search by term', function() {
    var scope, rootScope, controller, deferred;

	// setup the controller with dependencies.
    beforeEach(inject(function($rootScope, $controller, $q) {
        rootScope = $rootScope;
        scope = $rootScope.$new();
        controller = $controller('SearchController', {$scope: scope, SearchService: mockSearchService });
        deferred = $q.defer();
    }));

    it('should search when a term is set and search() is called', function() {
        spyOn(mockSearchService, 'query').andReturn(deferred.promise);
        scope.term = 'M';
        scope.search();
        deferred.resolve({data: {name: "Peyton Manning"}});
        rootScope.$apply();
        expect(scope.searchResults).toEqual({name: "Peyton Manning"});
    });
});</pre>
        <p>Related: <a href="http://angular-tips.com/blog/2014/03/introduction-to-unit-test-spies/">Introduction to
            Unit Test: Spies</a> is a good introduction to using spies in unit tests.<br/></p></li>
    <li>
        <p>Run the following command on the command line to start the Karma test runner. You can leave this process
            running and new tests will be run automatically. You can change the mocked data and expectation to see your
            test fail.</p>
        <pre>npm test</pre>
        <div class="alert alert-success"><strong>Running Tests from IntelliJ IDEA</strong><br>
            See <a href="https://www.jetbrains.com/idea/help/running-unit-tests-on-karma.html">Running Unit Tests on
                Karma</a> to learn how to run your tests from IntelliJ IDEA.
        </div>
    </li>
    <li>
        <p>Add a test to verify a search occurs automatically when the term is in the URL. Notice how the code structure had
            to change a bit to handle <code>$routeParams</code>.</p>
<pre class="brush: js">describe('search by term automatically', function() {
    var scope, rootScope, controller, location, deferred;

    beforeEach(inject(function($rootScope, $controller, $q) {
        rootScope = $rootScope;
        scope = $rootScope.$new();

        // in this case, expectations need to be setup before controller is initialized
        var routeParams = {"term": "peyton"};
        deferred = $q.defer();
        spyOn(mockSearchService, 'query').andReturn(deferred.promise);
        deferred.resolve({data: {name: "Peyton Manning"}});

        controller = $controller('SearchController', {$scope: scope, $routeParams: routeParams, SearchService: mockSearchService });
    }));

    it('should search automatically when a term is on the URL', function() {
        rootScope.$apply();
        expect(scope.searchResults).toEqual({name: "Peyton Manning"});
    });
});</pre>
    </li>
    <li>
        <p>Add a test to verify the <code>EditController</code> works as expected.</p>
<pre class="brush: js">describe('edit person', function() {
    var scope, rootScope, controller, location, deferred;

    beforeEach(inject(function($rootScope, $controller, $q) {
        rootScope = $rootScope;
        scope = $rootScope.$new();

        // expectations need to be setup before controller is initialized
        var routeParams = {"id": "1"};
        deferred = $q.defer();
        spyOn(mockSearchService, 'fetch').andReturn(deferred.promise);
        deferred.resolve({data: {name: "Peyton Manning", address: {street: "12345 Blake Street", city: "Denver"}}});

        controller = $controller('EditController', {$scope: scope, $routeParams: routeParams, SearchService: mockSearchService });
    }));

    it('should fetch a single record', function() {
        rootScope.$apply();
        expect(scope.person.name).toBe("Peyton Manning");
        expect(scope.person.address.street).toBe("12345 Blake Street");
    });
});</pre>
<p>After adding this test, you'll likely see the following error in your terminal.</p>
<pre class="brush: shell">
Chrome 40.0.2214 (Mac OS X 10.10.2) myApp.search module edit person should fetch a single record FAILED
	fetch() method does not exist
	TypeError: Cannot read property 'name' of undefined
</pre>
        <p>This happens because the <code>mockSearchService</code> does not have a fetch method defined. Modify the <code>beforeEach()</code> on line 7 to add this function.</p>
    <pre class="brush: js">mockSearchService = {query: function(term) {}, fetch: function(id) {}};</pre>
    </li>
</ol>
<h3>Extra Credit</h3>
<p>Create a test for saving a person. <a
        href="http://stackoverflow.com/questions/13664144/how-to-unit-test-angularjs-controller-with-location-service">Here's
    a question on Stack Overflow</a> that might help you verify the location after a save has been performed.</p>
<h3>Test the Search Feature</h3>
<p>To test if the application works end-to-end, you can write scenarios with <a
        href="http://angular.github.io/protractor/">Protractor</a>. These are also known as integration tests, as they
    test
    the <em>integration</em> between all layers of your application.</p>
<p>To verify end-to-end tests work in the project before you begin, run the following command in one terminal
    window:</p>
<pre>npm start</pre>
<p>Then in another window, run the following to execute the tests:</p>
<pre>npm run protractor</pre>
<ol>
    <li>
        <p>Write your first integration test to verify you can navigate to /search and enter a search term to see
            results. Add the following to <code>e2e-tests/scenarios.js</code>:</p>
<pre class="brush: js">describe('search', function() {
  var searchTerm = element(by.model('term'));
  var searchButton = element(by.id('search'));

  beforeEach(function() {
    browser.get('index.html#/search');
  });

  it('should allow searching at /search', function() {
    searchTerm.sendKeys("M");
    searchButton.click().then(function() {
      expect(element.all(by.repeater('person in searchResults')).count()).toEqual(3);
    });
  });
});</pre>
        <p>The &quot;searchTerm&quot; variable represents the input field. The <code>by.model()</code> syntax binds to
            the &quot;ng-model&quot; attribute you defined in the HTML. </p></li>
    <li>
        <p>Run &quot;npm run protractor&quot;. This should fail with the following error.</p>
<pre class="brush: bash">[launcher] Running 1 instances of WebDriver
Selenium standalone server started at http://172.16.6.39:64230/wd/hub
...F
Failures:
  1) my app search should allow searching at /search
   Message:
     NoSuchElementError: No element found using locator: By.id("search")</pre>
    </li>
    <li>
        <p>To fix, you need to add an &quot;id&quot; attribute to the Search button in
            <code>app/search/index.html</code>.</p>
<pre class="brush: xml">
&lt;form ng-submit="search()"&gt;
    &lt;input type="search" name="search" ng-model="term"&gt;
    &lt;button id="search"&gt;Search&lt;/button&gt;
&lt;/form&gt;
</pre>
    </li>
    <li>Run the tests again using &quot;npm run protractor&quot;. They should all pass this time.</li>
    <li>
        <p>Write another test to verify editing a user displays their information.</p>
<pre class="brush: js">describe('edit person', function() {
  var name = element(by.model('person.name'));
  var street = element(by.model('person.address.street'));
  var city = element(by.model('person.address.city'));

  beforeEach(function() {
    browser.get('index.html#/edit/1');
  });

  it('should allow viewing a person', function() {
    // getText() doesn't work with input elements, see the following for more information:
    // https://github.com/angular/protractor/blob/master/docs/faq.md#the-result-of-gettext-from-an-input-element-is-always-empty
    expect(name.getAttribute('value')).toEqual("Peyton Manning");
    expect(street.getAttribute('value')).toEqual("1234 Main Street");
    expect(city.getAttribute('value')).toEqual("Greenwood Village");
  });
});</pre>
        <p>Verify it works with &quot;npm run protractor&quot;.</p></li>
    <li>
        <p>Finally, write a test to verify you can save a person and their details are updated. Figuring out how to
            verify the URL after it changed was assisted by <a href="https://github.com/angular/protractor/issues/610">this
                issue</a>.</p>
<pre class="brush: js">describe('save person', function() {
  var name = element(by.model('person.name'));
  var save = element(by.id('save'));

  beforeEach(function() {
    browser.get('index.html#/edit/1');
  });

  it('should allow updating a name', function() {
    name.sendKeys(" Updated");
    save.click().then(function() {
      // verify url set back to search results
      browser.driver.wait(function() {
        return browser.driver.getCurrentUrl().then(function(url) {
          expect(url).toContain('/search/Peyton%20Manning%20Updated');
          return url;
        });
      });
      // verify one element matched this change
      expect(element.all(by.repeater('person in searchResults')).count()).toEqual(1);
    });
  });
});</pre>
    </li>
    <li>
        <p>When you run this test with &quot;npm run protractor&quot;, it should fail because there's no element with
            <code>id=&quot;save&quot;</code> in <code>app/search/edit.html</code>. Add it to the Save button in this
            file and try again. You should see something similar to the following:</p>
<pre class="brush: bash">Finished in 4.478 seconds
6 tests, 9 assertions, 0 failures</pre>
    </li>
</ol>
<h3>Source Code</h3>
<p>A completed project with this code in it is available on GitHub at <a
        href="https://github.com/mraible/angular-tutorial/">https://github.com/mraible/angular-tutorial</a> on the <strong>testing</strong> branch.
</p>
<p>There are two commits that make the changes for the two main steps in this tutorial:</p>
<ul>
    <li><a href="https://github.com/mraible/angular-tutorial/commit/f3551cc84bd2f0fb35b4dd9ac38cbf6f417c106f">Unit Tests</a></li>
    <li><a href="https://github.com/mraible/angular-tutorial/commit/12e37eb7ad8cde82c6ec92c8562700a7fb6952cc">Integration Tests</a></li>
</ul>

<h3>Summary</h3>
<p>I hope you've enjoyed this quick-and-easy tutorial on testing AngularJS applications. The first couple AngularJS applications
I developed didn't have tests and required a lot of manual testing to verify their quality. After learning that testing AngularJS
apps is pretty easy, I now do it on all my projects. Hopefully this tutorial motivates you to do do the same.
</p>