<p>Our Syncro has been running in tip-top shape ever since we got a <a href="//raibledesigns.com/rd/entry/the_first_day_of_school3">new Subaru H6 engine last
    August</a>. Since then, we've driven it 9000 miles, most of them on <a href="//raibledesigns.com/rd/entry/farewell_to_the_2014_2015">trips during the ski
    season</a>. To begin the camping season this year, we traveled to Moab, Utah for the annual <a href="http://syncrosolstice.com/">Syncro Solstice</a> gathering. We attended our first Syncro Solstice <a href="//raibledesigns.com/rd/entry/syncro_solstice_2014">last
    year</a> and it's been on our calendar ever since. This year, Trish and Abbie opted out, so it was a boys trip for
    Jack and I. </p>
<p style="text-align: center">
    <a href="https://c2.staticflickr.com/6/5447/17313781313_bdd4a9f953_c.jpg" title="Locked and Loaded by Matt Raible, on Flickr" rel="lightbox[syncrosolstice2015]" data-href="https://www.flickr.com/photos/mraible/17313781313"><img src="https://c2.staticflickr.com/6/5447/17313781313_bdd4a9f953_m.jpg" width="240" alt="Locked and Loaded" style="border: 1px solid black;"></a>
    <a href="https://c4.staticflickr.com/8/7673/17748031299_6dd360d41a_c.jpg" title="My co-pilot and best son by Matt Raible, on Flickr" rel="lightbox[syncrosolstice2015]" data-href="https://www.flickr.com/photos/mraible/17748031299"><img src="https://c4.staticflickr.com/8/7673/17748031299_6dd360d41a_m.jpg" width="240" alt="My co-pilot and best son" style="border: 1px solid black; margin-left: 15px;"></a>
</p>

<p>We left Denver on the Thursday morning
    before Mother's Day weekend. We made it all the way to Grand Junction (about 4 hours) before we stopped for gas.
    That's where our adventures began.</p>
<p>I reached for my wallet to grab a credit card. My heart sank when I realized it wasn't in my pocket
    and I'd left it at home. Luckily, we had $10 cash in the ashtray. We bought three gallons of gas and drove to a
    branch of a bank where I keep my checking account. They asked me for my debit card pin number, some other personal information
    and 30 minutes later, we had $200 and a full tank of gas. We drove with the radar detector on for the rest of the
    trip.</p>
<p>My parents met us in Moab, arriving from Montana after a 3-day camping/road trip of their own. Our timing couldn't
    have been more perfect as we arrived within the same minute. That evening, I walked around a bit and shot some
    pictures with Trish's D700.</p>
<p style="text-align: center">
    <a href="https://c4.staticflickr.com/8/7687/17300024763_0b971f201e_c.jpg" title="Sunset Walk by Matt Raible, on Flickr" rel="lightbox[syncrosolstice2015]" data-href="https://www.flickr.com/photos/mraible/17300024763"><img src="https://c4.staticflickr.com/8/7687/17300024763_0b971f201e.jpg" width="500" alt="Sunset Walk" style="border: 1px solid black;"></a>
</p>
<p style="text-align: center">
    <a href="https://c4.staticflickr.com/8/7658/17734251289_ef4a39e948_c.jpg" title="Sweet Double Cab by Matt Raible, on Flickr" rel="lightbox[syncrosolstice2015]" data-href="https://www.flickr.com/photos/mraible/17734251289"><img src="https://c4.staticflickr.com/8/7658/17734251289_ef4a39e948_m.jpg" width="240" alt="Sweet Double Cab" style="border: 1px solid black;"></a>
    <a href="https://c2.staticflickr.com/6/5345/17300020733_7d97431600_c.jpg" title="Kids too! by Matt Raible, on Flickr" rel="lightbox[syncrosolstice2015]" data-href="https://www.flickr.com/photos/mraible/17300020733"><img src="https://c2.staticflickr.com/6/5345/17300020733_7d97431600_m.jpg" width="240" alt="Kids too!" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p style="text-align: center">
    <a href="https://c2.staticflickr.com/6/5337/17732908610_2d596e1703_c.jpg" title="Abel and Friend with Libby! by Matt Raible, on Flickr" rel="lightbox[syncrosolstice2015]" data-href="https://www.flickr.com/photos/mraible/17732908610"><img src="https://c2.staticflickr.com/6/5337/17732908610_2d596e1703_m.jpg" width="240" alt="Abel and Friend with Libby!" style="border: 1px solid black;"></a>
    <a href="https://c2.staticflickr.com/6/5467/17732906600_7ef813713b_c.jpg" title="Colorado Neighbors! by Matt Raible, on Flickr" rel="lightbox[syncrosolstice2015]" data-href="https://www.flickr.com/photos/mraible/17732906600"><img src="https://c2.staticflickr.com/6/5467/17732906600_7ef813713b_m.jpg" width="240" alt="Colorado Neighbors!" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p style="text-align: center">
    <a href="https://c2.staticflickr.com/6/5462/17734244329_8b4a875d09_c.jpg" title="Tom, Wanita and My Folks by Matt Raible, on Flickr" rel="lightbox[syncrosolstice2015]" data-href="https://www.flickr.com/photos/mraible/17734244329"><img src="https://c2.staticflickr.com/6/5462/17734244329_8b4a875d09_m.jpg" width="240" alt="Tom, Wanita and My Folks" style="border: 1px solid black;"></a>
    <a href="https://c4.staticflickr.com/8/7777/17732902520_6c086aa516_c.jpg" title="My Awesome Parents by Matt Raible, on Flickr" rel="lightbox[syncrosolstice2015]" data-href="https://www.flickr.com/photos/mraible/17732902520"><img src="https://c4.staticflickr.com/8/7777/17732902520_6c086aa516_m.jpg" width="240" alt="My Awesome Parents" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p>On Friday, we joined a group of 10 Vanagons going to Determination Towers via <a href="http://www.tripadvisor.com/Guide-g60724-i11063-Moab_Utah.html">Monitor and Merrimac</a>.</p>
<p style="text-align: center">
    <a href="https://c4.staticflickr.com/8/7784/17931442402_8e322b2801_c.jpg" title="Syncro Solstice Tours by Matt Raible, on Flickr" rel="lightbox[syncrosolstice2015]" data-href="https://www.flickr.com/photos/mraible/17931442402"><img src="https://c4.staticflickr.com/8/7784/17931442402_8e322b2801.jpg" width="500" alt="Syncro Solstice Tours" style="border: 1px solid black;"></a>
</p>
<p>
    The road wasn't particularly difficult, but it did get my blood pumping at some points. Our engine hangs pretty
    low in the back. When I hit the first rock that dented the oil pan, I was thankful there were no leaks. However,
    we did spring a coolant leak when climbing the rocks to Determination Towers. </p>
<p style="text-align: center">
<a href="https://c4.staticflickr.com/8/7724/17934773145_44307805df_c.jpg" title="Sliced a coolant line by Matt Raible, on Flickr" rel="lightbox[syncrosolstice2015]" data-href="https://www.flickr.com/photos/mraible/17934773145"><img src="https://c4.staticflickr.com/8/7724/17934773145_44307805df_m.jpg" width="240" alt="Sliced a coolant line" style="border: 1px solid black;"></a>
<a href="https://c1.staticflickr.com/9/8829/17314272393_95dd61066c_c.jpg" title="Sliced a coolant line by Matt Raible, on Flickr" rel="lightbox[syncrosolstice2015]" data-href="https://www.flickr.com/photos/mraible/17314272393"><img src="https://c1.staticflickr.com/9/8829/17314272393_95dd61066c_m.jpg" width="240" alt="Sliced a coolant line" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p>
Luckily, the folks we were with
    had a spare hose I was able to use to temporarily fix the problem. They also had a bunch of shovels to help clean up
    the mess. I especially enjoyed watching the van in front
    of us pop their wheels off the ground a few times. Below is a video I made with the footage I shot on this
    trail.
</p>
<p style="text-align: center">
    <iframe src="https://player.vimeo.com/video/128494418" width="500" height="375" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
</p>
<p>
    On Saturday, we went 4x4ing on Willow Spring Road to Willow Flats Road (into Arches National Park). At about
    halfway, we got a whiff of antifreeze and stopped to check things out. Similar to our damage on Friday, a coolant
    hose was sliced through and we'd lost all our coolant. I shortened the hose, clamped it back on, and continued. We
    stopped at the end of the trail when the red temperature light came on. I looked under the van and it appeared that
    coolant was leaking from the metal pieces that that connect the hose to the engine block. I became a bit worried
    since I can fix hoses, but not engine connections.
</p>
<p>
    We headed into Moab with the engine running at 75% on the temp gauge. I replaced the hose in the parking lot at NAPA
    and threw some Stop Leak in with the coolant. We had a nice lunch at the Moab Brewery, then left town around 3pm for
    the drive back to Denver. It seemed like everything was good-to-go since we made it to Vail (250 miles) before
    stopping for gas. That's when it started to snow.
</p>
<p>
    On the way up Vail pass, the light came on again. I continued to the top since the temp gauge wasn't going up too
    much (60%). I checked everything, relieved some pressure on the coolant tank, then continued. When the light came on
    again by Copper, we decided to stop and have dinner in Frisco. Luckily, we found another brewery (Backcountry
    Brewery) and enjoyed another great meal. Before we left, I put a couple more quarts of coolant in.
</p>
<p>
    The light came on again once we got on I-70, so I pulled over in Silverthorne and called AAA. The tow truck driver
    wasn't willing to haul it back to Denver that night, but he said he would the next day. We continued in my parents
    rig and
    I-70 soon became treacherous. In fact, it was closed between El Rancho and C-470. Luckily, I knew a way around it,
    so we skipped sitting on the freeway and made it home around midnight. I had to drive with the window down for the
    last 1/2 hour to keep myself awake.
</p>
<p>The good news is our Syncro turned out to be in fine shape. I believe there was some air trapped in the cooling
    system,
    that's why the red light kept coming on. We've had no cooling issues since we arrived back in Denver.
</p>
<p>We had a great time at Syncro Solstice! It was great to meet so many Vanagon owners and drool over so many awesome
    vans. Can't wait for next year!</p>
<p style="text-align: center">
    <a href="https://c2.staticflickr.com/8/7753/17920503935_5e8c033cb8_c.jpg" title="Syncro Solstice 2015! by Matt Raible, on Flickr" rel="lightbox[syncrosolstice2015]" data-href="https://www.flickr.com/photos/mraible/17920503935"><img src="https://c2.staticflickr.com/8/7753/17920503935_5e8c033cb8.jpg" width="500" alt="Syncro Solstice 2015!" style="border: 1px solid black;"></a>
</p>