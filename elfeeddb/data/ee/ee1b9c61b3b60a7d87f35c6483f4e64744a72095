<p>I've been on a bit of a yak shave recently
  on <a href="http://technomancy.us">Bussard</a>, my spaceflight programming adventure
  game. The game relies pretty heavily on simulating various
  computer systems, both onboard your craft and in various space
  stations, portal installations, and other craft you encounter. It
  naturally needs to simulate communications between all these.

  </p><p>I started with a pretty simple method of having each connection
    spin up its own coroutine running its own sandboxed
    session. Space station sessions
    run <a href="https://gitlab.com/technomancy/bussard/blob/master/os/orb/resources/smash">smash</a>,
    a vaguely bash-like shell in a faux-unix, while connecting to a
    portal
    triggers <a href="https://gitlab.com/technomancy/bussard/blob/beta-2/os/lisp/resources/portal.lsp">a
    small lisp script to check for clearance and gradually activate
    the gateway sequence</a>. The main loop would allow each
    session's coroutine a slice of time for each update tick, but a
    badly-behaved script could make the frame rate suffer. Also
    input and output was handled in a pretty ad-hoc method where Lua
    tables were used as channels to send strings to and from these
    session coroutines. But most problematic of all was the fact
    that there wasn't any uniformity or regularity in the
    implementations of the various sessions.</p>

  <p>The next big feature I wanted to add was the ability to deploy
    rovers from your ship and SSH into them to control their
    movements or reprogram them. But I really didn't want to add a
    third half-baked session type; I needed all the different types
    to conform to a stricter interface. This required some rethinking.</p>

  <p>The codebase is written primarily in Lua, but not just any
    Lua—it uses the <a href="https://love2d.org">LÖVE</a>
    framework. While Lua's concurrency options are very limited,
    LÖVE offers <a href="https://love2d.org/wiki/love.thread">true
    OS threads</a> which run independently of each other. Now of
    course LÖVE can't magically change the semantics of
    Lua—these threads are technically in the same process but
    cannot communicate directly. All communication happens
    over <a href="https://love2d.org/wiki/Channel">channels</a>
    which allow <em>copies</em> of data to be shared, but not actual
    state.</p>

  <p>While these limitations could be annoying in some cases, they
    turn out to be a perfect fit for simulating communications
    between separate computer systems. Moving to threads allows for
    much more complex programs to run on stations, portals, rovers,
    etc without adversely affecting performance of the game.</p>

  <p>Each world
    has <a href="https://gitlab.com/technomancy/bussard/blob/threads/os/server.lua">a
    thread</a> with a pair of input/output channels that gets
    started when you enter that world's star system. When a login
    succeeds, a thread is created for that specific session, which
    also gets its own <tt>stdin</tt> channel. Each OS can provide
    its own implementation of what
    a <a href="https://gitlab.com/technomancy/bussard/blob/threads/os/orb/session.lua">session
    thread</a> looks like, but they all exchange stdin and stdout
    messages over channels. Interactive sessions will typically run
    a shell like <tt>smash</tt> or a repl and block
    on <tt>stdin:demand()</tt> which will wait until the main
    thread has some input to send along.</p>
    
  <pre class="code"><span class="keyword"><span class="idle-highlight"><span class="region">local</span></span></span><span class="region"> </span><span class="function-name"><span class="region">new_session</span></span><span class="region"> = </span><span class="keyword"><span class="region">function</span></span><span class="region">(username, password)
   </span><span class="keyword"><span class="region">if</span></span><span class="region">(</span><span class="keyword"><span class="region">not</span></span><span class="region"> </span><span class="builtin"><span class="region">os</span></span><span class="region">.is_authorized(hostname, username, password)) </span><span class="keyword"><span class="region">then</span></span><span class="region">
      </span><span class="keyword"><span class="region">return</span></span><span class="region"> output:push({op=</span><span class="string"><span class="region">"status"</span></span><span class="region">, out=</span><span class="string"><span class="region">"Login failed."</span></span><span class="region">})
   </span><span class="keyword"><span class="region">end</span></span><span class="region">
   </span><span class="keyword"><span class="idle-highlight"><span class="region">local</span></span></span><span class="region"> </span><span class="variable-name"><span class="region">session_id</span></span><span class="region"> = </span>utils.uuid()
   <span class="keyword"><span class="idle-highlight"><span class="region">local</span></span></span><span class="region"> </span><span class="variable-name"><span class="region">stdin</span></span><span class="region"> = love.thread.newChannel()
   sessions[session_id] = </span><span class="builtin"><span class="region">os</span></span><span class="region">.new_session(stdin, output, username, hostname)
   sessions[session_id].stdin = stdin
   output:push({op=</span><span class="string"><span class="region">"status"</span></span><span class="region">, ok=</span><span class="constant"><span class="region">true</span></span><span class="region">, [</span><span class="string"><span class="region">"new-session"</span></span><span class="region">] = session_id})
   </span><span class="keyword"><span class="region">return</span></span><span class="region"> </span><span class="constant"><span class="region">true</span></span><span class="region">
</span><span class="keyword"><span class="region">end</span></span><span class="region"></span></pre>