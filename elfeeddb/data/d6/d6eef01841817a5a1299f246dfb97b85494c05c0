<p style="float:right; padding-left:10px; padding-bottom:10px;"><a title="By Coasterman1234 at en.wikipedia (Own work) [CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ACarolina_Cyclone_(Double_Loop).JPG"><img itemprop="image" width="300" alt="Carolina Cyclone (Double Loop)" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Carolina_Cyclone_%28Double_Loop%29.JPG/512px-Carolina_Cyclone_%28Double_Loop%29.JPG"/></a></p>

<p>It was early in <b>Seth's</b> tenure at PicoServices Inc. when he first heard about Joe.</p>

<p>"Oh, man," he was told by a coworker he'd recognized as having a good head on her shoulders. "I can't wait until you end up at a Joe review."</p>

<p>"Joe? Why's that?" he asked, tilting his head.</p>

<p>"Nope, no way. You get to experience Joe the hard way."</p>

<p>Over the coming week, Seth was pretty sure he'd caught people taking bets for something to do with the coming code review, but he wasn't sure why. Joe had seemed like a reasonable fellow when they'd reviewed Beth's code together, and he'd had a few useful pointers for Seth about how to talk to one of their project managers. What could be so bad?</p>

<p>When he got to the conference room where the team had gathered to review Joe's new code, Beth beckoned him over to sit by her, right at the base of the U-shaped table where he had a prime view of the projector. As Joe got his laptop plugged in, most of the room seemed to be watching Seth's face with varying degrees of subtlety.</p>

<p><em>Oh ... kay ...</em> Seth thought, bracing himself for some weird hazing ritual.</p>

<p>Then the projector flickered to life, and he settled in to read the function.</p>
<pre>
<code>
public string DoSomething()
{
  string r = null;
  try
  {
    do
    {
      if (!somePrecondition) break;
      if (!someOtherPrecondition) break;
      //Code goes here
    } while(false);
  }
  catch (Exception e)
  {
    r = "Error: " + e.Message;
  }
  return r;
}
</code>
</pre>

<p>Joe walked them through his method, explaining the preconditions, then the business logic. Questions arose about this and that variable naming, inconsistent spacing, et cetera.</p>

<p>Finally, Seth blurted out the obvious question: "Why <i>while false?"</i></p>

<p>"Obviously, you only want to have a single return. It's much easier to trace that way," Joe replied.</p>

<p>Seth begged to differ. "Okay, and?"</p>

<p>"This way, the rest of the code isn't evaluated if the precondition is true."</p>

<p>Seth blinked. "Why not just use if statements?"</p>

<p>"I did. You see?"</p>

<p>"No, but I mean, put your code in the else."</p>

<p>"Ah, but it gets so messy, all the indentation. Much cleaner this way."</p>

<p>"But ... but that's not ... <i>what?</i> You spent extra time writing lines that do nothing!"</p>

<p>Joe smiled. "Oh, no, of course not. I have a template I use for all my methods."</p>

<p>Chuckles broke out across the room, and a few knowing looks were exchanged.</p>

<p>"He always writes like this," Beth explained in a whisper. "Don't feel bad. We've all tried to talk him out of it."</p>

<p>Seth shook his head. "Okay, whatever, but, why return null?"</p>

<p>"Otherwise, I couldn't return a string here, if there's an exception," Joe explained. "You can't return if it's void."</p>

<p>"Okay but, that's what the exceptions are for, surely. Why not just let it bubble up?"</p>

<p>"And make the calling code clean up the mess? No, no. This is much cleaner."</p>

<p>It was futile. Joe kept insisting that his code was "cleaner," no matter what Seth said.</p>

<p>But Seth would not be deterred. Over the coming weeks, he kept trying to talk to Joe about these anti-patterns, digging up articles about <code>goto</code>, explaining compiler optimization, even roping in experts when they were sent to conventions.</p>

<p>Finally, after 2 months of this, Joe admitted to seeing the light and agreed to change his pattern. The other developers took Seth out to celebrate his success, and he didn't buy a single beer all night. Surely now, things would start looking up, right?</p>

<p>As soon as he saw Joe's next checkin, Seth knew he had to write us:</p>
<pre>
<code>
for ( ; ; )
{
      if (!somePrecondition) break;
      if (!someOtherPrecondition) break;
      //Code goes here
  break; // Always break out of the loop at the end
}
</code>
</pre>

<link rel="stylesheet" href="http://yandex.st/highlightjs/7.3/styles/default.min.css">
<script src="http://img.thedailywtf.com/images/remy/highlight.js/highlight.pack.js"></script>
<script>hljs.initHighlightingOnLoad();</script>

<style>code { font-family: Consolas, monospace; }</style><div>
	<img src="http://thedailywtf.com/images/footer/puppetlabs50.png" style="display:block; float: left; margin: 0 10px 10px 0;" />
	[Advertisement] Manage IT infrastructure as code across all environments with <a href="https://puppetlabs.com/download-puppet-enterprise?ls=content-syndication&ccn=ankeny-2015&pub=DailyWTF&cid=701G0000001AXzu&utm_medium=promoted&utm_campaign=ankeny-2015&utm_source=DailyWTF">Puppet</a>. Puppet Enterprise now offers more control and insight, with role-based access control, activity logging and all-new Puppet Apps. Start your <a href="https://puppetlabs.com/download-puppet-enterprise?ls=content-syndication&ccn=ankeny-2015&pub=DailyWTF&cid=701G0000001AXzu&utm_medium=promoted&utm_campaign=ankeny-2015&utm_source=DailyWTF">free trial</a> today!
</div>
<div style="clear: left;"> </div><div class="feedflare">
<a href="http://syndication.thedailywtf.com/~ff/TheDailyWtf?a=DT2AMcH7DH8:Egpl2sBIEmg:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/TheDailyWtf?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/TheDailyWtf/~4/DT2AMcH7DH8" height="1" width="1" alt=""/>