<p>
    2015 was the year <a href="//raibledesigns.com/rd/category/The+Bus">The Bus</a> was supposed to be finished. If you
    read my <a
    href="//raibledesigns.com/rd/entry/2014_a_year_in_review">year in review from last year</a>, you'll see I was
    certain of it. To be fair, I did have estimates from people that had me expecting it to be done in July. The good
    news is the interior was finished in July. Since then, it's been back at Reincarnation getting the finishing touches
    applied. I believe if it was worked on for a week straight, it could be finished. It's <em>that</em> close. So
    close I can taste it. THIS will be the year!
</p>
<p>For this <a href="//raibledesigns.com/rd/tags/yearinreview">Year in Review</a> post, I'll same the format I've used
    the last few years.
</p>
<ul>
    <li><a href="//raibledesigns.com/rd/entry/2015_a_year_in_review#professional">Professional</a>
        <ul>
            <li><a href="//raibledesigns.com/rd/entry/2015_a_year_in_review#speaking">Speaking</a></li>
            <li><a href="//raibledesigns.com/rd/entry/2015_a_year_in_review#devoxx4kids">Devoxx4Kids</a></li>
            <li><a href="//raibledesigns.com/rd/entry/2015_a_year_in_review#projects">Projects</a></li>
        </ul>
    </li>
    <li><a href="//raibledesigns.com/rd/entry/2015_a_year_in_review#personal">Personal</a>
        <ul>
            <li><a href="//raibledesigns.com/rd/entry/2015_a_year_in_review#theskibus">The Ski Bus</a></li>
        </ul>
    </li>
    <li><a href="//raibledesigns.com/rd/entry/2015_a_year_in_review#2016">2016</a></li>
</ul>

<h2 id="professional">Professional</h2>
<p>I had four different clients in 2015. The first was in the healthcare industry, the second in the API hosting space,
    one in the fashion industry and one in computer software. For the first client, I wrote about <a
        href="//raibledesigns.com/rd/entry/integrating_node_js_ruby_and">integrating Node.js,
        Ruby and Spring with Okta's SAML support</a>. I also <a
        href="//raibledesigns.com/rd/entry/getting_started_with_angularjs">helped them adopt</a> and
    <a href="//raibledesigns.com/rd/entry/testing_angularjs_applications">learn AngularJS</a>. Learning about
    <a href="//raibledesigns.com/rd/entry/best_practices_for_using_foundation1">Foundation and Angular</a> was a nice
    treat too.
</p>
<p>In March, I revisited <a href="//raibledesigns.com/rd/entry/how_to_setup_your_own1">how to setup your own software
    company</a>. In that post, I wrote about how I felt when valuing time over money.</p>
<blockquote class="quote">
    Earlier this year, I had the opportunity to work 20 hours per week instead of 40. It was one of the greatest
    work-life experiences I've had to date. I was still able to pay all my bills, and I had time during
    each-and-every-day to do something fun. When working 40 hours per week, exercising and cooking dinner were somewhat
    of a chore. When I flipped to working less, work became the chore and exercise and cooking became the fun parts of
    my day. I read somewhere recently that if Americans valued <em>health over wealth</em>, we'd be a lot better off. I
    felt like I did this when working less and that I was <em>rich in time</em>.
</blockquote>
<p>
    The second client asked me to do a research project where I tried a bunch of API platforms (e.g.
    Apigee and 3Scale) and wrote up an analysis about five different stories they provided. It was a fun project where
    I learned a lot.
</p>
<p>
    In April, I went to work at a fashion industry startup in downtown Denver. I wrote JavaScript, CSS and HTML for them
    for most of the year. The location afforded me the ability to discontinue my office at
    <a href="http://www.thriveworkplace.com/en">Thrive</a>, a co-working space in Denver. While I missed having kombucha
    and
    local craft beer on tap, I loved the standup desk and the office environment with conversations, banter and laughter
    between desks.
</p>
<p>In November, I picked up a new client that I'm currently working with. They've recently adopted Angular and I've
    enjoyed the experience of working with a team of UI developers. We're putting a fresh UI on a legacy product and
    the team has been able to move really quickly. They're also in the Bay Area so it's been fun traveling to Silicon
    Valley once a month. My contract with them ends this spring.
</p>
<h3 id="speaking">Speaking</h3>
<p>
    I spoke about <a href="//raibledesigns.com/rd/entry/the_art_of_angularjs_in">Angular at Denver's Open Source User
    Group</a> in February
    and about <a href="//raibledesigns.com/rd/entry/getting_hip_with_jhipster_at">JHipster at Denver's Java User
    Group</a> in April. In the second half of the year, I re-joined the conference circuit, starting with
    <a href="//raibledesigns.com/rd/entry/uberconf_2015_my_presentations_on">UberConf</a> in July. September brought me
    to <a href="//raibledesigns.com/rd/entry/springone_2gx_2015_my_presentations">SpringOne 2GX</a> in Washington, DC
    and the <a href="//raibledesigns.com/rd/entry/angular_summit_2015">Angular Summit</a> in Boston.
</p>
<p>
    Trish and I giggled with glee as we <a href="//raibledesigns.com/rd/entry/devoxx_2015_a_java_hipster">returned to
    the Devoxx Belgium</a> in November.
</p>
<p style="text-align: center">
    <a rel="lightbox[2015yearinreview]" href="https://farm6.staticflickr.com/5696/22675879269_b89c0d7368_c.jpg"
       data-href="https://www.flickr.com/photos/mraible/22675879269/" title="First stop: Bier Central!"><img
        src="https://farm6.staticflickr.com/5696/22675879269_b89c0d7368_m.jpg" width="240"
        alt="First stop: Bier Central!" style="border: 1px solid black;"></a>
    <a rel="lightbox[2015yearinreview]" href="https://farm6.staticflickr.com/5811/22649819637_1381655f70_c.jpg"
       data-href="https://www.flickr.com/photos/mraible/22649819637/" title="Shopping"><img
        src="https://farm6.staticflickr.com/5811/22649819637_1381655f70_m.jpg" width="240" alt="Shopping"
        style="border: 1px solid black; margin-left: 15px"></a>
</p>
<p>
    <a href="https://www.youtube.com/watch?v=baVOGuFIe9M">My JHipster talk was a hit</a> and we loved our brief tour of
    Brugge.
</p>

<p style="text-align: center">
    <a href="https://farm6.staticflickr.com/5752/22690142739_24b443fc6a_c.jpg" title="Stadhuis Brugge Belgium"
       rel="lightbox[2015yearinreview]" data-href="https://www.flickr.com/photos/mcginityphoto/22690142739"><img
        src="https://farm6.staticflickr.com/5752/22690142739_24b443fc6a.jpg" width="500" alt="Stadhuis Brugge Belgium"
        style="border: 1px solid black;"></a>
</p>

<p>
    As always, the people and the memories were the
    best part.
</p>
<p style="text-align: center">
    <a href="https://farm1.staticflickr.com/595/22445469834_21210403fb_c.jpg"
       title="Java Hipsters!" rel="lightbox[2015yearinreview]"
       data-href="https://www.flickr.com/photos/mraible/22445469834"><img
        src="https://farm1.staticflickr.com/595/22445469834_21210403fb_m.jpg" width="240" alt="Java Hipsters!"
        style="border: 1px solid black;"></a>

    <a href="https://farm1.staticflickr.com/579/23042227126_7e05081502_c.jpg"
       title="More Cowbell!" rel="lightbox[2015yearinreview]"
       data-href="https://www.flickr.com/photos/mraible/23042227126"><img
        src="https://farm1.staticflickr.com/579/23042227126_7e05081502_m.jpg" width="240" alt="More Cowbell!"
        style="border: 1px solid black; margin-left: 15px;"></a>

</p>
<p>
    To end the year, I journeyed to Fort Lauderdale for the Rich Web Experience. After speaking, I worked remotely and
    networked for a few days. The networking lasted until the wee hours of the morning and good fun was had by all.
</p>


<h3 id="devoxx4kids">Devoxx4Kids</h3>
<p>I didn't plan any <a href="http://www.meetup.com/Devoxx4Kids-Denver">Devoxx4Kids</a> events until later in the
    year. The Denver Chapter watched Arun and Aditya Gupta <a
        href="//raibledesigns.com/rd/entry/minecraft_modding_at_denver_s">
        speak about modding Minecraft</a> in August. I also presented on
    <a href="//raibledesigns.com/rd/entry/setting_up_a_minecraft_server">setting up a Minecraft server in the cloud</a>.
    In October, the talented <a href="http://dbakevlar.com/">Kellyn Pot'Vin-Gorman</a> taught a class about <a
        href="http://www.meetup.com/Devoxx4Kids-Denver/events/224932151/">coding
        simple games with the Raspberry Pi</a>. It's always fun to see the kids' faces light up when they solve a
    programming problem.
</p>
<p>
    This year, with the help of <a href="http://tuliva.com/">Tuliva</a>, we hope to host one Devoxx4Kids class per
    quarter. If there's a particular topic
    or speaker you'd like to see, <a href="//raibledesigns.com/rd/page/contact">please let me know</a>.
</p>

<h3 id="projects">Projects</h3>
<p>
    <a href="http://www.infoq.com/minibooks/jhipster-mini-book">
        <img src="http://www.infoq.com/resource/minibooks/jhipster-mini-book/en/cover/Cover.jpg"
             alt="The JHipster Mini-Book" width="100" class="picture" style="border: 1px solid black"></a>
I released a <a href="//raibledesigns.com/rd/entry/appfuse_3_5_released">new version of AppFuse in February</a> and
    started working on the <a href="http://jhipster.github.io/">JHipster</a> Mini-Book in March. To help
    provide updates about
    the book, I created a blogging app with JHipster and started <a href="http://www.jhipster-book.com">www.jhipster-book.com</a>.
    I wrote the book and created
    <a href="http://www.21-points.com">21-Points Health</a> as its
    example application. I <a href="http://www.jhipster-book.com/#!/news/entry/jhipster-at-devoxx-belgium">open-sourced
        21-Points on GitHub</a> during my Devoxx presentation. Because of my writing and speaking
    about JHipster, I was invited to join the <a href="https://jhipster.github.io/team.html">JHipster Team</a>. I
    happily accepted; it's been a great group to work with.
</p>
<p>
    The <a href="http://www.jhipster-book.com/#!/news/entry/jhipster-mini-book-released">JHipster Mini-Book 1.0 was
    released on October 30, 2015</a>. I wrote about <a
    href="//raibledesigns.com/rd/entry/the_jhipster_mini_book_how">how I wrote it</a> a few days later.
</p>

<h2 id="personal">Personal</h2>
<p>I wrote down my goals at the beginning of 2015: </p>
<ul>
    <li>21-Points Fitness App</li>
    <li>JHipster Mini Book (InfoQ)</li>
    <li>Finish Bus</li>
    <li>New House</li>
    <li>Good Blood Pressure</li>
</ul>
<p>I'm proud to say I accomplished three out of five. The Bus didn't get finished and we failed to sell our house.
    I did create <a href="http://www.21-points.com/">21-Points Health</a>, wrote <a
        href="http://www.infoq.com/minibooks/jhipster-mini-book">
        The JHipster Mini-Book</a> and maintained good health all year.
</p>

<p>We weren't planning on selling our house this year; we wanted to find a new house and rent out our current one.
    However, we
    found a house we loved (small house, lots of land) in the heart of DTC and putting our house on the market was the
    best way to try and get it. Our offer failed
    and we endured the house-is-on-the-market dance through July and August. It was pretty sweet living in such a clean
    house, but we got tired of living in a de-personalized abode. We took it off the market in September and plan
    on renting it once we find our next home.
</p>
<p>The worst part of the year started with a phone call from a friend in the early afternoon on October 7. My good
    friend, Cletus, called to notify me that he'd found our friend, Jason Miller, dead that morning. Jason was a pledge
    class
    brother of Cletus and mine in the Chi Phi Fraternity at DU. You might remember Jason from my
    <a href="//raibledesigns.com/rd/entry/oktoberfest_best_vacation_ever">trip to Oktoberfest in 2008</a>. He was one of my best friends.
</p>
<p>
    Jason was a heckuva guy and an awesome friend. He always inspired laughter and smiles with his witty humor. He will
    be missed, but not forgotten. Jason had a zest for life and a good time that we've resolved to remember annually
    with a
    brother's trip to an exotic gambling destination. This year, we'll be traveling to Las Vegas for St. Patty's Day
    and March Madness.
</p>
<h3 id="theskibus">The Ski Bus</h3>
<p>
    The Syncro Ski Bus had a pretty good year. We took it on <a
    href="//raibledesigns.com/rd/entry/farewell_to_the_2014_2015">many skiing adventures</a>, with trips to
    Steamboat, Crested Butte, and Telluride.
</p>
<p style="text-align: center">
    <a href="https://c1.staticflickr.com/9/8729/16180350303_c112218136_c.jpg"
       title="Packed and ready for Crested Butte. by Matt Raible, on Flickr" rel="lightbox[2015yearinreview]"
       data-href="https://www.flickr.com/photos/mraible/16180350303"><img
        src="https://c1.staticflickr.com/9/8729/16180350303_c112218136_q.jpg" width="150"
        alt="Packed and ready for Crested Butte." style="border: 1px solid black;"></a>
    <a href="https://farm9.staticflickr.com/8613/16799153151_12e8b3d9ea_c.jpg"
       title="Jack, James, Abbie and myself skiing in heaven! by Matt Raible, on Flickr"
       rel="lightbox[2015yearinreview]"
       data-href="https://farm9.staticflickr.com/8613/16799153151_12e8b3d9ea_c.jpg"><img
        src="https://farm9.staticflickr.com/8613/16799153151_12e8b3d9ea_q.jpg" width="150"
        alt="Jack, James, Abbie and myself skiing in heaven!" style="border: 1px solid black; margin-left: 15px;"></a>
    <a href="https://farm9.staticflickr.com/8738/16612686298_1ea3dfc384_c.jpg"
       title="Yay! Car bombs at Ice bar by Matt Raible, on Flickr" rel="lightbox[2015yearinreview]"
       data-href="https://farm9.staticflickr.com/8738/16612686298_1ea3dfc384_c.jpg"><img
        src="https://farm9.staticflickr.com/8738/16612686298_1ea3dfc384_q.jpg" width="150"
        alt="Yay! Car bombs at Ice bar" style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p style="text-align: center">
    <a href="https://c4.staticflickr.com/8/7603/16799164872_82944691da_c.jpg"
       title="We love you Telluride! by Matt Raible, on Flickr" rel="lightbox[2015yearinreview]"
       data-href="https://www.flickr.com/photos/mraible/16799164872"><img
        src="https://c4.staticflickr.com/8/7603/16799164872_82944691da.jpg" width="500" alt="We love you Telluride!"
        style="border: 1px solid black;"></a>
<p>
    We enjoyed a couple overnights and tailgated a few times at our favorite ski area: Mary Jane.
</p>
<p style="text-align: center">
    <a href="https://farm9.staticflickr.com/8628/16800248455_de547b4489_c.jpg"
       title="Ski In, Ski Out at Challenger Lift by Matt Raible, on Flickr" rel="lightbox[2015yearinreview]"
       data-href="https://farm9.staticflickr.com/8628/16800248455_de547b4489_c.jpg"><img
        src="https://farm9.staticflickr.com/8628/16800248455_de547b4489_m.jpg" width="240"
        alt="Ski In, Ski Out at Challenger Lift" style="border: 1px solid black;"></a>
    <a href="https://farm9.staticflickr.com/8628/16180349473_b99397f8f9_c.jpg"
       title="Tailgating at Mary Jane by Matt Raible, on Flickr" rel="lightbox[2015yearinreview]"
       data-href="https://farm9.staticflickr.com/8628/16180349473_b99397f8f9_c.jpg"><img
        src="https://farm9.staticflickr.com/8628/16180349473_b99397f8f9_m.jpg" width="240" alt="Tailgating at Mary Jane"
        style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p>
    I took Jack and we met my parents in Moab in May for <a href="//raibledesigns.com/rd/entry/syncro_solstice_2015">Syncro Solstice 2015</a>. We took it for
    some off-roading, busted a cooling hose a couple times, and almost made it back to Denver.
</p>
<p style="text-align: center">
    <a href="https://c2.staticflickr.com/8/7753/17920503935_5e8c033cb8_c.jpg"
       title="Syncro Solstice 2015! by Matt Raible, on Flickr" rel="lightbox[2015yearinreview]"
       data-href="https://www.flickr.com/photos/mraible/17920503935"><img
        src="https://c2.staticflickr.com/8/7753/17920503935_5e8c033cb8.jpg" width="500" alt="Syncro Solstice 2015!"
        style="border: 1px solid black;"></a>
</p>
<p>
    That next weekend, we took it to <a href="https://www.flickr.com/photos/mcginityphoto/albums/72157654071550305">VWs
    on the Green</a> and showed it alongside The (under construction) Bus.
</p>
<p style="text-align: center">
    <a href="http://www.mcginityphoto.com/Portfolio/VWClassic/i-FvTx9Mx/0/L/Raibles%20at%20VW%20on%20the%20Green-L.jpg"
       rel="lightbox[2015yearinreview]">
        <img
            src="http://www.mcginityphoto.com/Portfolio/VWClassic/i-FvTx9Mx/0/M/Raibles%20at%20VW%20on%20the%20Green-M.jpg"
            alt="Raibles on the VW Green" width="600" style="border: 1px solid black">
    </a>
</p>
<p>
    In July, we journeyed to Montana for the 4th of July. We drove our Westy in the Swan Valley parade while the kids
    rode on top and threw water balloons and candy to the crowd.
    We drove back from Montana in a single day (900 miles). I remember Wyoming well. Sirius XM was broadcasting the last
    Grateful Dead concert from Soldier Field
    and Trish was driving. She sang for hours, driving through the rain and fog while I dozed periodically.
</p>

<p>Sewfine finished The Bus's interior in July. I wrote about this and many other things in <a
    href="//raibledesigns.com/rd/entry/life_update_the_bus_project">
    Life Update: The Bus Project, New Gigs, New House and More</a>.
</p>
<p style="text-align: center">
    <a href="https://farm1.staticflickr.com/404/19352716089_59ed639749_c.jpg"
       title="Love the color scheme with chrome accents" rel="lightbox[2015yearinreview]"
       data-href="https://www.flickr.com/photos/mraible/19352716089/in/album-72157655642253461/"><img
        src="https://farm1.staticflickr.com/404/19352716089_59ed639749_m.jpg" width="240"
        alt="Love the color scheme with chrome accents" style="border: 1px solid black;"></a>
    <a href="https://farm4.staticflickr.com/3786/19543443241_8eeb15dd74_c.jpg" title="The cockpit"
       rel="lightbox[2015yearinreview]"
       data-href="https://www.flickr.com/photos/mraible/19543443241/in/album-72157655642253461/"><img
        src="https://farm4.staticflickr.com/3786/19543443241_8eeb15dd74_m.jpg" width="240" alt="The cockpit"
        style="border: 1px solid black; margin-left: 15px;"></a>
</p>
<p>
    A week later, I won a runner-up trophy for the Syncro at <a
    href="http://www.bandimere.com/tickets-schedule/event-details/2015/07/12/default-calendar/colorado-bug-in-pres-by-nuvintage">
    The Colorado Bug-In at Bandimere Speedway</a>. On my birthday,
    July 16, we drove it to Dinosaur National Monument for a week-long raft trip, starting at Gates of Lodore. I hit my
    first deer
    that night, driving at 3am and doing minimal damage to our German-engineered RV. The deer broke the headlight, but
    not the bulb.
</p>
<p>
    We had some more camping/rafting adventures with it in the fall, but eventually had to take it to the shop for a
    transmission rebuild in November. While it was getting repairs, we upgraded a few things. It now has a South African
    grill, modern headlights, a new fridge and a Propex heater. It's finally become the ultimate camping vehicle, for
    summer or winter.
</p>
<h2 id="2016">2016</h2>
<p>Professionally, I plan to continue speaking at conferences this year. I've submitted a few proposals to <a
    href="http://www.devoxx.fr/">Devoxx France</a>,
    <a href="http://www.devoxx.co.uk/">Devoxx UK</a> and I've agreed to speak at <a href="http://2016.geekout.ee/">Geek
        Out</a>. And did you hear that <a href="http://springone.com/">SpringOne 2016</a> is in Las Vegas?!
</p>
<p>
    I suspect this will be the year of <a href="https://angular.io/">Angular 2</a> and microservices. Watch <a
    href="https://github.com/jhipster/jhipster-experimental-microservices">
    this project</a> to see how JHipster implements microservices. I plan to pump out a release of AppFuse in the near
    future, implementing #NoXML and moving to Java 8.
</p>
<p>
    Trish and I hope to move into a new house this year, but won't be disappointed if we don't find the right one. We
    have
    another year before Abbie starts high school at 7am.
</p><p>
    I have no real big projects planned, except for driving The Bus. We do have a few fun vacations planned.
    In fact,
    I'm writing this while riding in the back of our Westy. My parents and I are road-tripping from Denver to Jekyll
    Island, Georgia.
    We're taking the scenic route to see some places we've never been and staying at RV Parks with the top popped. We
    arrive in
    New Orleans this afternoon and will be staying two nights to enjoy the culture and cuisine.
</p>
<p style="text-align: center">
    <a  rel="lightbox[2015yearinreview]" href="https://farm2.staticflickr.com/1682/24235662332_ef9e21ff8e_c.jpg" data-href="https://www.flickr.com/photos/mraible/24235662332/"
       title="The back seat"><img src="https://farm2.staticflickr.com/1682/24235662332_ef9e21ff8e_m.jpg" width="240" style="border: 1px solid black" alt="The back seat"></a>
    <a  rel="lightbox[2015yearinreview]" data-href="https://www.flickr.com/photos/mraible/24317694286/" href="https://farm2.staticflickr.com/1612/24317694286_e05ec2fd4a_c.jpg"
       title="Somewhere in Arkansas..."><img src="https://farm2.staticflickr.com/1612/24317694286_e05ec2fd4a_m.jpg"
                                             width="240" alt="Somewhere in Arkansas..." style="border: 1px solid black; margin-left: 15px"></a>
</p>
<p> My Dad turns 70 this weekend and we planned a family vacation to celebrate. His brother and sister are joining us
    and Trish and the kids are flying in this weekend. We figured a road trip across the country was appropriate since
    we've done so many together and I happen to own the ultimate vehicle for car camping. Below is a <a href="https://goo.gl/maps/9hE5aNkF7k22">map of our
    route</a>.</p>
<div style="text-align: center">
<iframe src="https://www.google.com/maps/embed?pb=!1m58!1m12!1m3!1d13460084.342798809!2d-102.15251344423332!3d34.54967111566971!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m43!3e0!4m5!1s0x876b80aa231f17cf%3A0x118ef4f8278a36d6!2sDenver%2C+CO!3m2!1d39.739235799999996!2d-104.990251!4m5!1s0x87cf62f745c8983f%3A0x6bfd6cb31e690da0!2sSpringfield%2C+MO!3m2!1d37.2089572!2d-93.29229889999999!4m5!1s0x87cd2a9388325047%3A0xbaa8bef944021e0d!2sHot+Springs%2C+AR!3m2!1d34.5037004!2d-93.0551795!4m5!1s0x8620a454b2118265%3A0xdb065be85e22d3b4!2sNew+Orleans%2C+LA!3m2!1d29.9510658!2d-90.0715323!4m5!1s0x889381562ac66341%3A0xbf585ed52c4701f3!2sPanama+City%2C+FL!3m2!1d30.158812899999997!2d-85.6602058!4m5!1s0x88e4dbf62542c839%3A0x10d22d63ea360435!2sJekyll+Island%2C+Glynn+County%2C+GA!3m2!1d31.0685662!2d-81.4134297!4m5!1s0x876b80aa231f17cf%3A0x118ef4f8278a36d6!2sDenver%2C+CO!3m2!1d39.739235799999996!2d-104.990251!5e0!3m2!1sen!2sus!4v1452694275660" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<p>In June, we'll be celebrating Trish's parents 50th wedding anniversary in Kauai. With Trish's brother, sister and
    children joining in, everyone is sure to have a fantastic vacation. I hope to continue the fun by working remotely
    from Montana in July.
</p>
<p>When I drive the finished bus in the Swan Valley parade on the 4th of July, I'll be smiling from ear-to-ear. 2016
    <em>is</em> the year! The long wait will be over soon...
</p>