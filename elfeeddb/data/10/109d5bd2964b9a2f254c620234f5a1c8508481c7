<p><img align="right" src="https://herbsutter.files.wordpress.com/2014/03/031714_1624_stroustrups1.png?w=500" alt="" />It has occurred to me that I never announced this event here…
</p>
<p>In two weeks, Bjarne and I will be doing a <a href="http://isocpp.org/blog/2014/03/stroustrup-sutter2">two-day <strong>Stroustrup &amp; Sutter on C++</strong> seminar in the San Francisco Bay area</a>. It has been several years since the last S&amp;S event, so Bjarne and I are really looking forward to this.
</p>
<blockquote>
<p><a href="http://www.eeliveshow.com/sanjose/conference/super-c-tutorial.php"><span style="font-size:14pt;"><strong>Super C++ Tutorial: Stroustrup &amp; Sutter on C++</strong></span></a><span style="font-size:14pt;"><strong><br /></strong></span><br />EE Live!<br />March 31 &#8211; April 1, 2014<br />McEnery Convention Center<br />San Jose, CA, USA</p>
<p><a href="http://www.eeliveshow.com/sanjose/registration/?_mc=EELHOMEPAGE">Registration</a>
		</p>
</blockquote>
<p>The organizers have kindly made sure they can expand the room, so <a href="http://isocpp.org/blog/2014/03/stroustrup-sutter2">seats are still available</a>. Sorry for the short notice here on this blog.
</p>
<h2>Using C++ atomics: Lock-Free Algorithms and Data Structures in C++<br />
</h2>
<p>Here&#8217;s one interesting content update: The sessions page lists our talks, including a talk by me with the title <strong>&#8220;Three Cool Things in C++ Concurrency,&#8221;</strong> which is pronounced &#8220;I hadn&#8217;t decided what exactly I wanted to talk about by the time I had to submit the session description.&#8221;
</p>
<p>I have now decided, and the talk will be entirely on <strong>how to design and write lock-free algorithms and data structures using C++ atomic&lt;&gt;</strong> – something that can look deceptively simple, but contains very deep topics. (Important note: This is <em>not</em> the same as my &#8220;atomic&lt;&gt; Weapons&#8221; talk; that talk was about the &#8220;what they are and why&#8221; of the C++ memory model and atomics, and did not cover how to actually use atomics to implement highly concurrent algorithms and data structures.)
</p>
<p>This talk is about the &#8220;how to use them successfully&#8221; part of atomics, including:
</p>
<ul>
<li>Best practices and style for using atomic&lt;&gt;s
</li>
<li>Three examples, including lock-free mail slots and iterations of a lock-free linked list, all in portable Standard C++
</li>
<li>Defining and applying the different levels of &#8220;lock-freedom&#8221; (wait-free, lock-free, obstruction-free) to develop highly concurrent algorithms and data structures
</li>
<li>Explaining and applying key concepts including especially: linearizability; trading off concurrency vs. promptness; and trading off concurrency vs. throughput/utilization
</li>
<li>Demonstrating and solving the ABA problem, with clean and simple code – that&#8217;s right, did you know that in C++11 you can solve this without relying on esoterica like double-wide CAS (which isn&#8217;t always available) or hazard pointers (which are deeply complex) or garbage collection (which isn&#8217;t in the C++ standard… yet)?
</li>
</ul>
<p>A few of you may have seen part of this material in the few times I&#8217;ve taught the extended four-day version of my Effective Concurrency course. This version of the material is <strong>significantly updated for C++11/14</strong> and also contains <strong>new material never before seen</strong> even if you did take in the four-day EC course – including that instead of leaving the slist example as a cliffhanger, I present an actual complete solution for the slist example that is (a) correct and (b) can be entirely written using portable Standard C++11. It&#8217;s always nice to end with a solution you can actually use, instead of just an open problem cliffhanger…
</p>
<p>I&#8217;m looking forward to seeing many of you in San Jose in two weeks!</p><br />Filed under: <a href='https://herbsutter.com/category/c/'>C++</a>  <img alt="" border="0" src="https://pixel.wp.com/b.gif?host=herbsutter.com&#038;blog=3379246&#038;post=2434&#038;subd=herbsutter&#038;ref=&#038;feed=1" width="1" height="1" />